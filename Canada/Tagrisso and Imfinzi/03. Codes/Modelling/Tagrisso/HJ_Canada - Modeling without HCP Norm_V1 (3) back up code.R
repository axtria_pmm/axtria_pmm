#Setting directory
# setwd("C:/Users/knss680/Box Sync/Canada Oncology/Modelling/Tagrisso")

# Master data
# master_raw_act = read.xlsx("Tagrisso DB v3 Covid.xlsx",sheet = "Tagrisso DB v3 Covid")
# 18540*39
setwd("C:/Users/kxpx414/OneDrive - AZCollaboration/AZ/Global/Canada/Tagrisso/Modelling")
getwd()
# Master data
master_raw_act = read.csv("Tagrisso DB v3 Covid.csv")

# Renaming Column names
master_raw_act = colClean(master_raw_act)
names(master_raw_act)

master_raw = master_raw_act
master_raw$Tsa_sales[master_raw$Tsa_sales<0] = 0
master_raw = master_raw[master_raw$Province!="NT",]

# Converting Cluster column
# unique(master_raw$Cluster)
# master_raw$Cluster = ifelse(master_raw$Cluster=='Low',1,2)
# unique(master_raw$Cluster)

# Creating another cluster column
master_raw$Cluster = 1
master_raw$norm = 1

# Calculating Final PDEs after applying Omnichannel scores
master_raw$Total_PDEs = master_raw$F2F_pde + master_raw$Remote_pde + master_raw$Email_pde + master_raw$Phone_pde

F2F_pde_wt = 1
Remote_pde_wt = 0.5
Email_pde_wt = 0.1
Phone_pde_wt = 0.5

master_raw$Total_PDEs_wt = master_raw$F2F_pde*F2F_pde_wt + master_raw$Remote_pde*Remote_pde_wt +
  master_raw$Email_pde*Email_pde_wt + master_raw$Phone_pde*Phone_pde_wt

# Basic summary
sum(master_raw$Total_PDEs)
sum(master_raw$Tsa_sales)
sum(master_raw$Event_Attendees)
master_raw %>% filter(YM>=202101) %>%  select(FSA,Total_HCP, Targeted_HCP) %>% distinct() %>% 
   summarize(Total_HCP = sum(Total_HCP),
             Targeted_HCP = sum(Targeted_HCP))

sum(master_raw$Total_PDEs_wt[master_raw$YM>=202101 & master_raw$AU==1])
sum(master_raw$Tsa_sales[master_raw$YM>=202101 & master_raw$AU==1])
sum(master_raw$Event_Attendees[master_raw$YM>=202101 & master_raw$AU==1])
names(master_raw)

master_raw$Event_Attendees = master_raw$Meetings
# Normalizing with Relevant HCPs
# # master_raw$Tsa_sales_norm = master_raw$Tsa_sales/master_raw$Accounts
# master_raw$Total_PDEs_wt_norm = master_raw$Total_PDEs_wt/master_raw$Accounts
# master_raw$Event_Attendees_norm = master_raw$Event_Attendees/master_raw$Accounts

# Normalizing with Prov Relevant HCPs
master_raw$Tsa_sales_norm = master_raw$Tsa_sales/master_raw$Prov_Relevant_HCP
master_raw$Total_PDEs_wt_norm = master_raw$Total_PDEs_wt/master_raw$Prov_Relevant_HCP
master_raw$Event_Attendees_norm = master_raw$Event_Attendees/master_raw$Prov_Relevant_HCP





colnames(master_raw)[colnames(master_raw)=="YM"] = 'yrmo'

# Create any necessary normalized variables before proceeding
dependent_variable = "Tsa_sales_norm"
original_variable = "Tsa_sales"
normalization_variable = "Prov_Relevant_HCP"
monthly_normalization = "norm"

mth_norm = master_raw[,c("yrmo",monthly_normalization)]
mth_norm = mth_norm %>% distinct()

# Filtering Data based on months and Analysis Universe
# master_raw = master_raw[(master_raw$yrmo>=202009 & master_raw$yrmo<=202112 & master_raw$AU ==1),]
master_raw = master_raw[(master_raw$yrmo>=202009 & master_raw$yrmo<=202112),]
master = master_raw

colnames(master)[colnames(master)=="FSA"]='dma'
colnames(master_raw)[colnames(master_raw)=="FSA"]='dma'

##################################### ADSCTOCKING VARIABLES ##############################################
colnames(master)
n_lags = 2
master_2 = sales_lag_calc(master,dependent_variable,n_lags)

# Promotional Adstocking
alpha_pdes_wt = c(0.5,0.5^2,0.5^3,0.5^4)
alpha_attendees = c(0.5,0.5^2,0.5^3,0.5^4)
colnames(master_2)

master_2 = promo_lag_calc(master_2,'Total_PDEs_wt',4,alpha_pdes_wt)
master_2 = promo_lag_calc(master_2,'Event_Attendees',4,alpha_attendees)
master_2 = promo_lag_calc(master_2,'Total_PDEs_wt_norm',4,alpha_pdes_wt)
master_2 = promo_lag_calc(master_2,'Event_Attendees_norm',4,alpha_attendees)
colnames(master_2)

##################################### Transformations ##############################################
# Calculate segment curvatures and smooth the estimates  
##################################### PDEs CURVATURES  ##############################################
################ Creating segments on the basis of market share   ##############################################
grouping_data <- master_2 %>%
  select(dma,Tsa_sales,yrmo) %>%
  filter(yrmo>=202101) %>%
  group_by(dma) %>%
  summarise(BS=sum(Tsa_sales))

Grouping <- grouping_data %>%
  arrange(-desc(BS)) %>%
  mutate(Percent = cumsum(BS)/sum(BS)) %>%
  mutate (BS_Bkt =
            case_when(Percent <= 0.1 ~ 1,
                      Percent < 1 ~ 2,
                      T~2))
# Grouping_data = master_2 %>%
#           select(Province,dma,Tsa_sales) %>%
#           group_by(Province,dma) %>%
#           summarise(tt = sum(Tsa_sales))
# 
# 
# Grouping <- Grouping_data %>%
#             mutate(BS_Bkt = 
#                      case_when(Province == "NL" ~ 1,
#                                Province == "NS" ~ 2,
#                                Province == "PE" ~ 3,
#                                Province == "NB" ~ 4,
#                                Province == "QC" ~ 5,
#                                Province == "ON" ~ 6,
#                                Province == "MB" ~ 7,
#                                Province == "SK" ~ 8,
#                                Province == "AB" ~ 9,
#                                Province == "BC" ~ 10,
#                                T~33))

Grouping %>% 
  group_by(BS_Bkt) %>%
  summarise(n = n())

master_2 = merge(master_2,Grouping[,c("dma","BS_Bkt")], by = "dma", all.x = T)

# # Calculating Curvatures for promotional variables transformation
avg_df_pdes = data.frame(master_2[(master_2$yrmo>=202101 & master_2$Total_PDEs_wt_norm_e>0),c("BS_Bkt","Total_PDEs_wt_norm_e")] %>%
                      group_by(BS_Bkt) %>% 
                      summarise_each(funs = c(mean)))

S_A = 0.6

for(col in names(avg_df_pdes)[-1]){
  avg_df_pdes[paste0(col,"_C_SA")] = (-1/avg_df_pdes[col])*log(1-S_A)
  avg_df_pdes[paste0(col,"_1_Mean_C")] = 1/(avg_df_pdes[col])
  avg_df_pdes[paste0(col,"_4_Mean_C")] = 4/(avg_df_pdes[col])
}

avg_df_pdes
names(avg_df_pdes)

# Events Curvature
avg_df_att = data.frame(master_2[(master_2$yrmo>=202101 & master_2$Event_Attendees_norm_e>0),c("BS_Bkt","Event_Attendees_norm_e")] %>%
                           group_by(BS_Bkt) %>% 
                           summarise_each(funs = c(mean)))

S_A = 0.6

for(col in names(avg_df_att)[-1]){
  avg_df_att[paste0(col,"_C_SA")] = (-1/avg_df_att[col])*log(1-S_A)
  avg_df_att[paste0(col,"_1_Mean_C")] = 1/(avg_df_att[col])
  avg_df_att[paste0(col,"_4_Mean_C")] = 4/(avg_df_att[col])
}

avg_df_att
names(avg_df_att)

master_2 = merge(master_2,avg_df_pdes[-c(2)], by = "BS_Bkt",all.x = T)
master_2 = merge(master_2,avg_df_att[-c(2)], by = "BS_Bkt",all.x = T)
names(master_2)

# NLS Curvature
1/mean(master_2$Total_PDEs_wt_norm_e[master_2$yrmo>=202101 & master_2$Total_PDEs_wt_norm_e>0])
4/mean(master_2$Total_PDEs_wt_norm_e[master_2$yrmo>=202101 & master_2$Total_PDEs_wt_norm_e>0])

neg_exponential_transform <- cmpfun(function(df, formula){
  
  # browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula, 
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 0, b = 0, d = 0.1),
                       start_upper = c(a = 0, b = 0.1, d = 0.1),
                       lower = c(a = 0, b = -Inf, d = -Inf),
                       upper = c(a = Inf, b = Inf, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall() 
})


check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = -Inf, d = -Inf), 
                                           upper_bounds = c(a = Inf, b = Inf, d = Inf)){
  # browser()  
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})

master_2_transform_PDE <- master_2 %>%
  select(BS_Bkt, dma, yrmo, Tsa_sales_norm, Total_PDEs_wt_norm_e) %>%
  filter(yrmo>=202101) %>% 
  nest(data = -BS_Bkt) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Tsa_sales_norm ~ a * (1 - exp(-b * Total_PDEs_wt_norm_e)) + d), 
                          .options = get_furrr_options(scheduling = 2)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(BS_Bkt) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-BS_Bkt)

master_2_transform_PDE$curvature
# master_2_transform_PDE_suggested <- master_2_transform_PDE %>%
#   mutate(Total_PDEs_wt_C = apply_trend_smoothing(curvature))

# PDE_curvature_suggested <- master_2_transform_PDE[,c("Cluster","estimates_valid","curvature","Total_PDEs_wt_C")]


##################################### Attendees CURVATURES  ##############################################
1/mean(master_2$Event_Attendees_norm_e[master_2$yrmo>=202101 & master_2$Event_Attendees_norm_e>0])
4/mean(master_2$Event_Attendees_norm_e[master_2$yrmo>=202101 & master_2$Event_Attendees_norm_e>0])

neg_exponential_transform <- cmpfun(function(df, formula){
  
  # browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula, 
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 0, b = 0, d = 0.1),
                       start_upper = c(a = 0, b = 15, d = 0.1),
                       lower = c(a = 0, b = 0, d = -Inf),
                       upper = c(a = Inf, b = 100, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall() 
})


check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = 0, d = -Inf), 
                                           upper_bounds = c(a = Inf, b = 100, d = Inf)){
  # browser()  
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})


master_2_transform_Attendees <- master_2 %>%
  select(BS_Bkt, dma, yrmo, Tsa_sales_norm, Event_Attendees_norm_e) %>%
  filter(yrmo >=202101) %>% 
  nest(data = -BS_Bkt) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Tsa_sales_norm ~ a * (1 - exp(-b * Event_Attendees_norm_e)) + d), 
                          .options = get_furrr_options(scheduling = 2)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(BS_Bkt) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-BS_Bkt)

master_2_transform_Attendees$curvature

data_summary_atten=master_2%>%
  select(dma,yrmo,Tsa_sales,Total_PDEs_wt_e,BS_Bkt,Event_Attendees_e)%>%
  filter(Event_Attendees_e>0,yrmo>=202101)%>%
  group_by(BS_Bkt)%>%
  summarise(no_of_account=length(unique(dma)),
            Average_attend=mean(Event_Attendees_e),
            avg_1_1_attend=1/mean(Event_Attendees_e),
            avg_1_4_attend=4/mean(Event_Attendees_e),
            min_attend=min(Event_Attendees_e),maxi_attend=max(Event_Attendees_e))
View(data_summary_atten)
View(master_2_transform_Attendees)
master_2_transform_Attendees[master_2_transform_Attendees$BS_Bkt == 1,]$curvature = 0.6*0.2101167



data_summary_PDE=master_2%>%
  select(dma,yrmo,Tsa_sales,Total_PDEs_wt_e,BS_Bkt)%>%
  filter(Total_PDEs_wt_e>0,yrmo>=202101)%>%
  group_by(BS_Bkt)%>%
  summarise(no_of_account=length(unique(dma)),Average_PDE=mean(Total_PDEs_wt_e),
            avg_1_PDE=1/mean(Total_PDEs_wt_e),
            avg_4_PDE=4/mean(Total_PDEs_wt_e),
            min_pde=min(Total_PDEs_wt_e),maxi_pde=max(Total_PDEs_wt_e))
View(data_summary_PDE)
View(master_2_transform_PDE)
master_2_transform_PDE[master_2_transform_PDE$BS_Bkt == 1,]$curvature = 0.43*0.01076456




# Merging Curvature information with the main data
# master_3 = master_2 %>% 
#   left_join((master_2_transform_PDE %>% select(BS_Bkt, curvature) %>% rename(Total_PDEs_wt_e_C_NLS = "curvature")),
#             by = c("BS_Bkt"="BS_Bkt")) %>% 
#   left_join((master_2_transform_Attendees %>% select(BS_Bkt, curvature) %>% rename(Event_Attendees_e_C_NLS = "curvature")),
#             by = c("BS_Bkt"="BS_Bkt"))
# names(master_3)

# Curvature Tweaking
# master_3$Total_PDEs_wt_e_C_NLS = 0.0205229
# master_3$Event_Attendees_e_C_NLS = 0.0912486

avg_df_pdes
avg_df_att

# ###avg_pde_HCP_normal_HL
S_A=0.6
(PDE_C1=(1/0.0007881587))
(PDE_C2=(1/0.0026424615))
#avg_attend_HCP_normal_HL
(Attend_C1=(1.25/0.0002990047))
(Attend_C2=(0.6/0.0005792121))
################CHange the curvature values here###################
master_3= master_2
master_3 = master_3 %>%
  mutate(Total_PDEs_wt_e_C = ifelse(Cluster==1,PDE_C1,PDE_C2),
         Event_Attendees_e_C = ifelse(Cluster==1,Attend_C1,Attend_C2))

# write.csv(master_3, "Master_3.csv", row.names = F)

# Variable transformations
master_3$Total_PDEs_wt_norm_exp = 1- (exp(-master_3$Total_PDEs_wt_e_C*master_3$Total_PDEs_wt_norm_e))
master_3$Event_Attendees_norm_exp = 1- (exp(-master_3$Event_Attendees_e_C*master_3$Event_Attendees_norm_e))

# plot(master_3$Total_PDEs_wt_norm_e,master_3$Total_PDEs_wt_norm_exp)
# plot(master_3$Event_Attendees_norm_e,master_3$Event_Attendees_norm_exp)

promotions = c("Total_PDEs_wt_norm_exp","Event_Attendees_norm_exp")

names(master_3)[32:40]
cvd_ctrl = 1
covid_control = c("apple_transit")

# Models
start=202101
end=202112
model_equation = paste(dependent_variable,"~",
                       dependent_variable,"_1+",
                       dependent_variable,"_2+",
                       paste(promotions, collapse = '+')
                       ,"+"
                       ,paste(covid_control, collapse = '+')
                       ,sep = ""
)
model_equation

############ Regression
output = PRM(master_3,model_equation,start,end)
View(output)
sum(output$Estimate[2:3])

sum(master_3$Tsa_sales_norm_1)
sum(master_3$Tsa_sales_norm_2)
write.csv(output, "output_HL_ProvNormAcc_1m_1.csv")

# lag_sum_criteria = 0.8
# Sales_lower = 0
# sales_lag_condition = 2
# l1 = 0.5
# l2 = 0.4
# 
# cf = PRM1(master_2,model_equation,start,end)
# cf$rn = output$rn
# cf$variable = NULL
# rownames(cf) = rownames(output)
# cf
# 
# output = cf

# ########### Impact calculations
mth_start = 5
mth_end = 16

cvd_ctrl = 1

# master_2 = master_3
# fin_out = impact_calc(mth_start,mth_end,promotions)
fin_out = impact_calc(master_3,mth_start,mth_end,promotions)

a  = cbind(fin_out$impact_percent,percent(fin_out$impact_percent$percent))
colnames(a) = c(colnames(fin_out$impact_percent),"%")
a
View(a)
write.csv(a, "impact_HL_sales_Prov_NLS_acc_2.csv")

percent(fin_out$non_normalized_error)

s1 = fin_out$output_monthly

trend = s1[,c("yrmo","total_predicted","total_actual")]
trend$total_predicted = unlist(trend$total_predicted)
write.csv(trend, "trend_HL_sales_Prov_NLS_acc_2.csv", row.names = F)

ggplot(trend, aes(as.factor(yrmo), group = 1)) + 
  geom_line(aes(y = total_predicted, colour = "Predicted")) + 
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

output[,c("rn","Estimate")]
a
model_equation

