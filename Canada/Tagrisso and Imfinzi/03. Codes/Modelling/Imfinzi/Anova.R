rm(list=ls())

library(NbClust)
library(xlsx)
library(readxl)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(data.table)
library(DataCombine)
library(tidyverse)
library(ggplot2)
library(fastcluster)
library(clustertend)
library(gtools)
library(openxlsx)
library(StatMeasures)
library(dvmisc)
####WSS fuction####
options(scipen = 1000)

wssplot <- function(data, nc=15, seed=1234){
  wss <- (nrow(data)-1)*sum(apply(data,2,var))
  for (i in 2:nc){
    set.seed(seed)
    wss[i] <- sum(kmeans(data, centers=i)$withinss)}
  plot(1:nc, wss, type="b", xlab="Number of Clusters",
       ylab="Within groups sum of squares")
  wss
}


zscore <-function(data,col_nm)
{
  mn = mean(data[,col_nm])
  sdev = sd(data[,col_nm])
  data[,paste0(col_nm,"_z")] = (data$col_nm - mn)/sdev
}

##
setwd(loc)
loc = "C:/Users/kxpx414/OneDrive - AZCollaboration/AZ/Global/Canada/Imfinzi/Clustering/Tier 1"
# kmean_data=read.xlsx(paste0(loc,"/Imf_Clustering_db_v1.xlsx"))
kmean_data=read.xlsx(paste0(loc,"/Acc_FSA_Prov_imf_DB.xlsx"), sheet = "Province")

kmean_data_norm=kmean_data

# View(kmean_data_norm)
colnames(kmean_data_norm[,])
col_nms = colnames(kmean_data_norm[,c("Q1_sales","Q2_sales",     "Q3_sales",     "Q4_sales",
                                      "Q1_calls",     "Q2_calls",     "Q3_calls",     "Q4_calls",
                                      "Q1_meet",      "Q2_meet","Q3_meet",     "Q4_meet"
                                      )])

kmean_data_norm[,col_nms] = kmean_data_norm[,col_nms]/kmean_data_norm$Relevant_HCP

for (i in (1:length(col_nms))){
  print(i)
  mn = mean(kmean_data_norm[,col_nms[i]])
  sdev = sd(kmean_data_norm[,col_nms[i]])
  kmean_data_norm[,paste0(col_nms[i],"_z")] = (kmean_data_norm[,col_nms[i]] - mn)/sdev
}


# View(kmean_data_norm)
col_nms_z = paste0(col_nms,"_z")
##wss
wssplot(kmean_data_norm[,col_nms_z]) # 3 clust
fviz_nbclust(kmean_data_norm[,col_nms_z], kmeans, method = "silhouette")#3 clust

## number of clusters :3
set.seed(123)
kmeanss=kmeans(kmean_data_norm[,col_nms_z],3)
#Storing the results
kmean_data_norm_1=kmean_data_norm
kmean_data_norm_1$clusters=as.factor(kmeanss$cluster)
a=fviz_cluster(kmeanss,kmean_data_norm_1[,col_nms_z],geom = "point")
a

Ano_df_data=read.xlsx(paste0(loc,"/Anova_DB.xlsx"))

Clust_vec = c("Rule_1","Rule_2")
col_Name_vec = c("R12M_Sales","R12M_Calls","R12M_Meets","Sales_per_Call","Sales_per_Meet")

for (i in 1: (length(Clust_vec)))
{
for (j in 1: (length(col_Name_vec))) {
Ano_df_data=read.xlsx(paste0(loc,"/Anova_DB.xlsx"))
Ano_df = Ano_df_data[,c(Clust_vec[i],col_Name_vec[j])]
Ano_df$clusters = Ano_df[,Clust_vec[i]]
Ano_df$clusters = as.factor(Ano_df$clusters)
Ano_df_anv = aov( Ano_df[,col_Name_vec[j]] ~ clusters , data = Ano_df  )
summary(Ano_df_anv)
tukey_ano_df = TukeyHSD(Ano_df_anv)
tukey_ano_df
Ano_df_pVal = as.data.frame(tukey_ano_df$clusters)
t2=as.data.frame(row.names(Ano_df_pVal))
Ano_df_pVal=cbind(t2,Ano_df_pVal)

write.xlsx(Ano_df_pVal,paste0(Clust_vec[i],"_Anova_",col_Name_vec[j],".xlsx"))
}
}


wwerr = aov(Rule_2 ~ R12M_Sales + R12M_Calls+R12M_Meets+Sales_per_Call+Sales_per_Meet, data = Ano_df_data)

Ano_df_data$cluster_qc = as.character(Ano_df_data$Rule_2)

wwerr = aov(Sales_per_Call+Sales_per_Meet ~ Rule_2 , data = Ano_df_data)
eee = summary(wwerr)

rrr = do.call(rbind.data.frame, eee)

write.xlsx(rrr,"Summ_Rule_2.xlsx")
summary(wwerr)
TukeyHSD(wwerr)


BS_MS_df=test_df%>%select(micro_seg_spec,Total_BS,Total_MS)
BS_MS_df$micro_seg_spec=as.factor(BS_MS_df$micro_seg_spec)
BS_anv = aov( Total_BS ~ micro_seg_spec, data = BS_MS_df)
summary(BS_anv)
tukey_BS=TukeyHSD(BS_anv)
BS_pvalue=as.data.frame(tukey_BS$micro_seg_spec)
t2=as.data.frame(row.names(BS_pvalue))
BS_pvalue=cbind(t2,BS_pvalue)

write.xlsx(kmean_data_norm_1,"Province_Quartery_Sales_Calls_Meet_Rel_norm_Cluster_k3.xlsx")


