#Setting directory
setwd("C:/Users/kxpx414/OneDrive - AZCollaboration/AZ/Global/Canada/Imfinzi/Modelling")

# Master data
master_raw_act = read.xlsx("Imfinzi DB v3.xlsx", sheet = 'Db v3')
# 6148*115

# Renaming Column names
master_raw_act = colClean(master_raw_act)
names(master_raw_act)

master_raw = master_raw_act
master_raw$Tsa_Sales[master_raw$Tsa_Sales<0] = 0

# Converting Cluster column
unique(master_raw$Cluster)
master_raw$Cluster = ifelse(master_raw$Cluster=='Low',1,2)
unique(master_raw$Cluster)

# Creating another cluster column
master_raw$Cluster = 1

# Calculating Final PDEs after applying Omnichannel scores
master_raw$Total_PDEs = master_raw$F2F_PDEs + master_raw$Remote_PDE + master_raw$Email_PDEs + master_raw$Phone_PDEs + master_raw$Service_PDEs

F2F_PDEs_wt = 1
Remote_PDEs_wt = 0.5
Email_PDEs_wt = 0.1
Phone_PDEs_wt = 0.5
Service_PDEs_wt = 0.1

master_raw$Total_PDEs_wt = master_raw$F2F_PDEs*F2F_PDEs_wt + master_raw$Remote_PDEs*Remote_PDEs_wt +
  master_raw$Email_PDEs*Email_PDEs_wt + master_raw$Phone_PDEs*Phone_PDEs_wt + 
  master_raw$Service_PDEs*Service_PDEs_wt

sum(master_raw$Total_PDEs)
sum(master_raw$Total_PDEs_wt)





master_raw$Tsa_Sales_hcp_norm = master_raw$Tsa_Sales/master_raw$Relevant_HCP_V3
master_raw$Total_PDEs_wt_norm = master_raw$Total_PDEs_wt/master_raw$Relevant_HCP_V3
master_raw$Medical_Attendees_hcp_norm = master_raw$Medical_Attendees/master_raw$Relevant_HCP_V3

col_nms = c("Total_PDEs_wt","Medical_Attendees","Total_PDEs_wt_norm","Medical_Attendees_hcp_norm")
for (i in (1:length(col_nms))){
  print(i)
  
  master_raw[,paste0(col_nms[i],"_z")] = scale(master_raw[,col_nms[i]])
}

sum(master_raw$Total_PDEs_norm_z)

colnames(master_raw)[colnames(master_raw)=="YM"] = 'yrmo'

# Create any necessary normalized variables before proceeding
# dependent_variable = "Tsa_Sales_hcp_norm"
# original_variable = "Tsa_Sales"
# normalization_variable = "Relevant_HCP_V2"
# monthly_normalization = "norm"

dependent_variable = "Tsa_Sales_hcp_norm"
original_variable = "Tsa_Sales"
normalization_variable = "Relevant_HCP_V3"
monthly_normalization = "norm"

mth_norm = master_raw[,c("yrmo",monthly_normalization)]
mth_norm = mth_norm %>% distinct()

master_raw_filter = master_raw[(master_raw$yrmo<=202110 & master_raw$yrmo>=202007),]
master = master_raw_filter

colnames(master)[colnames(master)=="Account"]='dma'
colnames(master_raw)[colnames(master_raw)=="Account"]='dma'

##################################### ADSCTOCKING VARIABLES ##############################################
colnames(master)
master_2 <- master %>%
  select(dma, Cluster, yrmo, Tsa_Sales, Tsa_Sales_hcp_norm, 
         Total_PDEs_wt,Total_PDEs_wt_norm,
         Total_PDEs_wt_z,Total_PDEs_wt_norm_z,
         Medical_Attendees,Medical_Attendees_hcp_norm,
         Medical_Attendees_z,Medical_Attendees_hcp_norm_z, norm, Relevant_HCP_V3) %>%
  arrange(dma, yrmo) %>%
  group_by(dma) %>%
  mutate(Tsa_Sales_hcp_norm_1 = lag(Tsa_Sales_hcp_norm, 1),
         Tsa_Sales_hcp_norm_2 = lag(Tsa_Sales_hcp_norm, 2)) %>%
  ungroup %>%
  pivot_longer(c(Total_PDEs_wt,Total_PDEs_wt_norm,
                 Total_PDEs_wt_z,Total_PDEs_wt_norm_z,
                 Medical_Attendees,Medical_Attendees_hcp_norm,
                 Medical_Attendees_z,Medical_Attendees_hcp_norm_z)) %>%
  # pivot_longer(c(Total_PDEs_wt_norm,Medical_Attendees_hcp_norm)) %>%
  arrange(dma, name, yrmo) %>%
  group_by(dma, name) %>%
  mutate(value = value + (.5 * lag(value, 1, default = 0)) +
           (.5^2 * lag(value, 2, default = 0)) +
           (.5^3 * lag(value, 3, default = 0)) +
           (.5^4 * lag(value, 4, default = 0))) %>% 
  ungroup %>%
  #filter(year_month > max(year_month)-months(12)) %>% ##To change properly
  # filter(yrmo > 201812) %>%
  pivot_wider() 

master_2[is.na(master_2)] = 0
names(master_2)


##################################### Transformations ##############################################
# Calculate segment curvatures and smooth the estimates  
##################################### PDEs CURVATURES  ##############################################
1/mean(master_2$Total_PDEs_wt[master_2$yrmo>=202011])
# 48.707
4/mean(master_2$Total_PDEs_wt[master_2$yrmo>=202011])
# 194.8295

neg_exponential_transform <- cmpfun(function(df, formula){
  
  # browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula, 
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 0, b = 0, d = 0.1),
                       start_upper = c(a = 0, b = 0.1, d = 0.1),
                       lower = c(a = 0, b = -Inf, d = -Inf),
                       upper = c(a = Inf, b = Inf, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall() 
})


check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = -Inf, d = -Inf), 
                                           upper_bounds = c(a = Inf, b = Inf, d = Inf)){
  # browser()  
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})

master_2_transform_PDE <- master_2 %>%
  select(Cluster, dma, yrmo, Tsa_Sales, Total_PDEs_wt_norm) %>%
  nest(data = -Cluster) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Tsa_Sales ~ a * (1 - exp(-b * Total_PDEs_wt_norm)) + d), 
                          .options = get_furrr_options(scheduling = 2)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(Cluster) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-Cluster)

View(master_2_transform_PDE)
# master_2_transform_PDE_suggested <- master_2_transform_PDE %>%
#   mutate(Total_PDEs_wt_C = apply_trend_smoothing(curvature))

# PDE_curvature_suggested <- master_2_transform_PDE[,c("Cluster","estimates_valid","curvature","Total_PDEs_wt_C")]


##################################### Attendees CURVATURES  ##############################################
1/mean(master_2$Medical_Attendees_hcp_norm[master_2$yrmo>=202011])
# 74.42588
4/mean(master_2$Medical_Attendees_hcp_norm[master_2$yrmo>=202011])
# 297.7035

neg_exponential_transform <- cmpfun(function(df, formula){
  
  # browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula, 
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 0, b = 0, d = 0.1),
                       start_upper = c(a = 0, b = 15, d = 0.1),
                       lower = c(a = 0, b = -Inf, d = -Inf),
                       upper = c(a = Inf, b = Inf, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall() 
})


check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = 0, d = -Inf), 
                                           upper_bounds = c(a = Inf, b = Inf, d = Inf)){
  # browser()  
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})


master_2_transform_Attendees <- master_2 %>%
  select(Cluster, dma, yrmo, Tsa_Sales, Medical_Attendees) %>%
  nest(data = -Cluster) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Tsa_Sales ~ a * (1 - exp(-b * Medical_Attendees)) + d), 
                          .options = get_furrr_options(scheduling = 2)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(Cluster) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-Cluster)

View(master_2_transform_Attendees)
# master_2_transform_Attendees_suggested <- master_2_transform_Attendees %>%
#   mutate(Medical_Attendees_hcp_norm_C = apply_trend_smoothing(curvature))
# 
# Attendees_curvature_suggested <- master_2_transform_Attendees_suggested[,c("Cluster","estimates_valid","curvature","Medical_Attendees_hcp_norm_C")]


# # Calculating Curvatures for promotional variables transformation
avg_df = data.frame(master_2[(master_2$yrmo>=202011 & master_2$Cluster ==1),c("Total_PDEs_wt_norm",
                                                     "Medical_Attendees_hcp_norm","Total_PDEs_wt",
                                                     "Medical_Attendees")] %>%
                      summarise_each(funs = c(mean)))

S_A = 0.75
for(col in names(avg_df)){
  avg_df[paste0(col,"_C")] = (-1/avg_df[col])*log(1-S_A)
}

avg_df
#   Total_PDEs_wt_norm Medical_Attendees_hcp_norm Total_PDEs_wt_norm_C Medical_Attendees_hcp_norm_C
# 1         0.02053077                 0.01343619             51.13409                     78.13394


# Merging Curvature information with the main data
master_3 = master_2 %>% 
  left_join((master_2_transform_PDE %>% select(Cluster, curvature) %>% rename(Total_PDEs_wt_C = "curvature")),
            by = c("Cluster"="Cluster")) %>% 
  left_join((master_2_transform_Attendees %>% select(Cluster, curvature) %>% rename(Medical_Attendees_C = "curvature")),
            by = c("Cluster"="Cluster"))
# write.csv(master_3, "Master_3.csv", row.names = F)

# Variable transformations
master_3$Total_PDEs_wt_norm_exp = 1- (exp(-master_3$Total_PDEs_wt_C*master_3$Total_PDEs_wt_norm))
master_3$Medical_Attendees_hcp_norm_exp = 1- (exp(-master_3$Medical_Attendees_C*master_3$Medical_Attendees_hcp_norm))

plot(master_3$Total_PDEs_wt_norm,master_3$Total_PDEs_wt_norm_exp)
plot(master_3$Medical_Attendees_hcp_norm,master_3$Medical_Attendees_hcp_norm_exp)

promotions = c("Total_PDEs_wt_exp","Medical_Attendees_exp")

# cvd_ctrl = 1
# covid_control = c("Cases_per_cap","comp_growth_var")

#Function for Lag calculation
lag_cal = function(x,var_ind,gr_var_ind,l){
  for(i in c(1:l)){
    Nvar = paste(colnames(x[var_ind]),as.character(i),sep = "_")
    x = slide(x, Var = var_ind, GroupVar = gr_var_ind , NewVar = Nvar, slideBy = -i,reminder=F)
    i = i+1
    x[is.na(x[,Nvar]),Nvar] <- 0
  }
  return (x)
}

lags_func <- function(model_Data_3,varrrr,lags)
{
  for (i in 1:length(varrrr))
  {
    gr_var_ind<- "dma"
    # lags<- 3
    var_ind<- varrrr[i]
    model_Data_3 = lag_cal(model_Data_3,var_ind,gr_var_ind,lags)
    mddata_t=model_Data_3
  }
  return(mddata_t)
  
}

var_ad <- function(x,gr_var_ind,var_name,num_lags,lag_wt)
{
  x1 = lag_cal(x,var_name,gr_var_ind,num_lags)
  x1[is.na(x1)]<-0
  lag_name <- c(var_name,paste0(var_name,paste0("_",1:num_lags)))
  x1[,paste0(var_name,"_ad")] <- rowSums(t(lag_wt[1:length(lag_name)]*t(x1[,lag_name])))
  dropList=c(lag_name[2:length(lag_name)])
  x2=x1[,!colnames(x1) %in% dropList]
  return(x2)
}

adstock <-function(model_Data_3,var_ind,lag_wt,curv)
{
  
  gr_var_ind<- "dma"
  
  lags<- (length(lag_wt) - 1)
  model_Data_3 = var_ad(model_Data_3,gr_var_ind,var_ind,lags,lag_wt)
  # c=2/mean(model_Data_3[,paste0(var_ind,"_ad")])
  model_Data_3[,paste0(var_ind,"_ad_t")] <- (1-exp(-curv*model_Data_3[,paste0(var_ind,"_ad")]))
  
  
  return(model_Data_3)
}

mddata<-lags_func(master_3,dependent_variable,3)
# View(master_2_transform_PDE)
avg_df
mddata <- adstock(mddata,"Total_PDEs_wt_norm_z",c(1,.5),88)
plot(mddata$Total_PDEs_wt_norm_z_ad,mddata$Total_PDEs_wt_norm_z_ad_t)
# View(master_2_transform_Attendees)
mddata <- adstock(mddata,"Medical_Attendees_z_hcp_norm",c(1,.5),1)
plot(mddata$Medical_Attendees_hcp_z_norm_ad,mddata$Medical_Attendees_hcp_z_norm_ad_t)




# Models
cvd_ctrl = 0
start=202011
end=202110
model_equation = paste(dependent_variable,"~",
                       dependent_variable,"_1+",
                       dependent_variable,"_2+",
                       dependent_variable,"_3+",
                       paste(promotions, collapse = '+')
                       # ,"+"
                       # ,paste(covid_control, collapse = '+')
                       ,sep = ""
)
model_equation = "Tsa_Sales_hcp_norm~Tsa_Sales_hcp_norm_1+Tsa_Sales_hcp_norm_2+Total_PDEs_wt_z_norm_ad_t"

############ Regression
output = PRM(mddata,model_equation,start,end)
output
View(output)

# ########### Impact calculations
mth_start = 3
mth_end = 14

cvd_ctrl = 0
promotions = "Total_PDEs_wt_z_norm_ad_t"
fin_out = impact_calc(mddata,mth_start,mth_end,promotions)

a  = cbind(fin_out$impact_percent,percent(fin_out$impact_percent$percent))
colnames(a) = c(colnames(fin_out$impact_percent),"%")
a
percent(fin_out$non_normalized_error)

s1 = fin_out$output_monthly

trend = s1[,c("yrmo","total_predicted","total_actual")]

trend$total_predicted = unlist(trend$total_predicted)

ggplot(trend, aes(yrmo)) + 
  geom_line(aes(y = total_predicted, colour = "Predicted")) + 
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

ggplot(trend, aes(as.factor(yrmo), group = 1)) +
  geom_line(aes(y = total_predicted, colour = "Predicted")) +
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)
output[,c("rn","Estimate")]
a
model_equation
