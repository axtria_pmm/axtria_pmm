rm(list=ls())

library(NbClust)
library(xlsx)
library(readxl)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(data.table)
library(DataCombine)
library(tidyverse)
library(ggplot2)
library(fastcluster)
library(clustertend)
library(gtools)
library(openxlsx)
library(StatMeasures)
library(dvmisc)
####WSS fuction####
options(scipen = 1000)

wssplot <- function(data, nc=15, seed=1234){
  wss <- (nrow(data)-1)*sum(apply(data,2,var))
  for (i in 2:nc){
    set.seed(seed)
    wss[i] <- sum(kmeans(data, centers=i)$withinss)}
  plot(1:nc, wss, type="b", xlab="Number of Clusters",
       ylab="Within groups sum of squares")
  wss
}


zscore <-function(data,col_nm)
  {
 mn = mean(data[,col_nm])
 sdev = sd(data[,col_nm])
 data[,paste0(col_nm,"_z")] = (data$col_nm - mn)/sdev
 }

##
setwd(loc)
loc = "C:/Users/kxpx414/OneDrive - AZCollaboration/AZ/Global/Canada/Imfinzi/Clustering/Tier 1"
# kmean_data=read.xlsx(paste0(loc,"/Imf_Clustering_db_v1.xlsx"))
kmean_data=read.xlsx(paste0(loc,"/IMF_Cluster_DB.xlsx"))

kmean_data_norm=kmean_data%>%
  filter(AU == 1,Account>0)

# View(kmean_data_norm)
colnames(kmean_data_norm[,3:6])
col_nms = colnames(kmean_data_norm[,3:14])

kmean_data_norm[,3:6] = kmean_data_norm[,3:6]/kmean_data_norm$Relevant_HCP

for (i in (1:length(col_nms))){
  print(i)
  mn = mean(kmean_data_norm[,col_nms[i]])
  sdev = sd(kmean_data_norm[,col_nms[i]])
  kmean_data_norm[,paste0(col_nms[i],"_z")] = (kmean_data_norm[,col_nms[i]] - mn)/sdev
}


# View(kmean_data_norm)
col_nms_z = paste0(col_nms,"_z")
##wss
wssplot(kmean_data_norm[,col_nms_z]) # 3 clust
fviz_nbclust(kmean_data_norm[,col_nms_z], kmeans, method = "silhouette")#3 clust

## number of clusters :3
set.seed(123)
kmeanss=kmeans(kmean_data_norm[,col_nms_z],3)
#Storing the results
kmean_data_norm_1=kmean_data_norm
kmean_data_norm_1$clusters=as.factor(kmeanss$cluster)
a=fviz_cluster(kmeanss,kmean_data_norm_1[,col_nms_z],geom = "point")
a


write.xlsx(kmean_data_norm_1,"Qtr_Sales_Rel_Norm_Calls_meet_Cluster_k3.xlsx")
