-- 2020

select 'FORXIGA FAMILY' as Brand , call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd  ,min(date_sk) as min_date_sk , max(date_sk) as max_date_sk,count(distinct call_id) as Calls
from (
select  call_subtype_desc ,call_delivery_channel_desc, mdm_rep_type_cd , date_sk , call_id 
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
join can_mc_dw.dim_employee_role f on f.employee_role_sk=a.employee_role_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')    --- product_brand_sk_la to change when we are calculating sales for different brands
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and date_sk like '2020%')
group by call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd ;

-- 2019

select 'FORXIGA FAMILY' as Brand , call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd  ,min(date_sk) as min_date_sk , max(date_sk) as max_date_sk,count(distinct call_id) as Calls
from (
select  call_subtype_desc ,call_delivery_channel_desc, mdm_rep_type_cd , date_sk , call_id 
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
join can_mc_dw.dim_employee_role f on f.employee_role_sk=a.employee_role_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')    --- product_brand_sk_la to change when we are calculating sales for different brands
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and date_sk like '2019%')
group by call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd ;

-------------------------------------------------------------------------------------

select 'FORXIGA FAMILY' as Brand , call_type , call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd  ,min(date_sk) as min_date_sk , max(date_sk) as max_date_sk,count(distinct call_id) as Calls
from (
select  call_subtype_desc ,call_delivery_channel_desc, mdm_rep_type_cd , date_sk , call_id , call_type
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
join can_mc_dw.dim_employee_role f on f.employee_role_sk=a.employee_role_sk
where ((c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')    --- product_brand_sk_la to change when we are calculating sales for different brands
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and (date_sk like '2020%' OR date_sk like '2019%')))
group by call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd , call_type ;

--- 2019 , 2020 - By detailing sales team * month
select 'FORXIGA FAMILY' as Brand , call_type , call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd  , left(date_sk,6) as Year_month , count(distinct call_id) as Calls
from (
select  call_subtype_desc ,call_delivery_channel_desc, mdm_rep_type_cd , date_sk , call_id , call_type
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
join can_mc_dw.dim_employee_role f on f.employee_role_sk=a.employee_role_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')    --- product_brand_sk_la to change when we are calculating sales for different brands
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and (date_sk like '2019%' or date_sk like '2020%'))
group by call_subtype_desc ,call_delivery_channel_desc , mdm_rep_type_cd , call_type , Year_month ;


SELECT TOP 10 * FROM can_mc_dw.call_discussion_priority WHERE call_attendee_type='Person_Account_vod'

SELECT TOP 10 * FROM can_mc_dw.call_discussion_priority WHERE call_face_to_face_flag='No'

SELECT DISTINCT call_face_to_face_flag FROM can_mc_dw.call_discussion_priority
