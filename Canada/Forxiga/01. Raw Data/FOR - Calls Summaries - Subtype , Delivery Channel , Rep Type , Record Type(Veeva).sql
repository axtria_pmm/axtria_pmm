select top 10 * from can_mc_dw.call_detail

select distinct call_record_type_name from can_mc_dw.call_detail


select call_record_type_name , min(date_sk) , max(date_sk) from can_mc_dw.call_detail
group by call_record_type_name

select * from can_mc_dw.call_detail where call_record_type_name = 'Veeva Remote Detailing'

-- 2020
select distinct  call_subtype_desc ,call_delivery_channel , call_record_type_name , min(date_sk) as min_date_sk , max(date_sk) as max_date_sk,count(distinct call_id) as Calls , count(distinct mdm_customer_id) as HCPs
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel f on f.call_delivery_channel_sk = a.call_delivery_channel_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')  --- product_brand_sk_la to change when we are calculating sales for different brands or c.mdm_prod_brand_name like '%FORXIGA%'
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and ( date_sk like '2020%')
group by call_subtype_desc ,call_delivery_channel , call_record_type_name ;

-- 2019
select distinct  call_subtype_desc ,call_delivery_channel , call_record_type_name , min(date_sk) as min_date_sk , max(date_sk) as max_date_sk,count(distinct call_id) as Calls , count(distinct mdm_customer_id) as HCPs
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel f on f.call_delivery_channel_sk = a.call_delivery_channel_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')  --- product_brand_sk_la to change when we are calculating sales for different brands or c.mdm_prod_brand_name like '%FORXIGA%'
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and ( date_sk like '2019%')
group by call_subtype_desc ,call_delivery_channel , call_record_type_name ;


-- 2019 , 2020 Record Type Trend by Month
select distinct  call_record_type_name , left(date_sk,6) as Year_Month ,count(distinct call_id) as Calls
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel f on f.call_delivery_channel_sk = a.call_delivery_channel_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')  --- product_brand_sk_la to change when we are calculating sales for different brands or c.mdm_prod_brand_name like '%FORXIGA%'
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and ( date_sk like '2019%' OR date_sk like '2020%' )
group by call_record_type_name , Year_Month ;
