select customer_sk, cast(Year_Month as int) ,
(P1_Sales_call_2020*1) as P1_Sales_pde , (P2_Sales_call_2020*0.5) as P2_Sales_pde , 
(P1_LL_call_2020*1) as P1_LL_pde , (P2_LL_call_2020*0.5) as P2_LL_pde , 
(P1_Sales_Call_rem_2020*1) as P1_Sales_rem_pde , (P2_Sales_Call_rem_2020*0.5) as P2_Sales_rem_pde ,
(P1_LL_call_rem_2020*1) as P1_LL_rem_pde , (P2_LL_call_rem_2020*0.5) as P2_LL_rem_pde ,
(P1_Email_call_2020*1) as P1_Email_pde , (P2_Email_call_2020*0.5) as P2_Email_pde ,
(P1_Sales_call_2020*1 + P2_Sales_call_2020*0.5) as Sales_call_pde_2020 , 
(P1_LL_call_2020*1 + P2_LL_call_2020*0.5) as LL_pde_2020 , 
(P1_Sales_Call_rem_2020*1 + P2_Sales_Call_rem_2020*0.5) as Sales_rem_pde_2020 , 
(P1_LL_call_rem_2020*1 + P2_LL_call_rem_2020*0.5) as LL_rem_pde_2020 , 
(P1_Email_call_2020*1 + P2_Email_call_2020*0.5) as Email_pde_2020 , 
(Sales_call_pde_2020+LL_pde_2020 + Sales_rem_pde_2020 + LL_rem_pde_2020 + LL_rem_pde_2020) as total_pde_2020
into ##temp_pde_subcat_2020_fxg
from
(select  customer_sk, cast(Year_Month as int) ,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then Sales_call_2020 else 0 end) as P1_Sales_call_2020,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then Sales_Call_rem_2020 else 0 end) as P1_Sales_Call_rem_2020,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then Sales_call_2020 else 0 end) as P2_Sales_call_2020,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then Sales_Call_rem_2020 else 0 end) as P2_Sales_Call_rem_2020,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then LL_call_2020 else 0 end) as P1_LL_call_2020,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then LL_call_rem_2020 else 0 end) as P1_LL_call_rem_2020,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then LL_call_2020 else 0 end) as P2_LL_call_2020,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then LL_call_rem_2020 else 0 end) as P2_LL_call_rem_2020,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then Email_call_2020 else 0 end) as P1_Email_call_2020,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then Email_call_2020 else 0 end) as P2_Email_call_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and customer_specialty_group_desc='CARDIOLOGY' then Sales_call_2020 else 0 end) as P1_Sales_call_CV_Card_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and customer_specialty_group_desc='CARDIOLOGY' then LL_call_2020 else 0 end) as P1_LL_call_CV_Card_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and customer_specialty_group_desc='CARDIOLOGY' then Sales_Call_rem_2020 else 0 end) as P1_Sales_Call_rem_CV_Card_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and customer_specialty_group_desc='CARDIOLOGY' then LL_call_rem_2020 else 0 end) as P1_LL_call_rem_CV_Card_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and customer_specialty_group_desc='CARDIOLOGY' then Email_call_2020 else 0 end) as P1_Email_call_CV_Card_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) then Sales_call_2020 else 0 end) as P2_Sales_call_CV_nonCard_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) then LL_call_2020 else 0 end) as P2_LL_call_CV_nonCard_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) then Sales_Call_rem_2020 else 0 end) as P2_Sales_Call_rem_CV_nonCard_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) then LL_call_rem_2020 else 0 end) as P2_LL_call_rem_CV_nonCard_2020,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) then Email_call_2020 else 0 end) as P2_Email_call_CV_nonCard_2020
from (
select a.customer_sk, cast(Year_Month as int) ,mdm_rep_type_cd ,customer_specialty_group_desc,
(dp1_salescall + dp2_salescall + dp3_salescall) as Sales_call_2020 ,
(dp1_LL + dp2_LL + dp3_LL) as LL_call_2020,
(dp1_SalesCall_rem + dp2_SalesCall_rem + dp3_SalesCall_rem) as Sales_Call_rem_2020,
(dp1_LL_rem + dp2_LL_rem + dp3_LL_rem) as LL_call_rem_2020,
(dp1_Email_call + dp2_Email_call + dp3_Email_call) as Email_call_2020

from 
(select  a.customer_sk,left(date_sk,6) as Year_Month, mdm_rep_type_cd , 
count( distinct case when detail_priority=1 and call_subtype_desc='Sales Call' and call_delivery_channel_desc='F2F' then call_id else null end) as dp1_SalesCall,
count( distinct case when detail_priority=2 and call_subtype_desc='Sales Call' and call_delivery_channel_desc='F2F' then call_id else null end) as dp2_SalesCall,
count( distinct case when detail_priority=3 and call_subtype_desc='Sales Call' and call_delivery_channel_desc='F2F' then call_id else null end) as dp3_SalesCall,

count( distinct case when detail_priority=1 and (
(call_subtype_desc='Sales Call' and call_delivery_channel_desc='Remote') or
(call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='F2F')or 
(call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='Remote')) then call_id else null end) as dp1_SalesCall_rem,
count( distinct case when detail_priority=2 and (
(call_subtype_desc='Sales Call' and call_delivery_channel_desc='Remote') or
(call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='F2F')or 
(call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='Remote')) then call_id else null end) as dp2_SalesCall_rem,
count( distinct case when detail_priority=3 and (
(call_subtype_desc='Sales Call' and call_delivery_channel_desc='Remote') or
(call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='F2F')or 
(call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='Remote')) then call_id else null end) as dp3_SalesCall_rem,

count( distinct case when detail_priority=1 and call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='F2F' then call_id else null end) as dp1_LL,
count( distinct case when detail_priority=2 and call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='F2F' then call_id else null end) as dp2_LL,
count( distinct case when detail_priority=3 and call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='F2F' then call_id else null end) as dp3_LL,

count( distinct case when detail_priority=1 and ( 
(call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Remote') or
(call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='F2F')or 
(call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='Remote')) then call_id else null end) as dp1_LL_rem,
count( distinct case when detail_priority=2 and (
(call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Remote') or
(call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='F2F')or 
(call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='Remote')) then call_id else null end) as dp2_LL_rem,
count( distinct case when detail_priority=3 and (
(call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Remote') or
(call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='F2F')or 
(call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='Remote')) then call_id else null end) as dp3_LL_rem,

count( distinct case when detail_priority=1 and (
(call_subtype_desc='Sales Call' and call_delivery_channel_desc='Email') or 
(call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Email') ) then call_id else null end) as dp1_Email_call,
count( distinct case when detail_priority=2 and (
(call_subtype_desc='Sales Call' and call_delivery_channel_desc='Email') or 
(call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Email') ) then call_id else null end) as dp2_Email_call,
count( distinct case when detail_priority=3 and (
(call_subtype_desc='Sales Call' and call_delivery_channel_desc='Email') or 
(call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Email') ) then call_id else null end) as dp3_Email_call

from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_employee_role f on a.employee_role_sk = f.employee_role_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
where (c.mdm_prod_brand_name like '%FORXIGA%')   --- product_brand_sk_la to change when we are calculating sales for different brands
--- PDES - NOT INCLUDING XIGDUO --  or c.mdm_prod_brand_name like '%XIGDUO%'
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and date_sk between '20200101' and '20201231'
group by a.customer_sk,left(date_sk,6), mdm_rep_type_cd) a
left join 

(select distinct cst.customer_sk,cst.mdm_customer_id,customer_specialty_group_desc
--into ##temp_spec_grp_fxg
from  can_mc_dw.dim_customer cst
left join 
(select distinct splm.customer_specialty_sk,customer_specialty_group_desc from 
can_mc_dw.dim_curnt_customer_specialty_grouping_to_market splm 
join can_mc_dw.dim_curnt_customer_specialty_group cspg on cspg.customer_specialty_group_sk=splm.customer_specialty_group_sk
where mdm_prod_mkt_id in (select distinct mdm_prod_mkt_id from can_mc_dw.dim_curnt_product_market where mdm_prod_mkt_name='INSULIN MARKET')
)z on cst.customer_specialty_sk=z.customer_specialty_sk
) b
on a.customer_sk=b.customer_sk
)
group by customer_sk , Year_Month)
where total_pde_2020>0;

select * from ##temp_pde_subcat_2020_fxg;
