

drop table IF EXISTS ##temp_pde_subcat_2020_fxg;
drop table IF EXISTS ##temp_sales_direction_2020_fxg;
drop table IF EXISTS ##temp_CV_call_details;

---- Create temp table with Sales Direction for CV Reps 

---- Create temp table with Sales Direction for CV Reps

select distinct customer_sk,Year_Month, mdm_rep_type_cd , SALES_DIRECTION,customer_specialty_group_desc,call_subtype_desc,
call_delivery_channel_desc,province , geo_province_name, employee_role_sk , role_level_cd ,--'Spec Rep CV' as mdm_rep_type_cd,
count(distinct call_id) calls  
into ##temp_sales_direction_2020_fxg
from 
(
select distinct a.customer_sk,a.call_id,Year_Month,customer_specialty_group_desc,a.call_subtype_desc,a.call_delivery_channel_desc,province , geo_province_name,
ISNULL(p1,'') as p1_new,ISNULL(p2,'') as p2_new,ISNULL(p3,'') as p3_new,p1_new+','+p2_new+','+p3_new as SALES_DIRECTION , a.employee_role_sk , a.role_level_cd , a.mdm_rep_type_cd from
(select distinct a.customer_sk,left(date_sk,6) as Year_Month,call_subtype_desc,call_delivery_channel_desc,province , geo_province_name,call_id , a.employee_role_sk , f.role_level_cd 
, f.mdm_rep_type_cd
from can_mc_dw.fact_call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as pbrg on pbrg.product_brand_sk = a.product_brand_sk
join can_mc_dw.dim_product_brand as p on p.product_brand_sk = pbrg.product_brand_sk_la
join  can_mc_dw.dim_call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
left join can_mc_dw.dim_curnt_customer_address  as h on a.customer_sk=h.customer_sk
join can_mc_dw.dim_geo_province as i on h.province = i.geo_province_code
join can_mc_dw.dim_employee_role f on a.employee_role_sk = f.employee_role_sk
where date_sk between '20200101' and '20201231'
and  call_attendee_type='Person_Account_vod' 
and h.preferred_alignment = 'D'
and a.customer_sk>0
and (
 (call_subtype_desc='Sales Call' and call_delivery_channel_desc='F2F') or
 (call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='F2F') or
 (call_subtype_desc='Sales Call' and call_delivery_channel_desc='Remote') or
 (call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='F2F')or 
 (call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='Remote') or
 (call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Remote') or
 (call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='F2F')or 
 (call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='Remote') or
 (call_subtype_desc='Sales Call' and call_delivery_channel_desc='Phone') or 
 (call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Phone')or
 (call_subtype_desc='Sales Call' and call_delivery_channel_desc='Email') or 
 (call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Email'))
group by a.customer_sk,call_id,left(date_sk,6),call_subtype_desc,call_delivery_channel_desc,province , geo_province_name ,a.employee_role_sk , f.role_level_cd , f.mdm_rep_type_cd
) a
left join 

(select distinct cst.customer_sk,cst.mdm_customer_id,customer_specialty_group_desc
--into ##temp_spec_grp_fxg
from  can_mc_dw.dim_customer cst
left join 
(select distinct splm.customer_specialty_sk,customer_specialty_group_desc from 
can_mc_dw.dim_curnt_customer_specialty_grouping_to_market splm 
join can_mc_dw.dim_curnt_customer_specialty_group cspg on cspg.customer_specialty_group_sk=splm.customer_specialty_group_sk
where mdm_prod_mkt_id in (select distinct mdm_prod_mkt_id from can_mc_dw.dim_curnt_product_market where mdm_prod_mkt_name='INSULIN MARKET')
)z on cst.customer_specialty_sk=z.customer_specialty_sk
) x on a.customer_sk=x.customer_sk

left join 
(select distinct a.customer_sk,call_id,mdm_rep_type_cd ,
(case when  detail_priority=1 then  mdm_prod_brand_name else '' end) as P1
from can_mc_dw.fact_call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.dim_call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_employee_role f on a.employee_role_sk = f.employee_role_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
where date_sk between '20200101' and '20201231'
and  call_attendee_type='Person_Account_vod' 
and P1 <>''
and call_subtype_desc in ('Sales Call','Lunch & Learn','Remote Lunch & Learn','Remote Sales Call')
and mdm_rep_type_cd='Spec Rep CV'
group by a.customer_sk,call_id,mdm_rep_type_cd,detail_priority,mdm_prod_brand_name 
) b on a.customer_sk=b.customer_sk and a.call_id=b.call_id 

left join 

(select distinct a.customer_sk,call_id,mdm_rep_type_cd,
(case when  detail_priority=2 then  mdm_prod_brand_name else '' end) as P2
from can_mc_dw.fact_call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.dim_call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_employee_role f on a.employee_role_sk = f.employee_role_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
where date_sk between '20200101' and '20201231'
and  call_attendee_type='Person_Account_vod' 
and P2 <>''
and call_subtype_desc in ('Sales Call','Lunch & Learn','Remote Lunch & Learn','Remote Sales Call')
and mdm_rep_type_cd='Spec Rep CV'
group by a.customer_sk,call_id,mdm_rep_type_cd,detail_priority,mdm_prod_brand_name
) c on a.customer_sk=c.customer_sk and a.call_id=c.call_id

left join 

(select distinct a.customer_sk,call_id,mdm_rep_type_cd,
(case when  detail_priority=3 then  mdm_prod_brand_name else '' end) as P3
from can_mc_dw.fact_call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.dim_call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_employee_role f on a.employee_role_sk = f.employee_role_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
where date_sk between '20200101' and '20201231'
and  call_attendee_type='Person_Account_vod' 
and P3 <>''
and call_subtype_desc in ('Sales Call','Lunch & Learn','Remote Lunch & Learn','Remote Sales Call')
and mdm_rep_type_cd='Spec Rep CV'
group by a.customer_sk,call_id,mdm_rep_type_cd,detail_priority,mdm_prod_brand_name
) d on a.customer_sk=d.customer_sk and a.call_id=d.call_id

)
where SALES_DIRECTION like ('%FORXIGA%')
group by customer_sk,Year_Month,SALES_DIRECTION,customer_specialty_group_desc,call_subtype_desc,call_delivery_channel_desc,province , geo_province_name, 
employee_role_sk , role_level_cd  , mdm_rep_type_cd
order by calls desc;

--------

select customer_sk, cast(Year_Month as int) ,mdm_rep_type_cd ,SALES_DIRECTION ,customer_specialty_group_desc,province , geo_province_name,  employee_role_sk , role_level_cd ,
sum( case when call_subtype_desc='Sales Call' and call_delivery_channel_desc='F2F' then calls else 0 end) as SalesCall_f2f,
sum( case when ((call_subtype_desc='Sales Call' and call_delivery_channel_desc='Remote') or
                           (call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='F2F')or 
                           (call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='Remote')) then calls else 0 end) as SalesCall_rem,
sum( case when call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='F2F' then calls else 0 end) as LLCall_f2f,

sum( case when ((call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Remote') or
                           (call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='F2F')or 
                           (call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='Remote')) then calls else 0 end) as LLCall_rem,
sum( case when call_subtype_desc='Sales Call' and call_delivery_channel_desc='Phone' then calls else 0 end) as SalesCall_Phone,
sum( case when call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Phone' then calls else 0 end) as LLCall_Phone,
sum( case when ((call_subtype_desc='Sales Call' and call_delivery_channel_desc='Email') or 
                           (call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Email') ) then calls else 0 end) as EmailCall
into ##temp_CV_call_details
from ##temp_sales_direction_2020_fxg
group by customer_sk, Year_Month ,mdm_rep_type_cd ,SALES_DIRECTION,customer_specialty_group_desc,province , geo_province_name ,employee_role_sk , role_level_cd ;


--select * from ##temp_CV_call_details

-- select mdm_rep_type_cd , employee_role_sk , role_level_cd , 
-- sum(SalesCall_f2f) as SalesCall_f2f , sum(SalesCall_phone) as SalesCall_phone , sum(SalesCall_rem) as SalesCall_rem ,
-- sum(LLCall_f2f) as LLCall_f2f , sum(LLCall_phone) as LLCall_phone , sum(LLCall_rem) as LLCall_rem , sum(EmailCall) as EmailCall
-- from ##temp_CV_call_details
-- group by mdm_rep_type_cd , employee_role_sk , role_level_cd
--------------------------------------------------------------------------------
---- Final PDE Calculation
--------------------------------------------------------------------------------

select mdm_rep_type_cd , employee_role_sk , role_level_cd , 
sum(SalesCall_f2f) as SalesCall_f2f , sum(SalesCall_phone) as SalesCall_phone , sum(SalesCall_rem) as SalesCall_rem ,
sum(LLCall_f2f) as LLCall_f2f , sum(LLCall_phone) as LLCall_phone , sum(LLCall_rem) as LLCall_rem , sum(EmailCall) as EmailCall

from (
select customer_sk, cast(Year_Month as int) , mdm_rep_type_cd,customer_specialty_group_desc,province , geo_province_name, employee_role_sk , role_level_cd ,
((P1_SalesCall_f2f + P1_SalesCall_f2f_CV)*1) as P1_SalesCall_f2f , ((P1_SalesCall_rem + P1_SalesCall_rem_CV)*1) as P1_SalesCall_rem , 
((P1_LLCall_f2f + P1_LLCall_f2f_CV)*1) as P1_LLCall_f2f , ((P1_LLCall_rem + P1_LLCall_rem_CV)*1) as P1_LLCall_rem , 
((P1_SalesCall_Phone + P1_SalesCall_Phone_CV)*1) as P1_SalesCall_Phone , 
((P1_LLCall_Phone + P1_LLCall_Phone_CV)*1) as P1_LLCall_Phone ,
((P1_EmailCall + P1_EmailCall_CV)*1) as P1_EmailCall , 

((P2_SalesCall_f2f + P2_SalesCall_f2f_CV)*1) as P2_SalesCall_f2f , ((P2_SalesCall_rem + P2_SalesCall_rem_CV)*1) as P2_SalesCall_rem , 
((P2_LLCall_f2f + P2_LLCall_f2f_CV)*1) as P2_LLCall_f2f , ((P2_LLCall_rem + P2_LLCall_rem_CV)*1) as P2_LLCall_rem , 
((P2_SalesCall_Phone + P2_SalesCall_Phone_CV)*1) as P2_SalesCall_Phone ,
((P2_LLCall_Phone + P2_LLCall_Phone_CV)*1) as P2_LLCall_Phone ,
((P2_EmailCall + P2_EmailCall_CV)*1) as P2_EmailCall , 

(P1_SalesCall_f2f + P2_SalesCall_f2f) as SalesCall_f2f , (P1_SalesCall_rem + P2_SalesCall_rem) as SalesCall_rem , 
(P1_LLCall_f2f + P2_LLCall_f2f) as LLCall_f2f , (P1_LLCall_rem + P2_LLCall_rem) as LLCall_rem , 
(P1_SalesCall_Phone + P2_SalesCall_Phone) as SalesCall_Phone ,
(P1_LLCall_Phone + P2_LLCall_Phone) as LLCall_Phone ,
(P1_EmailCall + P2_EmailCall) as EmailCall ,

(SalesCall_f2f + SalesCall_rem + LLCall_f2f + LLCall_rem + SalesCall_Phone +LLCall_Phone) as Total_Call_2020

--into ##temp_pde_subcat_2020_fxg
from
(select  customer_sk, cast(Year_Month as int) ,mdm_rep_type_cd,customer_specialty_group_desc,province , geo_province_name,SALES_DIRECTION, employee_role_sk , role_level_cd , 

-- P1 Calls by Diabetes Sales Teams for Forxiga
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then SalesCall_f2f  else 0 end) as P1_SalesCall_f2f,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then SalesCall_rem  else 0 end) as P1_SalesCall_rem,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then LLCall_f2f  else 0 end) as P1_LLCall_f2f,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then LLCall_rem  else 0 end) as P1_LLCall_rem,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then SalesCall_Phone  else 0 end) as P1_SalesCall_Phone,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then LLCall_Phone  else 0 end) as P1_LLCall_Phone,
sum( case when mdm_rep_type_cd='Diabetes PC Rep' OR mdm_rep_type_cd='Diabetes Spec Rep' then EmailCall else 0 end) as P1_EmailCall,

-- P2 Calls by Respiratory and other Sales Teams for Forxiga
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then SalesCall_f2f  else 0 end) as P2_SalesCall_f2f,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then SalesCall_rem  else 0 end) as P2_SalesCall_rem,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then LLCall_f2f  else 0 end) as P2_LLCall_f2f,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then LLCall_rem  else 0 end) as P2_LLCall_rem,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then SalesCall_Phone  else 0 end) as P2_SalesCall_Phone,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then LLCall_Phone  else 0 end) as P2_LLCall_Phone,
sum( case when mdm_rep_type_cd='Inhaled Resp PC' OR mdm_rep_type_cd='Pharmacy Rep' OR mdm_rep_type_cd='CSR' OR mdm_rep_type_cd='UNKNOWN' then EmailCall else 0 end) as P2_EmailCall,

-- P1 Calls by CardioVascular Sales Teams , just for Cardiology as HCP Speciality for Forxiga

sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then SalesCall_f2f  else 0 end) as P1_SalesCall_f2f_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then SalesCall_rem  else 0 end) as P1_SalesCall_rem_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then LLCall_f2f  else 0 end) as P1_LLCall_f2f_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then LLCall_rem  else 0 end) as P1_LLCall_rem_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then SalesCall_Phone  else 0 end) as P1_SalesCall_Phone_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then LLCall_Phone  else 0 end) as P1_LLCall_Phone_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and ((customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%LOKELMA%') and SALES_DIRECTION not like ('%BRILINTA%')) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION = 'FORXIGA,,')) then EmailCall else 0 end) as P1_EmailCall_CV,

-- P2 Calls by CardioVascular Sales Teams , for any other HCP Speciality except Cardiology for Forxiga

sum( case when mdm_rep_type_cd='Spec Rep CV' and (
(customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then SalesCall_f2f  else 0 end) as P2_SalesCall_f2f_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (
  (customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then SalesCall_rem  else 0 end) as P2_SalesCall_rem_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (
(customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then LLCall_f2f  else 0 end) as P2_LLCall_f2f_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (
(customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then LLCall_rem  else 0 end) as P2_LLCall_rem_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (
(customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then SalesCall_Phone  else 0 end) as P2_SalesCall_Phone_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (
(customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then LLCall_Phone  else 0 end) as P2_LLCall_Phone_CV,
sum( case when mdm_rep_type_cd='Spec Rep CV' and (
(customer_specialty_group_desc<>'CARDIOLOGY' or customer_specialty_group_desc is null) or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION not like ('%LOKELMA%') and SALES_DIRECTION <> 'FORXIGA,,'))or
(customer_specialty_group_desc='CARDIOLOGY' and SALES_DIRECTION like ('%BRILINTA%') ) then EmailCall else 0 end) as P2_EmailCall_CV

from ( 

select a.customer_sk, cast(Year_Month as int) ,mdm_rep_type_cd ,SALES_DIRECTION,customer_specialty_group_desc,province , geo_province_name, employee_role_sk , role_level_cd , 
SalesCall_f2f , SalesCall_rem , LLCall_f2f , LLCall_rem , SalesCall_Phone,LLCall_Phone,EmailCall from 
(select  a.customer_sk,left(date_sk,6) as Year_Month, mdm_rep_type_cd ,'' as SALES_DIRECTION, province , geo_province_name,a.employee_role_sk ,f.mdm_az_role_id , f.role_level_cd ,
count( distinct case when call_subtype_desc='Sales Call' and call_delivery_channel_desc='F2F' then call_id else null end) as SalesCall_f2f,
count( distinct case when call_subtype_desc='Sales Call' and call_delivery_channel_desc='Phone' then call_id else null end) as SalesCall_Phone,
count( distinct case when ((call_subtype_desc='Sales Call' and call_delivery_channel_desc='Remote') or
                           (call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='F2F')or 
                           (call_subtype_desc='Remote Sales Call' and call_delivery_channel_desc='Remote')) then call_id else null end) as SalesCall_rem,

count( distinct case when call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='F2F' then call_id else null end) as LLCall_f2f,
count( distinct case when call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Phone' then call_id else null end) as LLCall_Phone,
count( distinct case when ((call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Remote') or
                           (call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='F2F')or 
                           (call_subtype_desc='Remote Lunch & Learn' and call_delivery_channel_desc='Remote')) then call_id else null end) as LLCall_rem,
                  
count( distinct case when ((call_subtype_desc='Sales Call' and call_delivery_channel_desc='Email') or 
                           (call_subtype_desc='Lunch & Learn' and call_delivery_channel_desc='Email') ) then call_id else null end) as EmailCall

from can_mc_dw.fact_call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.dim_call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.dim_customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_employee_role f on a.employee_role_sk = f.employee_role_sk
join can_mc_dw.dim_call_delivery_channel g on g.call_delivery_channel_sk = a.call_delivery_channel_sk
left join can_mc_dw.dim_curnt_customer_address  as h on a.customer_sk=h.customer_sk
join can_mc_dw.dim_geo_province as i on h.province = i.geo_province_code
 
where (c.mdm_prod_brand_name like '%FORXIGA%')   --- product_brand_sk_la to change when we are calculating sales for different brands
and  call_attendee_type='Person_Account_vod' 
and h.preferred_alignment = 'D'
and  a.customer_sk>0 and date_sk between '20200101' and '20201231'
and mdm_rep_type_cd<>'Spec Rep CV'
group by a.customer_sk,left(date_sk,6), mdm_rep_type_cd,province , geo_province_name,SALES_DIRECTION , a.employee_role_sk ,f.mdm_az_role_id , f.role_level_cd) a
left join 
(select distinct cst.customer_sk,cst.mdm_customer_id,customer_specialty_group_desc
from  can_mc_dw.dim_customer cst
left join 
(select distinct splm.customer_specialty_sk,customer_specialty_group_desc from 
can_mc_dw.dim_curnt_customer_specialty_grouping_to_market splm 
join can_mc_dw.dim_curnt_customer_specialty_group cspg on cspg.customer_specialty_group_sk=splm.customer_specialty_group_sk
where mdm_prod_mkt_id in (select distinct mdm_prod_mkt_id from can_mc_dw.dim_curnt_product_market where mdm_prod_mkt_name='INSULIN MARKET')
)z on cst.customer_specialty_sk=z.customer_specialty_sk
) b
on a.customer_sk=b.customer_sk
union
select * from ##temp_CV_call_details
)
group by customer_sk, Year_Month ,  mdm_rep_type_cd , customer_specialty_group_desc , province , geo_province_name, SALES_DIRECTION , employee_role_sk , role_level_cd ))
group by mdm_rep_type_cd , employee_role_sk , role_level_cd ;

--- Total PDE master
select * from ##temp_pde_subcat_2020_fxg;
