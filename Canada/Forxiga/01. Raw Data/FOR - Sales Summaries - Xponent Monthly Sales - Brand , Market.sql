--- Month wise Forxiga Family Sales
--- For L3M and L12M consider quarterly and 12th month values only
select 'FORXIGA FAMILY' as Brand, month_period_ends_sk,
sum( case when metric_type_sk=1 and time_period_sk=1 then volume else null end) as fxg_trx_MTH ,
--sum( case when metric_type_sk=1 and time_period_sk=2 then volume else null end) as fxg_trx_L3M ,
--sum( case when metric_type_sk=1 and time_period_sk=3 then volume else null end) as fxg_trx_L12M ,
sum( case when metric_type_sk=2 and time_period_sk=1 then volume else null end) as fxg_nrx_MTH 
--sum( case when metric_type_sk=2 and time_period_sk=2 then volume else null end) as fxg_nrx_L3M ,
--sum( case when metric_type_sk=2 and time_period_sk=3 then volume else null end) as fxg_nrx_L12M
from can_mc_dw.fact_xponent_brand as a --- table for brand level trx/nrx
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join  can_mc_dw.dim_customer_source  as cs on a.customer_source_sk=cs.customer_source_sk 
where ((c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%') and month_period_ends_sk between 201901 and 202012 --- product_brand_sk_la to change when we are calculating sales for different brands
and cs.customer_sk > 0 ) 
group by month_period_ends_sk , Brand
order by month_period_ends_sk desc;

--- Monthly Province Level Xponent Sales

--2020
select 'FORXIGA FAMILY' AS Brand ,province,geo_province_name, month_period_ends_sk , sum(TRX_2020) as Total_TRX_2020, sum(NRX_2020) as Total_NRX_2020 from (
select cs.customer_sk , ca.province , prov.geo_province_name , month_period_ends_sk , 
sum(case when (metric_type_sk = 2 AND month_period_ends_sk like '2020%') then volume else null end) as NRX_2020,
sum(case when (metric_type_sk = 1 AND month_period_ends_sk like '2020%') then volume else null end) as TRX_2020
from can_mc_dw.fact_xponent_brand as a --- table for brand level trx/nrx
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.dim_customer_source  as cs on a.customer_source_sk=cs.customer_source_sk 
join can_mc_dw.dim_curnt_customer_address  as ca on cs.customer_sk=ca.customer_sk 
join can_mc_dw.dim_geo_province as prov on ca.province = prov.geo_province_code
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%') and --- product_brand_sk_la to change when we are calculating sales for different brands
ca.preferred_alignment = 'D'
and cs.customer_sk > 0
and time_period_sk=1  ---- time_period_sk -- 1 = monthly, 2=L3M, 3=L12M, 4=YTD
and (month_period_ends_sk like '2020%')  --------to filter based on date/month/year in combination with time_period_sk
group by cs.customer_sk , ca.province ,prov.geo_province_name, month_period_ends_sk)
group by province , geo_province_name , month_period_ends_sk ;

--2019
select 'FORXIGA FAMILY' AS Brand ,province,geo_province_name, month_period_ends_sk , sum(TRX_2019) as Total_TRX_2019, sum(NRX_2019) as Total_NRX_2019 from (
select cs.customer_sk , ca.province , prov.geo_province_name , month_period_ends_sk , 
sum(case when (metric_type_sk = 2 AND month_period_ends_sk like '2019%') then volume else null end) as NRX_2019,
sum(case when (metric_type_sk = 1 AND month_period_ends_sk like '2019%') then volume else null end) as TRX_2019
from can_mc_dw.fact_xponent_brand as a --- table for brand level trx/nrx
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.dim_customer_source  as cs on a.customer_source_sk=cs.customer_source_sk 
join can_mc_dw.dim_curnt_customer_address  as ca on cs.customer_sk=ca.customer_sk 
join can_mc_dw.dim_geo_province as prov on ca.province = prov.geo_province_code
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%') and --- product_brand_sk_la to change when we are calculating sales for different brands
ca.preferred_alignment = 'D'
and cs.customer_sk > 0
and time_period_sk=1  ---- time_period_sk -- 1 = monthly, 2=L3M, 3=L12M, 4=YTD
and (month_period_ends_sk like '2019%')  --------to filter based on date/month/year in combination with time_period_sk
group by cs.customer_sk , ca.province ,prov.geo_province_name, month_period_ends_sk)
group by province , geo_province_name , month_period_ends_sk ;


----- Market Sales

select distinct month_period_ends_sk,
sum( case when metric_type_sk=1 and time_period_sk=1 and month_period_ends_sk like '2020%' then volume else null end) as mkt_trx_2020_MTH ,
sum( case when metric_type_sk=2 and time_period_sk=1 and month_period_ends_sk like '2020%' then volume else null end) as mkt_nrx_2020_MTH ,
sum( case when metric_type_sk=1 and time_period_sk=1 and month_period_ends_sk like '2019%' then volume else null end) as mkt_trx_2019_MTH ,
sum( case when metric_type_sk=2 and time_period_sk=1 and month_period_ends_sk like '2019%' then volume else null end) as mkt_nrx_2019_MTH 
from can_mc_dw.fact_xponent_brand as a --- table for brand level trx/nrx
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join  can_mc_dw.customer_source  as cs on a.customer_source_sk=cs.customer_source_sk 
where c.mdm_prod_brand_name in (select distinct mdm_prod_brand_name from can_mc_dw.dim_product_brand where product_molecule_sk in (
select distinct product_molecule_sk from can_mc_dw.dim_product_MOLECULE where product_class_sk in (
SELECT distinct product_class_sk from  can_mc_dw.dim_product_class where mdm_prod_class_name='SGLT-2'))) --- product_brand_sk_la to change when we are calculating sales for different brands
and cs.customer_sk > 0
and (month_period_ends_sk like '2020%' or month_period_ends_sk like '2019%' )
group by month_period_ends_sk;
