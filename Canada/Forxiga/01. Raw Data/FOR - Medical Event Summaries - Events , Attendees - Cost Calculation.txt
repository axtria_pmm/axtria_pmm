
----MEDICAL EVENTS FOR FORXIGA FAMILY IN 2020
-------------- Number of Medical Events (by event type , attendee status) - by year month - 2020
----- excluding customer_sk = -2 while counting medical events

select distinct medical_event_type_name ,medical_event_status_desc,Year_Month,medical_event_id , event_id ,attendee_customer_sk ,medical_event_attendee_status_desc
from(
select *
from
(
SELECT *,left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%' )  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%' )
--OR date_sk like '2019%' ) --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values 
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
)
where medical_event_status_desc like '%Completed%' ;------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED
--and medical_event_attendee_status_desc like '%Attended%';  ------ FILTERING ON MEDICAL EVENT ATTENDEE STATUS : ATTENDED



select distinct medical_event_type_name ,medical_event_status_desc,Year_Month,medical_event_id , event_id ,medical_event_attendee_status_desc
from(
select *
from
(
SELECT *,left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%' )  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%' )
--OR date_sk like '2019%' ) --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values 
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
)
where medical_event_status_desc like '%Completed%' ;------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED
--and medical_event_attendee_status_desc like '%Attended%';  ------ FILTERING ON MEDICAL EVENT ATTENDEE STATUS : ATTENDED

