
----MEDICAL EVENTS FOR FORXIGA FAMILY IN 2020/2019
-------------- Number of Medical Events (by event type) - by year month - 2020 and 2019
----- excluding customer_sk = -2 while counting medical events
--- keeping events with 1 or more attendees with status 'Attended'
--- 'Physician Organization Accredited Meeting' , 'HCP Initiated Rounds' , 'HCP Initiated Journal Club' , 'Preceptorship' NOT INCLUDED in the medical events or attendees
Select Year_Month ,
( AZC_Non_Accredited_Events + AZ_Non_Accredited_Events + HCP_Learning_Activity_Events + FMOQ_Accredited_Events + AZC_Accredited_Events + AZ_Accredited_Events ) as Total_Medical_Events from (
select Year_Month ,---- to view the data at attendee level
count(case when medical_event_type_name = 'AZC Initiated Non-Accredited Meeting' then medical_event_id else null end) as AZC_Non_Accredited_Events, -- AZC Initiated Non-Accredited Meeting
count(case when medical_event_type_name = 'AZ Initiated Non-Accredited Meeting' then medical_event_id else null end) as AZ_Non_Accredited_Events, -- AZ Initiated Non-Accredited Meeting
count(case when medical_event_type_name = 'HCP Initiated Learning Activity' then medical_event_id else null end) as HCP_Learning_Activity_Events, -- HCP Initiated Learning Activity
count(case when medical_event_type_name = 'FMOQ Accredited Meeting' then medical_event_id else null end) as FMOQ_Accredited_Events, -- FMOQ Accredited Meeting
count(case when medical_event_type_name = 'AZ Initiated Accredited Meeting' then medical_event_id else null end) as AZ_Accredited_Events, -- AZ Initiated Accredited Meeting 
/* ABOVE DOESN'T EXISTS FOR 2019 , 2020 */
count(case when medical_event_type_name = 'AZC Initiated Accredited Meeting' then medical_event_id else null end) as AZC_Accredited_Events, -- AZC Initiated Accredited Meeting
count(case when medical_event_type_name = 'HCP Initiated Rounds' then medical_event_id else null end) as HCP_Initiated_Rounds_Events, -- HCP Initiated Rounds
count(case when medical_event_type_name = 'HCP Initiated Journal Club' then medical_event_id else null end) as HCP_Journal_Club_Events, -- HCP Initiated Journal Club
count(case when medical_event_type_name = 'Physician Organization Accredited Meeting' then medical_event_id else null end) as Physician_Organization_Accredited_Events, -- Physician Organization Accredited Meeting
count(case when medical_event_type_name = 'Preceptorship' then medical_event_id else null end) as Preceptorship -- Preceptorship
from ##temp_medical_events_occurence
group by Year_Month 
order by Year_Month 
) ; -- This table has medical events by the date of occurence and status of the event. The query is given below.

select distinct medical_event_type_name ,medical_event_status_desc,Year_Month,medical_event_id into ##temp_medical_events_occurence
from(
select *
from
(
SELECT *,left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%' )  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%' OR date_sk like '2019%' ) --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values 
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
)
where medical_event_status_desc like '%Completed%' ------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED
and medical_event_attendee_status_desc like '%Attended%';  ------ FILTERING ON MEDICAL EVENT ATTENDEE STATUS : ATTENDED

----- NUMBER OF ATTENDEES BY EVENT TYPE - YEAR 2020 , 2019
---- excluding customer_sk = -2
----- Counting attendees multiple times if attendeed multiple events
--- 'Physician Organization Accredited Meeting' , 'HCP Initiated Rounds' , 'HCP Initiated Journal Club' , 'Preceptorship' NOT INCLUDED in the medical events or attendees

select Year_Month, 
( AZC_Non_Accredited_Attendees + AZ_Non_Accredited_Attendees + HCP_Learning_Activity_Attendees + FMOQ_Accredited_Attendees + AZC_Accredited_Attendees + AZ_Accredited_Attendees ) as Total_Attendees from (
select Year_Month,
count(case when medical_event_type_name = 'AZC Initiated Non-Accredited Meeting' then mdm_customer_id else null end) as AZC_Non_Accredited_Attendees, -- AZC Initiated Non-Accredited Meeting
count(case when medical_event_type_name = 'HCP Initiated Learning Activity' then mdm_customer_id else null end) as HCP_Learning_Activity_Attendees, -- HCP Initiated Learning Activity
count(case when medical_event_type_name = 'AZ Initiated Non-Accredited Meeting' then mdm_customer_id else null end) as AZ_Non_Accredited_Attendees, -- AZ Initiated Non-Accredited Meeting
count(case when medical_event_type_name = 'FMOQ Accredited Meeting' then mdm_customer_id else null end) as FMOQ_Accredited_Attendees, -- FMOQ Accredited Meeting
/* BELOW EXISTS only for 2018 */
count(case when medical_event_type_name = 'AZ Initiated Accredited Meeting' then mdm_customer_id else null end) as AZ_Accredited_Attendees, -- AZ Initiated Accredited Meeting,
/* ABOVE EXISTS only for 2018 */
count(case when medical_event_type_name = 'AZC Initiated Accredited Meeting' then mdm_customer_id else null end) as AZC_Accredited_Attendees -- AZC Initiated Accredited Meeting
from (
select distinct medical_event_type_name , date_sk,Year_Month
,mdm_customer_id,attendee_customer_sk ,medical_event_id
from(
select *
from
(
SELECT * , left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%' )  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%' OR date_sk like '2019%') --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
left join can_mc_dw.customer as cst 
on a.attendee_customer_sk=cst.customer_sk
)
where medical_event_status_desc like '%Completed%' and  ------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED
medical_event_attendee_status_desc like '%Attended%'and  ------ FILTERING ON MEDICAL EVENT ATTENDEE STATUS : ATTENDED
speaker_flag = 0 ) --- to include HCPs which were attendees at the event and not speakers
group by  Year_Month 
order by Year_Month );


--------------EVENTS WITH THEIR COUNT OF ATTENDEES (DATA TO BE MOVED TO AN EXCEL AND COUNT 
------------------------Year 2020/2019

select distinct medical_event_type_name ,medical_event_status_desc,Year_Month,medical_event_id, attendee_customer_sk  , medical_event_attendee_status_desc 
from(
select *
from
(
SELECT *,left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%' )  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%' OR date_sk like '2019%') --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values 
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
)
where medical_event_status_desc like '%Completed%';  ------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED


----- Unique number of attendees - THIS DOESN'T EXCLUDE MEDICAL EVENTS TO EXCLUDED TYPES LIKE HCP JOURNAL ETC.

select Year_Month, count(mdm_customer_id) as Attendees
from (
select distinct Year_Month
,mdm_customer_id,attendee_customer_sk
from(
select *
from
(
SELECT * , left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%' )  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%' OR date_sk like '2019%') --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
left join can_mc_dw.customer as cst 
on a.attendee_customer_sk=cst.customer_sk
)
where medical_event_status_desc like '%Completed%' and  ------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED
medical_event_attendee_status_desc like '%Attended%'and  ------ FILTERING ON MEDICAL EVENT ATTENDEE STATUS : ATTENDED
speaker_flag = 0 ) --- to include HCPs which were attendees at the event and not speakers
group by  Year_Month
order by Year_Month ;

