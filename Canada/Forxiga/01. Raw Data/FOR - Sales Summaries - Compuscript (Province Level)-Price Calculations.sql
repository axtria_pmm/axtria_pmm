--- COMPUSCRIPT
------------------- 'FORXIGA FAMILY'  - YEAR 2019/2020 - BY YEAR
-- 2020
select distinct 'FORXIGA FAMILY' AS Brand, geo_province_code,  geo_province_name,  sum(trx) as TRX_2020 , sum(nrx) as NRX_2020 , sum(eutrx) as EUTRX_2020 , sum(eunrx) as EUNRX_2020 , sum(ims_actual_demand_dollar) as Retail_Demand_Sales_2020
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx , ims_actual_demand_dollar
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a  
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2020%' -------to filter based on month/year
group by geo_province_code, geo_province_name 
order by geo_province_code; 


-- 2019
select distinct 'FORXIGA FAMILY' AS Brand, geo_province_code,  geo_province_name,  sum(trx) as TRX_2019 , sum(nrx) as NRX_2019 , sum(eutrx) as EUTRX_2019 , sum(eunrx) as EUNRX_2019 , sum(ims_actual_demand_dollar) as Retail_Demand_Sales_2019
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx , ims_actual_demand_dollar
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a  
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2019%' -------to filter based on month/year
group by geo_province_code, geo_province_name 
order by geo_province_code; 

------------------- 'FORXIGA FAMILY'  - YEAR 2019/2020 - BY MONTH
-- 2020
select distinct 'FORXIGA FAMILY' AS Brand, geo_province_code,  geo_province_name,month_sk,  sum(trx) as TRX_2020 , sum(nrx) as NRX_2020 , sum(eutrx) as EUTRX_2020 , sum(eunrx) as EUNRX_2020 , sum(ims_actual_demand_dollar) as Retail_Demand_Sales_2020
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx , ims_actual_demand_dollar
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a  
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2020%' -------to filter based on month/year
group by geo_province_code, geo_province_name ,month_sk
order by geo_province_code; 


-- 2019
select distinct 'FORXIGA FAMILY' AS Brand, geo_province_code,  geo_province_name, month_sk, sum(trx) as TRX_2019 , sum(nrx) as NRX_2019 , sum(eutrx) as EUTRX_2019 , sum(eunrx) as EUNRX_2019 , sum(ims_actual_demand_dollar) as Retail_Demand_Sales_2019
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx , ims_actual_demand_dollar
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a  
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2019%' -------to filter based on month/year
group by geo_province_code, geo_province_name ,month_sk
order by geo_province_code; 
