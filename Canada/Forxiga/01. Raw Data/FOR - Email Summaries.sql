--- Forxiga Family - Year 2020

select 'FORXIGA FAMILY' AS Brand ,left(email_activity_date_sk, LEN(email_activity_date_sk) - 2) as Year_Month,
count(case when email_activity_status_sk=1 then email_id else null end) as FOR_clicked_vod, 
count(case when email_activity_status_sk=5 then email_id else null end) as FOR_opened_vod,
count(case when email_activity_status_sk=6 then email_id else null end) as FOR_delivered_vod
FROM(
select email_date_sk , customer_sk , email_status_sk , min(email_activity_date_sk) as email_activity_date_sk , email_activity_status_sk , email_id
from can_mc_dw.email_activity a
JOIN can_mc_dw.dim_brg_product_brand b ON b.product_brand_sk = a.product_brand_sk
JOIN can_mc_dw.dim_product_brand c ON c.product_brand_sk = b.product_brand_sk_la
where customer_sk>0 
AND (email_activity_date_sk like '2020%' OR email_activity_date_sk like '2019%')
and (mdm_prod_brand_name like '%FORXIGA%' OR mdm_prod_brand_name like '%XIGDUO%')
group by email_date_sk,customer_sk,email_status_sk,email_activity_status_sk,email_id)
group by Year_Month 
order by Year_Month;

---- Reached HCPs - 2020 - Based on Delivered Mails

select 'FORXIGA FAMILY' AS Brand , count(customer_sk) as Reached_HCPs from(
select customer_sk ,sum(FOR_delivered_vod) as sum_delivered_mails from
(select customer_sk ,left(email_activity_date_sk, LEN(email_activity_date_sk) - 2) as Year_Month,
count(case when email_activity_status_sk=1 then email_id else null end) as FOR_clicked_vod, 
count(case when email_activity_status_sk=5 then email_id else null end) as FOR_opened_vod,
count(case when email_activity_status_sk=6 then email_id else null end) as FOR_delivered_vod
FROM(
select email_date_sk , customer_sk , email_status_sk , min(email_activity_date_sk) as email_activity_date_sk , email_activity_status_sk , email_id
from can_mc_dw.email_activity a
JOIN can_mc_dw.dim_brg_product_brand b ON b.product_brand_sk = a.product_brand_sk
JOIN can_mc_dw.dim_product_brand c ON c.product_brand_sk = b.product_brand_sk_la
where customer_sk>0 
AND email_activity_date_sk like '2020%'  
and (mdm_prod_brand_name like '%FORXIGA%' OR mdm_prod_brand_name like '%XIGDUO%')
group by email_date_sk,customer_sk,email_status_sk,email_activity_status_sk,email_id)
group by Year_Month , customer_sk 
order by Year_Month , customer_sk 
)
group by customer_sk )
where sum_delivered_mails > 0;

---- Reached HCPs - 2019 - Based on Delivered Mails

select 'FORXIGA FAMILY' AS Brand , count(customer_sk) as Reached_HCPs from(
select customer_sk ,sum(FOR_delivered_vod) as sum_delivered_mails from
(select customer_sk ,left(email_activity_date_sk, LEN(email_activity_date_sk) - 2) as Year_Month,
count(case when email_activity_status_sk=1 then email_id else null end) as FOR_clicked_vod, 
count(case when email_activity_status_sk=5 then email_id else null end) as FOR_opened_vod,
count(case when email_activity_status_sk=6 then email_id else null end) as FOR_delivered_vod
FROM(
select email_date_sk , customer_sk , email_status_sk , min(email_activity_date_sk) as email_activity_date_sk , email_activity_status_sk , email_id
from can_mc_dw.email_activity a
JOIN can_mc_dw.dim_brg_product_brand b ON b.product_brand_sk = a.product_brand_sk
JOIN can_mc_dw.dim_product_brand c ON c.product_brand_sk = b.product_brand_sk_la
where customer_sk>0 
AND email_activity_date_sk like '2019%'  
and (mdm_prod_brand_name like '%FORXIGA%' OR mdm_prod_brand_name like '%XIGDUO%')
group by email_date_sk,customer_sk,email_status_sk,email_activity_status_sk,email_id)
group by Year_Month , customer_sk 
order by Year_Month , customer_sk 
)
group by customer_sk )
where sum_delivered_mails > 0;



------ Email content

select min(email_activity_date_sk) as email_activity_date_sk , customer_sk , email_status_sk ,  a.email_activity_status_sk , email_id  , a.email_approved_document_sk , d.name as Email_Name , e.name as Activity_Status
from can_mc_dw.email_activity a
JOIN can_mc_dw.dim_brg_product_brand b ON b.product_brand_sk = a.product_brand_sk
JOIN can_mc_dw.dim_product_brand c ON c.product_brand_sk = b.product_brand_sk_la
JOIN can_mc_dw.dim_email_approved_document d on a.email_approved_document_sk = d.email_approved_document_sk
JOIN can_mc_dw.dim_email_activity_status e on a.email_activity_status_sk = e.email_activity_status_sk
where customer_sk>0 
AND (email_activity_date_sk like '2020%' OR email_activity_date_sk like '2019%')
and (mdm_prod_brand_name like '%FORXIGA%' OR mdm_prod_brand_name like '%XIGDUO%')
group by customer_sk , email_status_sk ,  a.email_activity_status_sk , email_id  , a.email_approved_document_sk ,  d.name , e.name
