----Brand: Forxiga Family
--- FORXIGA , XIGDUO CALLS

--- Dropping temp tables if they already exist
drop table IF EXISTS ##temp_brand_writer_fxg ;
drop table IF EXISTS ##temp_market_writer_fxg ;
drop table IF EXISTS ##temp_pde_subcat_2020_fxg ;
drop table IF EXISTS ##temp_sample_fxg ;
drop table IF EXISTS ##temp_medical_events_occurence_fxg ;
drop table IF EXISTS ##temp_speaker_prog_2020_fxg ;
drop table IF EXISTS ##temp_emails_fxg ;
drop table IF EXISTS ##temp_all_customer_sk_fxg ;
drop table IF EXISTS ##temp_data_Raw ;

--- Brand volume
--- MTH level sales is considered for monthly Brand Volume 

select distinct cs.customer_sk,--- Metric and time period to change for Rx type and time level
sum( case when metric_type_sk=2 and time_period_sk=1 and month_period_ends_sk like '2020%' then volume else null end) as fxg_nrx_2020_MTH, 
--sum( case when metric_type_sk=2 and time_period_sk=3 and month_period_ends_sk like '202012%' then volume else null end) as fxg_nrx_2020_L12M,
sum( case when metric_type_sk=1 and time_period_sk=1 and month_period_ends_sk like '2020%' then volume else null end) as fxg_trx_2020_MTH 
--,sum( case when metric_type_sk=1 and time_period_sk=3 and month_period_ends_sk like '202012%' then volume else null end) as fxg_trx_2020_L12M
into ##temp_brand_writer_fxg
from can_mc_dw.fact_xponent_bfamily as a --- table for brand level trx/nrx
join
can_mc_dw.dim_brg_product_brand_family as b on a.product_brand_family_sk = b.product_brand_family_sk
join
can_mc_dw.dim_product_brand_family as c on c.product_brand_family_sk = b.product_brand_family_sk_la
join  can_mc_dw.dim_customer_source  as cs on a.customer_source_sk=cs.customer_source_sk 
where (mdm_brand_family_name like '%FORXIGA%') and --- product_brand_sk_la to change when we are calculating sales for different brands
cs.customer_sk > 0 
and ( month_period_ends_sk like '2020%' )
group by cs.customer_sk
--having (fxg_nrx_2020_MTH>0 or fxg_nrx_2020_L12M>0 or fxg_trx_2020_MTH>0 or fxg_trx_2020_L12M>0 );
having (fxg_nrx_2020_MTH>0 or fxg_trx_2020_MTH>0 );
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
---Market volume
---MTH level sales is considered for monthly Market Volume 
--- SGLT-2 considered as market for Forxiga

select cs.customer_sk, --- Metric and time period to change for Rx type and time level
sum( case when metric_type_sk=2 and time_period_sk=1 and month_period_ends_sk like '2020%' then volume else null end) as mkt_nrx_2020_MTH ,
--sum( case when metric_type_sk=2 and time_period_sk=3 and month_period_ends_sk like '202012%' then volume else null end) as mkt_nrx_2020_L12M,
sum( case when metric_type_sk=1 and time_period_sk=1 and month_period_ends_sk like '2020%' then volume else null end) as mkt_trx_2020_MTH
--,sum( case when metric_type_sk=1 and time_period_sk=3 and month_period_ends_sk like '202012%' then volume else null end) as mkt_trx_2020_L12M
into ##temp_market_writer_fxg
FROM can_mc_dw.fact_xponent_class x 
JOIN can_mc_dw.dim_brg_product_class pbrg ON pbrg.product_class_sk = x.product_class_sk
JOIN can_mc_dw.dim_product_class p ON p.product_class_sk = pbrg.product_class_sk_la
join can_mc_dw.dim_customer_source  as cs on x.customer_source_sk=cs.customer_source_sk 
where upper(p.mdm_prod_class_name) = 'SGLT-2' ---- to change for different product class
and cs.customer_sk > 0 
and ( month_period_ends_sk like '2020%' )
group by cs.customer_sk
having (mkt_nrx_2020_MTH>0 or mkt_trx_2020_MTH>0);

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
---PDE - FORXIGA ONLY
---PDE calculated as sum of all call types

select customer_sk , sum(Calls) as total_Calls_2020 
into ##temp_pde_subcat_2020_fxg
from (
select distinct  a.customer_sk , call_subtype_desc ,call_delivery_channel_desc , min(date_sk) as min_date_sk , max(date_sk) as max_date_sk,count(distinct call_id) as Calls , count(distinct mdm_customer_id) as HCPs
from can_mc_dw.call_discussion_priority a
join can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
join can_mc_dw.call_subtype d on a.call_subtype_sk = d.call_subtype_sk
join can_mc_dw.customer_source e on e.customer_sk=a.customer_sk
join can_mc_dw.dim_call_delivery_channel f on f.call_delivery_channel_sk = a.call_delivery_channel_sk
where (c.mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%')  --- product_brand_sk_la to change when we are calculating sales for different brands or c.mdm_prod_brand_name like '%FORXIGA%'
and  call_attendee_type='Person_Account_vod' 
and  a.customer_sk>0 and ( date_sk like '2020%')
group by a.customer_sk,call_subtype_desc ,call_delivery_channel_desc)
group by customer_sk
having total_Calls_2020>0;

----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

--Sample drops

select customer_sk, sum( case when date_sk like '2020%'then sample_drop_qty else null end) as fxg_sample_2020
into ##temp_sample_fxg
from can_mc_dw.call_sample_drop as a
join can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' or c.mdm_prod_package_nm like '%XIGDUO%')   ----- to be changed for identifying a different product package
and customer_sk >0 and date_sk between '20200101' and '20201231'
group by  customer_sk 
having fxg_sample_2020>0;

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

--- Speaker Programs

select distinct medical_event_type_name , attendee_customer_sk , medical_event_id
into ##temp_medical_events_occurence_fxg
from(
select *
from
(
SELECT * , left(date_sk,6) as Year_Month
from
can_mc_dw.medical_event as a
join
can_mc_dw.dim_brg_product_brand as b on a.product_brand_sk = b.product_brand_sk
join
can_mc_dw.dim_product_brand as c on c.product_brand_sk = b.product_brand_sk_la
where (c.mdm_prod_brand_name like '%FORXIGA%' or c.mdm_prod_brand_name like '%XIGDUO%')  --- mdm_prod_brand_name to change when we are calculating for different brands
and (date_sk like '2020%') --- to change as per date/month/year
and attendee_customer_sk > 0 ) as a --- Filtering out invalid customer identifier values
left join
can_mc_dw.medical_event_program as b
on a.medical_event_program_sk = b.medical_event_program_sk
left join
can_mc_dw.medical_event_type as c
on a.medical_event_type_sk = c.medical_event_type_sk -- to know the medical event type
left join
can_mc_dw.medical_event_attendee_status as d
on a.medical_event_attendee_status_sk = d.medical_event_attendee_status_sk -- to know the attendee status for a medical event
left join
can_mc_dw.medical_event_status as e
on a.medical_event_status_sk = e.medical_event_status_sk -- to know the medical event status 
left join can_mc_dw.dim_customer as cst 
on a.attendee_customer_sk=cst.customer_sk
)
where medical_event_status_desc like '%Completed%' and  ------ FILTERING ON MEDICAL EVENT STATUS : COMPLETED (ALL COSTS ENTERED) OR COMPLETED
medical_event_attendee_status_desc like '%Attended%'and  ------ FILTERING ON MEDICAL EVENT ATTENDEE STATUS : ATTENDED
speaker_flag = 0  --- to include HCPs which were attendees at the event and not speakers
;

with a as ( Select attendee_customer_sk ,
( AZC_Non_Accredited_Events + AZ_Non_Accredited_Events + HCP_Learning_Activity_Events + FMOQ_Accredited_Events + AZC_Accredited_Events + AZ_Accredited_Events ) as Total_Medical_Events 
from (
select attendee_customer_sk ,  ---- to view the data at attendee level
count(case when medical_event_type_name = 'AZC Initiated Non-Accredited Meeting' then medical_event_id else null end) as AZC_Non_Accredited_Events, -- AZC Initiated Non-Accredited Meeting
count(case when medical_event_type_name = 'AZ Initiated Non-Accredited Meeting' then medical_event_id else null end) as AZ_Non_Accredited_Events, -- AZ Initiated Non-Accredited Meeting
count(case when medical_event_type_name = 'HCP Initiated Learning Activity' then medical_event_id else null end) as HCP_Learning_Activity_Events, -- HCP Initiated Learning Activity
count(case when medical_event_type_name = 'FMOQ Accredited Meeting' then medical_event_id else null end) as FMOQ_Accredited_Events, -- FMOQ Accredited Meeting
/* BELOW EXISTS only for 2018 */
count(case when medical_event_type_name = 'AZ Initiated Accredited Meeting' then medical_event_id else null end) as AZ_Accredited_Events, -- AZ Initiated Accredited Meeting 
/* ABOVE EXISTS only for 2018 */
count(case when medical_event_type_name = 'AZC Initiated Accredited Meeting' then medical_event_id else null end) as AZC_Accredited_Events, -- AZC Initiated Accredited Meeting
count(case when medical_event_type_name = 'HCP Initiated Rounds' then medical_event_id else null end) as HCP_Initiated_Rounds_Events, -- HCP Initiated Rounds
count(case when medical_event_type_name = 'HCP Initiated Journal Club' then medical_event_id else null end) as HCP_Journal_Club_Events, -- HCP Initiated Journal Club
count(case when medical_event_type_name = 'Physician Organization Accredited Meeting' then medical_event_id else null end) as Physician_Organization_Accredited_Events, -- Physician Organization Accredited Meeting
count(case when medical_event_type_name = 'Preceptorship' then medical_event_id else null end) as Preceptorship -- Preceptorship
from ##temp_medical_events_occurence_fxg
group by attendee_customer_sk 
order by attendee_customer_sk )) -- This table has medical events by the date of occurence and status of the event. The query is given below.

select distinct attendee_customer_sk as customer_sk, sum(total_medical_events) total_medical_events_2020
into ##temp_speaker_prog_2020_fxg
from a 
group by customer_sk
having total_medical_events_2020>0;

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

--- Email Activity - (with non-invitational intent ) - filtering on 'email_approved_document_sk' OR 'name'

with a as(
select email_date_sk,a.customer_sk,email_status_sk,min(email_activity_date_sk) as email_activity_date_sk,email_activity_status_sk,email_id,a.email_approved_document_sk , name 
from can_mc_dw.email_activity a
JOIN can_mc_dw.dim_brg_product_brand b ON b.product_brand_sk = a.product_brand_sk
JOIN can_mc_dw.dim_product_brand c ON c.product_brand_sk = b.product_brand_sk_la
JOIN can_mc_dw.dim_email_approved_document d ON d.email_approved_document_sk = a.email_approved_document_sk
where a.customer_sk>0 AND email_activity_date_sk like '2020%' 
and (mdm_prod_brand_name like '%FORXIGA%' OR c.mdm_prod_brand_name like '%XIGDUO%') 
group by email_date_sk,a.customer_sk,email_status_sk,email_activity_status_sk,email_id,a.email_approved_document_sk , name )
, b as (
select distinct  a.customer_sk,email_approved_document_sk,cast(left(email_activity_date_sk,6) as int) as Year_month,
count(case when email_activity_status_sk=1 then email_id else null end) as fxg_fam_clicked_vod, 
count(case when email_activity_status_sk=5 then email_id else null end) as fxg_fam_opened_vod,
count(case when email_activity_status_sk=6 then email_id else null end) as fxg_fam_delivered_vod 
from a
group by a.customer_sk,email_approved_document_sk , Year_month )

select distinct  b.customer_sk, 
sum(fxg_fam_clicked_vod ) as fxg_fam_clicked_vod , --- Delivered Emails with non-invitational message
sum(fxg_fam_opened_vod ) as fxg_fam_opened_vod , --- Delivered Emails with non-invitational message
sum(fxg_fam_delivered_vod ) as fxg_fam_delivered_vod --- Delivered Emails with non-invitational message
into ##temp_emails_fxg
from b
group by b.customer_sk ;

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

--Union of customers sks to capture all customers

select * into ##temp_all_customer_sk_fxg 
from (
select distinct customer_sk from ##temp_brand_writer_fxg
union
select distinct customer_sk from ##temp_market_writer_fxg
union
select distinct customer_sk from ##temp_pde_subcat_2020_fxg
union
select distinct customer_sk from ##temp_emails_fxg
union
select distinct customer_sk from ##temp_sample_fxg
union
select distinct customer_sk from ##temp_speaker_prog_2020_fxg
) ;


select * from ##temp_all_customer_sk_fxg 

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- Consolidated Table

select distinct a.customer_sk,cst.mdm_customer_id,province ,fxg_nrx_2020_mth, mkt_nrx_2020_mth , fxg_trx_2020_mth, mkt_trx_2020_mth , 
round(cast(fxg_nrx_2020_mth as decimal(10,2))/cast(mkt_nrx_2020_mth as decimal(10,2)),2) as market_share_nrx_mth,
round(cast(fxg_trx_2020_mth as decimal(10,2))/cast(mkt_trx_2020_mth as decimal(10,2)),2) as market_share_trx_mth,
total_Calls_2020 ,fxg_sample_2020 ,total_medical_events_2020, fxg_fam_clicked_vod ,fxg_fam_opened_vod ,fxg_fam_delivered_vod
into ##temp_data_Raw
from ##temp_all_customer_sk_fxg as a
left join ##temp_brand_writer_fxg as b on a.customer_sk=b.customer_sk 
left join ##temp_market_writer_fxg  as c on a.customer_sk=c.customer_sk 
left join ##temp_pde_subcat_2020_fxg   as d on a.customer_sk=d.customer_sk 
left join ##temp_emails_fxg  as e on a.customer_sk=e.customer_sk 
left join ##temp_sample_fxg  as h on a.customer_sk=h.customer_sk 
left join ##temp_speaker_prog_2020_fxg  as j on a.customer_sk=j.customer_sk 
left join can_mc_dw.dim_curnt_customer as cst on a.customer_sk=cst.customer_sk 
left join can_mc_dw.dim_curnt_customer_address cadd on cadd.customer_sk=a.customer_sk
WHERE a.customer_sk>0 and preferred_alignment ='D' 
order by customer_sk;

select * from ##temp_data_Raw;
