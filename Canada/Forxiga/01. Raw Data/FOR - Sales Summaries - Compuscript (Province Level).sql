
--- COMPUSCRIPT
------------------- FORXIGA FAMILY- YEAR 2019/2020 - BY MONTH
-- 2020
select distinct  'FORXIGA FAMILY' AS Brand , geo_province_code,  geo_province_name,  sum(trx) as TRX , sum(nrx) as NRX , sum(eutrx) as EUTRX , sum(eunrx) as EUNRX
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a 
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2020%' -------to filter based on month/year
group by geo_province_code, geo_province_name 
order by trx desc; 


-- 2019
select distinct  'FORXIGA FAMILY' AS Brand , geo_province_code,  geo_province_name,  sum(trx) as TRX , sum(nrx) as NRX , sum(eutrx) as EUTRX , sum(eunrx) as EUNRX
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a 
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2019%' -------to filter based on month/year
group by geo_province_code, geo_province_name 
order by trx desc; 

--- 2020
select distinct  'FORXIGA FAMILY' AS Brand , geo_province_code, month_sk , geo_province_name,  sum(trx) as TRX , sum(nrx) as NRX , sum(eutrx) as EUTRX , sum(eunrx) as EUNRX
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%')  ----- to be changed for identifying a different product package
)as a 
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2020%' -------to filter based on month/year
group by geo_province_code, geo_province_name , month_sk 
order by trx desc; 


-- 2019
select distinct  'FORXIGA FAMILY' AS Brand , geo_province_code, geo_province_name,month_sk ,  sum(trx) as TRX , sum(nrx) as NRX , sum(eutrx) as EUTRX , sum(eunrx) as EUNRX
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%')  ----- to be changed for identifying a different product package
)as a 
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2019%' -------to filter based on month/year
group by geo_province_code, geo_province_name , month_sk
order by trx desc; 



-- 2020 - Month * Province
select distinct  'FORXIGA FAMILY' AS Brand , geo_province_code,  geo_province_name,month_sk, sum(trx) as TRX , sum(nrx) as NRX , sum(eutrx) as EUTRX , sum(eunrx) as EUNRX
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a 
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2020%' -------to filter based on month/year
group by geo_province_code, geo_province_name , month_sk
order by trx desc; 


-- 2019 - Month * Province
select distinct  'FORXIGA FAMILY' AS Brand , geo_province_code,  geo_province_name, month_sk, sum(trx) as TRX , sum(nrx) as NRX , sum(eutrx) as EUTRX , sum(eunrx) as EUNRX
from
(select geo_province_sk , a.product_package_sk , month_sk , trx , nrx , eutrx , eunrx
from
can_mc_dw.fact_compuscript_monthly as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
)as a 
left join can_mc_dw.dim_geo_province as b ------ to fetch province code and name
on a.geo_province_sk=b.geo_province_sk
where month_sk like '2019%' -------to filter based on month/year
group by geo_province_code, geo_province_name ,month_sk
order by trx desc; 
