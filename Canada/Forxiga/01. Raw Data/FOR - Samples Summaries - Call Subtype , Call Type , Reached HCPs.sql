-------SAMPLES DROPPED AT CUSTOMER LEVEL
------------------- FORXIGA FAMILY - YEAR 2019/2020 - BY MONTH

select 'FORXIGA FAMILY' as Brand , Year_Month , sum(sample_drop_qty )as FOR_Sample_Drops 
from(
select customer_sk , Year_Month , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,sample_drop_id , sample_drop_qty
from
(
(select a.* ,c.mdm_prod_package_nm , left(date_sk,6) as Year_Month
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
and customer_sk >0
and (date_sk like '2019%' OR date_sk like '2020%')) as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk)
order by customer_sk , date_sk  )
group by Year_Month
order by Year_Month;

---------

select 'FORXIGA FAMILY' as Brand , Year ,  call_type , call_subtype_desc , sum(sample_drop_qty )as FOR_Sample_Drops 
from(
select customer_sk , Year , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,sample_drop_id , sample_drop_qty
from
(
(select a.* ,c.mdm_prod_package_nm , left(date_sk,4) as Year
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
and customer_sk >0
and (date_sk like '2019%' OR date_sk like '2020%')) as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk)
order by customer_sk , date_sk  )
group by Year ,  call_type , call_subtype_desc
order by Year ,  call_type , call_subtype_desc ;

-----------------------------

select 'FORXIGA FAMILY' as Brand , Year ,  call_type , call_subtype_desc , call_sample_delivery_method_desc , sum(sample_drop_qty )as FOR_Sample_Drops 
from(
select customer_sk , Year , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,sample_drop_id , sample_drop_qty , call_sample_delivery_method_desc
from
(
(select a.* ,c.mdm_prod_package_nm , left(date_sk,4) as Year, d.call_sample_delivery_method_desc
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
join
can_mc_dw.call_sample_delivery_method as d on a.call_sample_delivery_method_sk = d.call_sample_delivery_method_sk
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') ----- to be changed for identifying a different product package
and customer_sk >0
and (date_sk like '2019%' OR date_sk like '2020%')) as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk)
order by customer_sk , date_sk  )
group by Year ,  call_type , call_subtype_desc , call_sample_delivery_method_desc
order by Year ,  call_type , call_subtype_desc , call_sample_delivery_method_desc ;

-----------------------------

--2019
select  'FORXIGA FAMILY' AS Brand ,province , geo_province_name, sum(Sample_Drops_2019) as Total_Samples_Inhalers_2019 
from
( 
select customer_sk ,province, geo_province_name, sum (sample_drop_qty) as Sample_Drops_2019 
from
(
select a.customer_sk ,province, geo_province_name,Year_Month , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,
sample_drop_id , sample_drop_qty
from
(
select a.* ,c.mdm_prod_package_nm , left(date_sk,6) as Year_Month,f.province , g.geo_province_name
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
join can_mc_dw.dim_curnt_customer_address  as f on a.customer_sk=f.customer_sk 
join can_mc_dw.dim_geo_province as g on f.province = g.geo_province_code
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') --- product_brand_sk_la to change when we are calculating sales for different brands
and f.preferred_alignment = 'D'
and a.customer_sk >0
and date_sk like '2019%'
) as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk
order by customer_sk , date_sk  
)
group by customer_sk , province, geo_province_name
order by Sample_Drops_2019 asc 
)
group by province , geo_province_name;

--2020
select  'FORXIGA FAMILY' AS Brand ,province , geo_province_name, sum(Sample_Drops_2020) as Total_Samples_Inhalers_2020
from
( 
select customer_sk ,province, geo_province_name, sum (sample_drop_qty) as Sample_Drops_2020 
from
(
select a.customer_sk ,province, geo_province_name,Year_Month , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,
sample_drop_id , sample_drop_qty
from
(
select a.* ,c.mdm_prod_package_nm , left(date_sk,6) as Year_Month,f.province , g.geo_province_name
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
join can_mc_dw.dim_curnt_customer_address  as f on a.customer_sk=f.customer_sk 
join can_mc_dw.dim_geo_province as g on f.province = g.geo_province_code
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%') --- product_brand_sk_la to change when we are calculating sales for different brands
and f.preferred_alignment = 'D'
and a.customer_sk >0
and date_sk like '2020%'
) as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk
order by customer_sk , date_sk  
)
group by customer_sk , province, geo_province_name
order by Sample_Drops_2020 asc 
)
group by province , geo_province_name;


---------------------------------
---- Reached HCP

--- Year 2020
select * from (
select 'FORXIGA FAMILY' as Brand , customer_sk as reached_customer_sk , sum (sample_drop_qty) as FOR_Sample_Drops_2020 from (
select customer_sk , Year_Month , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,sample_drop_id ,  sample_drop_qty
from
(
(select a.* ,c.mdm_prod_package_nm , left(date_sk,6) as Year_Month
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%')  ----- to be changed for identifying a different product package
and customer_sk >0
and date_sk like '2020%') as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk)
order by customer_sk , date_sk  
)
group by customer_sk
order by FOR_Sample_Drops_2020 asc 
)
where FOR_Sample_Drops_2020 > 0;


--- Year 2019
select * from (
select 'FORXIGA FAMILY' as Brand , customer_sk as reached_customer_sk , sum (sample_drop_qty) as FOR_Sample_Drops_2019 from (
select customer_sk , Year_Month , date_sk , a.product_package_sk , a.mdm_prod_package_nm , call_type , call_subtype_desc  ,sample_drop_id ,  sample_drop_qty
from
(
(select a.* ,c.mdm_prod_package_nm , left(date_sk,6) as Year_Month
from
can_mc_dw.call_sample_drop as a
join
can_mc_dw.dim_brg_product_package as b on a.product_package_sk = b.product_package_sk
join
can_mc_dw.dim_product_package as c on c.product_package_sk = b.product_package_sk_la
where (c.mdm_prod_package_nm like '%FORXIGA%' OR c.mdm_prod_package_nm like '%XIGDUO%')  ----- to be changed for identifying a different product package
and customer_sk >0
and date_sk like '2019%') as a  ---- to be changed as per year/month/date 
left join 
can_mc_dw.call_subtype as b
on a.call_subtype_sk = b.call_subtype_sk)
order by customer_sk , date_sk  
)
group by customer_sk
order by FOR_Sample_Drops_2019 asc 
)
where FOR_Sample_Drops_2019 > 0;

