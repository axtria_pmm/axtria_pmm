neg_exponential_transform <- cmpfun(function(df, formula){
  
  #browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula,
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 1, b = 2, d = .1),
                       start_upper = c(a = 10, b = 50, d = 3),
                       lower = c(a = 0, b = 2, d = -Inf),
                       upper = c(a = Inf, b = 50, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall()
})

##### check_estimate_validity function #########
check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = 2, d = -Inf),
                                           upper_bounds = c(a = Inf, b = 50, d = Inf)){
  #browser()
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})

Samples_Total_2020 <- Transformation_data_cluster %>%
  select(Cluster, Brick, yrmo, Sales_n, samples_n) %>%
  filter(yrmo >= 202001)%>%
  filter(yrmo <= 202109)%>%
  nest(data = -Cluster) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Sales_n ~ a * (1 - exp(-b * samples_n)) + d),
                          .options = furrr_options(scheduling = 2, seed = TRUE)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(Cluster) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-Cluster)

neg_exponential_transform <- cmpfun(function(df, formula){
  
  #browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula,
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 1, b = 0.5, d = .1),
                       start_upper = c(a = 10, b = 30, d = 3),
                       lower = c(a = 0, b = 0.5, d = -Inf),
                       upper = c(a = Inf, b = 30, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall()
})

##### check_estimate_validity function #########
check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = 0.5, d = -Inf),
                                           upper_bounds = c(a = Inf, b = 30, d = Inf)){
  #browser()
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})

Samples_Total_2021 <- Transformation_data_cluster %>%
  select(Cluster, Brick, yrmo, Sales_n, samples_n) %>%
  filter(yrmo >= 202101)%>%
  filter(yrmo <= 202109)%>%
  nest(data = -Cluster) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Sales_n ~ a * (1 - exp(-b * samples_n)) + d),
                          .options = furrr_options(scheduling = 2, seed = TRUE)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(Cluster) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-Cluster)


df$Cluster <- 1
neg_exponential_transform <- cmpfun(function(df, formula){
  
  #browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula,
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 1, b = 0.5, d = .1),
                       start_upper = c(a = 10, b = 200, d = 3),
                       lower = c(a = 0, b =0.5, d = -Inf),
                       upper = c(a = Inf, b = 200, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall()
})

##### check_estimate_validity function #########
check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b =0.5, d = -Inf),
                                           upper_bounds = c(a = Inf, b = 200, d = Inf)){
  #browser()
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})

##### emails- Total #####
Email_Total_2020 <- df %>%
  select(Cluster, Brick, yrmo, Sales_n, email_n) %>%
  filter(yrmo >= 202001)%>%
  filter(yrmo <= 202012)%>%
  nest(data = -Cluster) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Sales_n ~ a * (1 - exp(-b * email_n)) + d),
                          .options = furrr_options(scheduling = 2, seed = TRUE)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(Cluster) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-Cluster)

neg_exponential_transform <- cmpfun(function(df, formula){
  
  #browser()
  #    file_name <- paste0("C:\\Users\\khvr444\\Desktop\\AZ Canada\\Profiling\\Profiling working\\", paste0(paste0(gsub(":", "_", gsub(" ", "_", date())), runif(1)),"iteration_info.txt"))
  #   sink(file = file_name, type = "output", split = T)
  return(nls_multstart(formula = formula,
                       data = df,
                       iter = 350,
                       convergence_count = 100,
                       start_lower = c(a = 1, b = 0.5, d = .1),
                       start_upper = c(a = 10, b = 20, d = 3),
                       lower = c(a = 0, b = 0.5, d = -Inf),
                       upper = c(a = Inf, b = 20, d = Inf),
                       # control = nls.lm.control(maxiter = 200),
                       supp_errors = "Y"))
  
  # sinkall()
})

##### check_estimate_validity function #########
check_estimate_validity <- cmpfun(function(fitted_model, lower_bounds = c(a = 0, b = 0.5, d = -Inf),
                                           upper_bounds = c(a = Inf, b = 20, d = Inf)){
  #browser()
  tidied <- tidy(fitted_model)
  
  if(any(abs(c(lower_bounds - tidied$estimate, upper_bounds - tidied$estimate)) < .000001)){return(FALSE)}
  
  return(TRUE)
})

##### emails- Total #####

Email_Total_2021 <- df %>%
  select(Cluster, Brick, yrmo, Sales_n, email_n) %>%
  filter(yrmo >= 202101)%>%
  filter(yrmo <= 202109)%>%
  nest(data = -Cluster) %>%
  mutate(fit = future_map(data, ~ neg_exponential_transform(.x, formula = Sales_n ~ a * (1 - exp(-b * email_n)) + d),
                          .options = furrr_options(scheduling = 2, seed = TRUE)),
         estimates_valid = map(fit, ~ check_estimate_validity(.x))) %>%
  unnest(estimates_valid) %>%
  mutate(converged_fit = ifelse(estimates_valid == FALSE, list(NULL), fit)) %>%
  #  mutate(Orignal_fit =  map(fit,get_nls_coef(.x))) %>%
  arrange(Cluster) %>%
  fill(converged_fit, .direction = "updown") %>%
  mutate(curvature = map(converged_fit, ~ get_nls_coef(.x))) %>%
  unnest(curvature) %>%
  arrange(-Cluster)

