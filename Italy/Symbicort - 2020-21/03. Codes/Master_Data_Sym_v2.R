getwd()
setwd("C:/Users/kmdc693/OneDrive - AZCollaboration/AZ/Global MMx Project/Italy/2021/PMM/Input Data/MasterData")

rm(list=ls())

output_path <- "C:/Users/kmdc693/OneDrive - AZCollaboration/AZ/Global MMx Project/Italy/2021/PMM/Intermediate/Processed Summaries/Master Data/Symbicort"

############################## Libraries #############################
library(readxl)
library(dplyr)
library(tidyr)
library(stats)


library(furrr)
library(compiler)
library(nls.multstart)

library(furrr)
library(nls.multstart)
library(broom)
library(dplyr)
library(tidyr)
library(tidyverse)
library(lubridate)
library(sparklyr)
library(fastcluster)
library(cluster)
library(nls.multstart)
library(broom)
library(compiler)
library(formula.tools)
library(statmod)
library(lme4)
library(optimx)
library(zoo)
library(RcppEigen)
#library(MASS)
library(corrplot)
library(pls)
library(plm)
library(openxlsx)
library(Hmisc)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(data.table)
library(car)
library(ggplot2)

############## Preparing HCP's attributes master data ################

##### HCP brick mapping #####
hcp_brick_mapping <- read.csv2("ITA_HCP_BRICK.csv", stringsAsFactors = FALSE)
hcp_brick_mapping <- hcp_brick_mapping %>% rename("Customer GUID"="�..HCP")
colnames(hcp_brick_mapping)

##### Bricks alphanumeric to numeric mapping #####
brick_alphanum_to_num_mapping <- read_excel("Brick_Alphanum_to_Num_mapping.xlsx")
brick_alphanum_to_num_mapping <- brick_alphanum_to_num_mapping %>% select(-Brick_Numeric_v2)

##### HCP attributes #####
hcp_info <- read_excel("Customer Detail.xlsx")

##### Populating Alphanumeric Bricks in HCP_info #####
hcp_info <- hcp_info %>% merge(brick_alphanum_to_num_mapping, by.x = "Mini Brck Code (P)",
                               by.y = "Brick_Numeric", all.x=TRUE)

table(is.na(hcp_info$Brick_Alphanumeric))

##### Preparing exhaustive HCP Brick mapping using HCP_Brick_Mapping and HCP_info #####
hcp_brick_mapping_2 <- hcp_info %>% filter(!is.na(Brick_Alphanumeric)) %>% 
  select("Customer GUID","Brick_Alphanumeric") %>%
  rename("BRICK"="Brick_Alphanumeric")

hcp_brick_mapping_3 <- hcp_brick_mapping_2 %>% 
  filter(!`Customer GUID` %in%  hcp_brick_mapping$`Customer GUID`) 

rm(hcp_brick_mapping_2)  

hcp_brick_mapping <- rbind(hcp_brick_mapping, hcp_brick_mapping_3)

rm(hcp_brick_mapping_3)  

hcp_brick_mapping <- hcp_brick_mapping %>% distinct()

##### Brick to Region mapping #####
brick_region_mapping <- read_excel("Brick_Region_Mapping.xlsx")

##### Preparing HCP master data set #####
hcp_master <- hcp_brick_mapping %>% merge(brick_region_mapping, 
                                          by.x = "BRICK", by.y = "Brick", all.x = TRUE)

rm(hcp_brick_mapping, brick_region_mapping, brick_alphanum_to_num_mapping)

table(!is.na(hcp_master$BRICK))
table(!is.na(hcp_master$Region))

hcp_info_filtered <- hcp_info %>% select(`Customer GUID`, `Speciality Name (P)`, `Customer Group`, 
                                         `Customer Type`, `Customer Status`)

hcp_master <- hcp_master %>% merge(hcp_info_filtered, 
                                   by= "Customer GUID", all.x = TRUE)

hcp_master <- hcp_master %>% rename("Brick" = "BRICK")

rm(hcp_info, hcp_info_filtered)

fileName = paste(output_path, 'hcp_master.csv', sep = '')
write.csv(hcp_master, fileName)

############################### Master_Brick_Level ##############################

######### Sales Data #########
sales <- read_excel("Italy_Sales_Sym.xlsx")

# sales_master_brick <- sales %>% select(yrmo, TERRITORY_CODE, PRODUCT_NAME, VALUE) %>% 
#   filter(!PRODUCT_NAME=="R3F_S") %>% 
#   mutate(Sales_Sym= case_when(PRODUCT_NAME=="SYMBICORT" ~ VALUE)) %>%
#   group_by(yrmo, TERRITORY_CODE) %>% 
#   summarise(Sales_Sym = sum(Sales_Sym, na.rm = TRUE), Sales_Mkt= sum(VALUE, na.rm = TRUE)) %>%
#   arrange(yrmo, TERRITORY_CODE) %>%
#   mutate(Sales_Competitors = Sales_Mkt- ifelse(is.na(Sales_Sym),0, Sales_Sym)) %>%
#   rename("Brick"="TERRITORY_CODE")

sales_master_brick <- sales %>% select(yrmo, TERRITORY_CODE, PRODUCT_NAME, VALUE) %>% 
  filter(PRODUCT_NAME %in% c("R3F_S","SYMBICORT"))%>% 
  mutate(Sales_Sym= case_when(PRODUCT_NAME=="SYMBICORT" ~ VALUE)) %>%
  group_by(yrmo, TERRITORY_CODE) %>% 
  summarise(Sales_Sym = sum(Sales_Sym, na.rm = TRUE), Sales_Mkt= sum(VALUE, na.rm = TRUE)) %>%
  arrange(yrmo, TERRITORY_CODE) %>%
  mutate(Sales_Competitors = Sales_Mkt- ifelse(is.na(Sales_Sym),0, Sales_Sym)) %>%
  rename("Brick"="TERRITORY_CODE")

write.csv(sales_master_brick,"Sales_master_brick.csv")

rm(sales)

######### Calls Data #########
call_activity <- read.csv("Call_Activity_Sym_Fxg.csv", stringsAsFactors = FALSE)
call_detail <- read.csv("Call_Detail_Sym_Fxg.csv", stringsAsFactors = FALSE)

call_activity <- call_activity %>% select(Call, Call.Datetime, Call.Status, 
                                          Delivery.Channel, Call.Product.Count)

call_detail_sym <- call_detail %>% filter(Trade.Name == "Symbicort")
rm(call_detail)

call_master <- call_detail_sym %>% merge(call_activity, by = "Call", all.x=TRUE)
rm(call_activity, call_detail_sym)

# to check if there are any un-mapped call
table(is.na(call_master$Call.Status))

call_master <- call_master %>% mutate(yrmo=substr(Call.Datetime, 1, 6),
                                      year = substr(Call.Datetime, 1, 4),
                                      sales_team = substr(REP.Position,1,4))  

call_master$yrmo <- as.numeric(call_master$yrmo)

call_master$sales_team <- gsub("ITBI", "ITBIR", call_master$sales_team)
call_master$sales_team <- gsub("ITDI", "ITDIA", call_master$sales_team)
call_master$sales_team <- gsub("ITLR", "ITLRD", call_master$sales_team)
call_master$sales_team <- gsub("ITRA", "ITRAM", call_master$sales_team)

call_master <- call_master %>% merge(hcp_master %>% select(`Customer GUID`, Brick, `Speciality Name (P)`), 
                                     by.x = "Customer.GUID", by.y= "Customer GUID", all.x = TRUE)

call_master <- call_master %>% 
  mutate(specialty = case_when(`Speciality Name (P)` %in% 
                                 c("General Practice", "Pneumology", "Internal Medicine", "Allergology") ~ 
                                 `Speciality Name (P)`, 
                               !is.na(`Speciality Name (P)`) ~ "Others Spec"))

call_master <- call_master %>% 
  mutate(PDE = case_when(Call.Product.Count==1 & Brand.Priority.Position==1 ~ 1,
                         Call.Product.Count==2 & Brand.Priority.Position==1 ~ 0.6,
                         Call.Product.Count==2 & Brand.Priority.Position==2 ~ 0.4,
                         Call.Product.Count==3 & Brand.Priority.Position==1 ~ 0.5,
                         Call.Product.Count==3 & Brand.Priority.Position==2 ~ 0.3,
                         Call.Product.Count==3 & Brand.Priority.Position==3 ~ 0.2,
                         Call.Product.Count==4 & Brand.Priority.Position==1 ~ 0.5,
                         Call.Product.Count==4 & Brand.Priority.Position==2 ~ 0.3,
                         Call.Product.Count==4 & Brand.Priority.Position==3 ~ 0.15,
                         Call.Product.Count==4 & Brand.Priority.Position==4 ~ 0.05,
                         Call.Product.Count==5 & Brand.Priority.Position==1 ~ 0.5,
                         Call.Product.Count==5 & Brand.Priority.Position==2 ~ 0.3,
                         Call.Product.Count==5 & Brand.Priority.Position==3 ~ 0.1,
                         Call.Product.Count==5 & Brand.Priority.Position==4 ~ 0.05,
                         Call.Product.Count==5 & Brand.Priority.Position==5 ~ 0.05 ),
         Detailing_Position = case_when(Brand.Priority.Position==1 ~ "P1",
                                        Brand.Priority.Position==2 ~ "P2",
                                        Brand.Priority.Position==3 ~ "P3",
                                        Brand.Priority.Position==4 ~ "P4",
                                        Brand.Priority.Position==5 ~ "P5")   )

table(is.na(call_master$PDE))
table(is.na(call_master$Detailing_Position))

call_master <- call_master %>% filter(Delivery.Channel %in% 
                                        c("F2F", "Remote Detailing", "Phone Detailing"))

call_master <- call_master %>% filter(sales_team %in% 
                                        c("ITRS", "ITRP"))

call_target_HCP <- unique(call_master[,c("Brick", "yrmo", "Customer.GUID")])

call_total <- call_master %>% 
  group_by(yrmo, Brick) %>%
  summarise(calls_Total = n_distinct(Call, na.rm = TRUE), PDE_Total = sum(PDE, na.rm = TRUE))

call_by_delivery_channel_1 <- call_master %>% 
  group_by(yrmo, Brick, Delivery.Channel) %>%
  summarise(calls = n_distinct(Call, na.rm = TRUE), PDE = sum(PDE, na.rm = TRUE))

call_by_delivery_channel <- call_by_delivery_channel_1 %>% 
  pivot_wider(names_from = Delivery.Channel, values_from = c(calls, PDE), values_fill= 0)

rm(call_by_delivery_channel_1)

call_by_sales_team_1 <- call_master %>% 
  group_by(yrmo, Brick, sales_team) %>%
  summarise(calls = n_distinct(Call, na.rm = TRUE), PDE = sum(PDE, na.rm = TRUE))

call_by_sales_team <- call_by_sales_team_1 %>% 
  pivot_wider(names_from = sales_team, values_from = c(calls, PDE), values_fill= 0)

rm(call_by_sales_team_1)

call_by_detailing_pos_1 <- call_master %>% 
  group_by(yrmo, Brick, Detailing_Position) %>%
  summarise(calls = n_distinct(Call, na.rm = TRUE), PDE = sum(PDE, na.rm = TRUE))

call_by_detailing_pos <- call_by_detailing_pos_1 %>% 
  pivot_wider(names_from = Detailing_Position, values_from = c(calls, PDE), values_fill= 0)

rm(call_by_detailing_pos_1)

call_by_HCP_spec_1 <- call_master %>% 
  group_by(yrmo, Brick, specialty) %>%
  summarise(calls = n_distinct(Call, na.rm = TRUE), PDE = sum(PDE, na.rm = TRUE))

call_by_HCP_spec_1 <- call_by_HCP_spec_1 %>% filter(!is.na(specialty))

call_by_HCP_spec <- call_by_HCP_spec_1 %>% 
  pivot_wider(names_from = specialty, values_from = c(calls, PDE), values_fill= 0)


call_by_HCP <- call_master %>% 
  filter(yrmo>=202001 & yrmo <= 202109) %>%
  group_by(Brick, specialty) %>%
  summarise(count = n_distinct(Customer.GUID), na.rm = TRUE)

call_by_HCP2 <- call_by_HCP %>% 
  pivot_wider(names_from = specialty, values_from = c(count), values_fill= 0)

write.csv(call_by_HCP2, "called_on.csv")
rm(call_by_HCP_spec_1)

call_master_brick <- call_total %>% 
  left_join(call_by_delivery_channel, by=c("yrmo","Brick")) %>%
  left_join(call_by_sales_team, by=c("yrmo","Brick")) %>%
  left_join(call_by_detailing_pos, by=c("yrmo","Brick")) %>%
  left_join(call_by_HCP_spec, by=c("yrmo","Brick"))

rm(call_total, call_by_delivery_channel, call_by_sales_team, call_by_detailing_pos, call_by_HCP_spec)

## Removing data for un-mapped bricks
call_master_brick <- call_master_brick %>% filter(!is.na(Brick))

## replacing NAs with zero
call_master_brick[is.na(call_master_brick)] <- 0

## Calculating "virtual calls and PDE", "calls and PDE for sales team other than ITRP and ITRS"
call_master_brick <- call_master_brick %>% 
  mutate(calls_Virtual = calls_Total - calls_F2F,
         PDE_Virtual = PDE_Total - PDE_F2F,
         Calls_Exc_ITRP_ITRS = calls_Total - calls_ITRP - calls_ITRS,
         PDE_Exc_ITRP_ITRS = PDE_Total - PDE_ITRP - PDE_ITRS)


######### VAE Data #########

VAE <- read_excel("VAE_Symbicort.xlsx")

VAE_master <- VAE %>% select(yrmo, MDM_CUST_GUID, MicroBrk, id_email, Opened_AZ, Clicked) %>%
  group_by(yrmo, MDM_CUST_GUID, MicroBrk) %>% 
  summarise(email_cnt=n_distinct(id_email, na.rm = TRUE), 
            opened_email=sum(Opened_AZ, na.rm = TRUE), cliked_email= sum(Clicked, na.rm = TRUE))

VAE_master <- VAE_master %>% rename("Brick"="MicroBrk", "Customer GUID"="MDM_CUST_GUID")

## Removing data for un-mapped bricks
VAE_master <- VAE_master %>% filter(!is.na(Brick))
VAE_master$Brick <- paste0("ITA", VAE_master$Brick)

VAE_target_HCP <- unique(data.frame(VAE_master[,c("Brick", "yrmo", "Customer GUID")]))
VAE_target_HCP <- VAE_target_HCP %>% rename("Customer.GUID"="Customer GUID")

VAE_master_brick <- VAE %>% select(yrmo, MicroBrk, id_email, Opened_AZ, Clicked) %>%
  group_by(yrmo, MicroBrk) %>% 
  summarise(email_cnt=n_distinct(id_email, na.rm = TRUE), 
            opened_email=sum(Opened_AZ, na.rm = TRUE), clicked_email= sum(Clicked, na.rm = TRUE)) %>%
  arrange(yrmo, MicroBrk)

VAE_master_brick <- VAE_master_brick %>% rename("Brick"="MicroBrk")

rm(VAE)

## Removing data for un-mapped bricks
VAE_master_brick <- VAE_master_brick %>% filter(!is.na(Brick))
VAE_master_brick$Brick <- paste0("ITA", VAE_master_brick$Brick)


######### Samples Data #########

samples <- read.csv("Samples_Fxg_Sym.csv", stringsAsFactors = FALSE)

samples <- samples %>% 
  filter(Trade.Name=='Symbicort', Call.Status=='Submitted_vod') %>%
  rename("Customer GUID"= "Customer.GUID")

samples_master <- samples %>% merge(hcp_master %>% select(`Customer GUID`, Brick), 
                                    by = "Customer GUID", all.x = TRUE)

rm(samples)

samples_master$Call.Datetime <- as.Date(samples_master$Call.Datetime, format='%m/%d/%Y')

samples_master$yrmo <- as.numeric(format(samples_master$Call.Datetime, format='%Y')) * 100 +
  as.numeric(format(samples_master$Call.Datetime, format='%m'))

samples_target_HCP <- unique(samples_master[,c("Brick", "yrmo", "Customer GUID")])
samples_target_HCP <- samples_target_HCP %>% rename("Customer.GUID"="Customer GUID")

samples_master_brick <- samples_master %>% group_by(yrmo, Brick) %>% 
  summarise(samples=sum(Sample.Quantity, na.rm = TRUE ), 
            calls_with_samples = n_distinct(Call, na.rm = TRUE))


######### Meetings Data #########

events <- read_excel("Events.xlsx")
events <- events %>% filter(`Brand Name`=="SYMBICORT TURBUHALER") %>%
  select(`Event ID`, `Meeting Start Date`)

attendees <- read_excel("Attendees.xlsx")
attendees <- attendees %>% filter(`Brand Name`=="SYMBICORT TURBUHALER", `Meeting Speaker`==0) %>%
  select(`Event ID`, `Customer GUID`)

attendees$HCP_flag <- 1

meetings_master <- events %>% merge(attendees, by = "Event ID", all.x = TRUE)  

rm(events, attendees)

meetings_master$yrmo <- as.numeric(format(meetings_master$`Meeting Start Date`, format = '%Y')) * 100 +
  as.numeric(format(meetings_master$`Meeting Start Date`, format = '%m'))

meetings_master <- meetings_master %>% merge(hcp_master %>% select(`Customer GUID`, Brick),
                                             by = "Customer GUID", all.x = TRUE)

meetings_master_brick <- meetings_master %>% group_by(yrmo, Brick) %>%
  summarise(events=n_distinct(`Event ID`, na.rm = TRUE), attendees= sum(HCP_flag, na.rm = TRUE))


######## HCP counts ########

HCP_by_brick <- hcp_master %>% group_by(Brick) %>% 
  summarise(HCPs=n_distinct(`Customer GUID`, na.rm = TRUE))

HCP_spec_by_brick <- hcp_master %>% 
  filter(`Speciality Name (P)` %in% 
           c("General Practice", "Pneumology", "Internal Medicine", "Allergology")) %>% 
  group_by(Brick) %>% summarise(HCPs_Sym_spec=n_distinct(`Customer GUID`, na.rm = TRUE))

HCP_by_spec_by_brick <- hcp_master %>% 
  filter(`Speciality Name (P)` %in% 
           c("General Practice", "Pneumology", "Internal Medicine", "Allergology")) %>% 
  group_by(Brick, `Speciality Name (P)`) %>% summarise(HCPs=n_distinct(`Customer GUID`, na.rm = TRUE))

HCP_by_spec_by_brick <- HCP_by_spec_by_brick %>% 
  pivot_wider(names_from = `Speciality Name (P)`, values_from = HCPs, values_fill= 0, names_prefix="HCPs_")

HCP_cust_group_by_brick <- hcp_master %>% 
  filter(`Customer Group` %in% c("Specialist", "General Practitioner")) %>% 
  group_by(Brick, `Customer Group`) %>% summarise(HCPs=n_distinct(`Customer GUID`, na.rm = TRUE))

HCP_cust_group_by_brick <- HCP_cust_group_by_brick %>% 
  pivot_wider(names_from = `Customer Group`, values_from = HCPs, values_fill= 0, names_prefix="HCPs_")


Target_HCP <- rbind(call_target_HCP, samples_target_HCP)
Target_HCP <- rbind(Target_HCP, VAE_target_HCP)
Target_HCP <- Target_HCP %>% filter(yrmo >=202001 & yrmo <= 202109)
Target_HCP <- unique(Target_HCP[,c("Brick", "Customer.GUID")])
Target_HCP_count <- Target_HCP %>% group_by(Brick) %>% summarise(Target_HCP = n_distinct(Customer.GUID, na.rm = TRUE))


######## Master Data ############

master_data_sym <- sales_master_brick %>% 
  full_join(call_master_brick, by = c("yrmo", "Brick")) %>%
  full_join(samples_master_brick, by = c("yrmo", "Brick")) %>%
  full_join(VAE_master_brick, by = c("yrmo", "Brick")) %>%
  full_join(meetings_master_brick, by = c("yrmo", "Brick")) %>%
  left_join(HCP_by_brick, by = "Brick") %>%
  left_join(HCP_spec_by_brick, by = "Brick") %>%
  left_join(HCP_cust_group_by_brick, by = "Brick") %>%
  left_join(HCP_by_spec_by_brick, by = "Brick") %>%
  left_join(Target_HCP_count, by = "Brick")

## August month flag
master_data_sym$august_flag <- ifelse(master_data_sym$yrmo %% 100 == 8, 1, 0)

## Removing data for un-mapped bricks
master_data_sym <- master_data_sym %>% filter(!is.na(Brick))

## replacing NAs with zero
master_data_sym
master_data_sym[is.na(master_data_sym)] <- 0

######## Working Days ############

working_days <- read_excel("Italy_Working_Days.xlsx")
master_data_sym <- master_data_sym %>% merge(working_days, by = "yrmo", all.x = TRUE)

write.csv(master_data_sym, "master_data_sym.csv")

