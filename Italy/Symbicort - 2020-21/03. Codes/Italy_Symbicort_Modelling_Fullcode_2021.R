getwd()
setwd("C:/Users/kmdc693/OneDrive - AZCollaboration/AZ/Global MMx Project/Italy/2021/PMM/Input Data/MasterData/Model_Input")
# rm(list=ls())
output_path <- "C:/Users/kmdc693/OneDrive - AZCollaboration/AZ/Global MMx Project/Italy/2021/PMM/Input Data/MasterData/Output"

############################## Libraries #############################
library(readxl)
library(dplyr)
library(tidyr)
library(stats)
library(scales)

library(furrr)
library(compiler)
library(nls.multstart)

library(furrr)
library(nls.multstart)
library(broom)
library(dplyr)
library(tidyr)
library(tidyverse)
library(lubridate)
library(sparklyr)
library(fastcluster)
library(cluster)
library(nls.multstart)
library(broom)
library(compiler)
library(formula.tools)
library(statmod)
library(lme4)
library(optimx)
library(zoo)
library(RcppEigen)
#library(MASS)
library(corrplot)
library(pls)
library(plm)
library(openxlsx)
library(Hmisc)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(data.table)
library(car)
library(ggplot2)



############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################

setwd("C:/Users/kmdc693/OneDrive - AZCollaboration/AZ/Global MMx Project/Italy/2021/PMM/Input Data/MasterData/Output")
Modelling_Dataset1 <- read.csv("Modelling_Dataset_Cluster_v10_11.1.csv", stringsAsFactors =  FALSE)

Modelling_Dataset1$Price_Change = ifelse(Modelling_Dataset1$yrmo >= 202103 ,1,0)

for(i in length(C))
  Correlation_data<- Modelling_Dataset1 %>% 
  select(Sales_n, Total_PDEs_n, Total_PDEs_exp, PDEs_F2F_n, F2F_PDEs_exp, PDEs_Virtual_n, Virtual_PDEs_exp, 
         PDEs_ITRP_n, ITRP_PDEs_exp, PDEs_ITRS_n, ITRS_PDEs_exp,
         samples_n, Total_Sample_exp, email_n, Total_Email_exp, opened_email_n, Opened_Email_exp,
         google_mobility, apple_mobility, cases, august_flag.x, 
         growth, Comp_sales)

Correlation_matrix_2 <- data.frame(cor(Correlation_data))
# view(Correlation_matrix_2)

#### Modelling 

colnames(Modelling_Dataset1)
Modelling_Dataset <- Modelling_Dataset1
Modelling_Dataset$normalization_variable <- 1
original_variable = "Sales_Sym"
dependent_variable = "Sales_n"

normalization_variable = "normalization_variable"
monthly_normalization = "norm_var"
Modelling_Dataset$covid <- ((Modelling_Dataset$apple_mobility))
Modelling_Dataset$seasonality <- Modelling_Dataset$august_flag.x
Modelling_Dataset$trend <- Modelling_Dataset$growth
Modelling_Dataset$Price_Change <- Modelling_Dataset$Price_Change

mth_norm = Modelling_Dataset[,c("yrmo",monthly_normalization)]
mth_norm = mth_norm %>% distinct()

mth_count <- mth_norm
mth_count$yrmo <- seq.int(nrow(mth_count)) 
mth_norm_wide <- as.data.frame(mth_count %>%
                                 pivot_wider(names_from = yrmo,values_from=c(norm_var)))

# "Total_Sample_exp" ,"Total_Email_exp" , "Opened_Email_exp"
# "ITRP_PDEs_exp", "ITRS_PDEs_exp", "Total_PDEs_exp" , "Virtual_PDEs_exp","F2F_PDEs_exp"
# "covid"

for(i in length(C))
  Correlation_data_Modelling<- Modelling_Dataset %>%
  filter(yrmo>=202001 & yrmo <= 202109) %>%
  select(Sales_n,Total_PDEs_exp,ITRS_PDEs_exp,ITRP_PDEs_exp,Total_Sample_exp,Total_Email_exp,Opened_Email_exp,covid,seasonality,trend)

Correlation_matrix_3 <- data.frame(cor(Correlation_data_Modelling))

##################################9 Months Time period##############################

start_T2=202101
end_T2=202109

Modelling_Dataset_T2 <- Modelling_Dataset %>% filter(yrmo >= start_T2 & yrmo <=end_T2)

###########################################
## Fixed Models
promotions = c("Total_PDEs_exp" , "trend","covid","Price_Change")
var_not_carry <- c("trend","covid","Price_Change")
var_not_norm <- c("trend","covid","Price_Change")

formula_x <- paste(dependent_variable , paste(paste(dependent_variable,1,sep="_"),paste(dependent_variable,2,sep="_"),paste(promotions, collapse = "+"),sep = "+") ,sep="~")

Sales_lower <- 0.1
mean(Modelling_Dataset_T2$Sales_n) *Sales_lower
sales_lag_sum_T2 <- 0.8542
# 0.8542

cluster_estimates_NLS_T2 <- Modelling_Dataset_T2 %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(PRM3(dataset = .x,formula_x,start_T2,end_T2,sales_lag_sum_T2)) %>%
                 select(term, estimate)) %>%
  ungroup %>%
  pivot_wider(names_from = term,values_from=c(estimate))

names(cluster_estimates_NLS_T2) <- gsub(x = names(cluster_estimates_NLS_T2), pattern = "A_", replacement = "") 

cluster_estimates_NLS_T2<-cluster_estimates_NLS_T2 %>% 
  rename(intercept=First_intercept)

cluster_estimates_T2 <-Modelling_Dataset_T2 %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(lm(as.formula(formula_x)
                         , data = .x)) %>%
                 select(term, estimate)) %>%
  ungroup %>%
  mutate(term=ifelse(term=='(Intercept)','intercept',term))%>%
  pivot_wider(names_from = term,values_from=c(estimate))

cluster_estimates_T2[c(1),] <- cluster_estimates_NLS_T2[c(1),]

cluster_pvalues_T2 <-Modelling_Dataset_T2 %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(lm(as.formula(formula_x)
                         , data = .x)) %>%
                 select(term, p.value)) %>%
  ungroup %>%
  mutate(term=ifelse(term=='(Intercept)','intercept',term))%>%
  pivot_wider(names_from = term,values_from=c(p.value))

cluster_r_square_T2 <- Modelling_Dataset_T2 %>%
  group_by(Cluster) %>%
  group_modify(~ glance(lm(as.formula(formula_x)
                           , data = .x)) %>%
                 select(r.squared,AIC,BIC)) %>%
  ungroup 


mth_start_T2 = 17
mth_end_T2 = 25
fin_out_T2 = cluster_impact_calc(mth_start_T2,mth_end_T2,Modelling_Dataset, promotions,var_not_carry,var_not_norm,cluster_estimates_T2)

#Actual Impact
s2_T2 = fin_out_T2$output_monthly
trend_actual_T2 = s2_T2[, c("yrmo", "total_predicted", "total_actual")]

paste("Actual_T2",prettyNum(sum(trend_actual_T2$total_actual),big.mark = ','))
paste("Predicted_T2",prettyNum(sum(trend_actual_T2$total_predicted),big.mark = ','))
paste("Error_T2",percent(sum(trend_actual_T2$total_predicted) / sum(trend_actual_T2$total_actual) - 1))
trend_actual_T2$total_predicted = unlist(trend_actual_T2$total_predicted)
# dev.off()
ggplot(trend_actual_T2, aes(yrmo)) +
  geom_line(aes(y = total_predicted, colour = "Predicted")) +
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

actual_impact_T2  = cbind(fin_out_T2$impact_percent,percent(fin_out_T2$impact_percent$percent))
colnames(actual_impact_T2) = c(colnames(fin_out_T2$impact_percent), "%")
view(actual_impact_T2[,c(1,2,4)])
paste("Error_T2",percent(fin_out_T2$non_normalized_error))


#Normalised Impact
s1_T2 = fin_out_T2$output_monthly_norm
trend_T2 = s1_T2[, c("yrmo", "total_norm_predicted", "total_norm_actual")]

paste("Norm_Actual_T2",prettyNum(sum(trend_T2$total_norm_actual),big.mark = ','))
paste("Norm_Predicted_T2",prettyNum(sum(trend_T2$total_norm_predicted),big.mark = ','))
paste("Norm_Error_T2",percent(sum(trend_T2$total_norm_predicted) / sum(trend_T2$total_norm_actual) - 1))
trend_T2$total_norm_predicted = unlist(trend_T2$total_norm_predicted)
# dev.off()
ggplot(trend_T2, aes(yrmo)) +
  geom_line(aes(y = total_norm_predicted, colour = "Predicted")) +
  geom_line(aes(y = total_norm_actual, colour = "Actual")) +
  expand_limits(y = 0)

normalised_impact_T2 = cbind(fin_out_T2$impact_norm_percent,percent(fin_out_T2$impact_norm_percent$percent))
colnames(normalised_impact_T2) = c(colnames(fin_out_T2$impact_norm_percent), "%")
(normalised_impact_T2[,c(1,2,4)])
paste("Norm_Error_T2",percent(fin_out_T2$normalized_error))

########### Residual Model - 1 ############

Samples_c_T1 <- c(0.5,3.5,0.5,2.5,1.5)
# c(1,4,1,3,2)
Samples_c_T2 <- c(13.1,23.1,17.1,23.1,19.1)
# c(12,22,16,22,18)
for (i in unique(Modelling_Dataset$Cluster))
{
  Modelling_Dataset$Total_Sample_exp1a <- 1-(exp(-Modelling_Dataset$samples_n*Samples_c_T1[i]))
  Modelling_Dataset$Total_Sample_exp1b <- 1-(exp(-Modelling_Dataset$samples_n*Samples_c_T2[i]))
}

Modelling_Dataset$Total_Sample_exp1= ifelse(Modelling_Dataset$yrmo >= start_T1 & Modelling_Dataset$yrmo <= end_T1 ,Modelling_Dataset$Total_Sample_exp1a,
                                            ifelse(Modelling_Dataset$yrmo >= start_T2 & Modelling_Dataset$yrmo <= end_T2 ,Modelling_Dataset$Total_Sample_exp1b,
                                                   Modelling_Dataset$Total_Sample_exp))

promotions_residual <- c("Total_Sample_exp1")
formula_z <- paste(dependent_variable , paste(promotions_residual, collapse = "+") ,sep="~")

variables <- c("Sales_n_1", "Sales_n_2", promotions)

Modelling_Dataset_Residual_T2 <- Modelling_Dataset %>% filter(yrmo >= start_T2 & yrmo <=end_T2)

estimates_T2 <- cluster_estimates_T2
colnames(estimates_T2) = paste(colnames(estimates_T2),"_coef",sep="")
Modelling_Dataset_Residual_T2 = merge(Modelling_Dataset_Residual_T2, estimates_T2 , by.x  = "Cluster" , by.y = "Cluster_coef", all.x = TRUE)

Modelling_Dataset_Residual_T2[,paste("Total","pure_calc",sep="_")] <- 0
for(i in variables)
{
  Modelling_Dataset_Residual_T2[,paste(i,"pure_calc",sep="_")] <- Modelling_Dataset_Residual_T2[,paste(i)] * Modelling_Dataset_Residual_T2[,paste(i,"coef",sep="_")]
  Modelling_Dataset_Residual_T2[,paste("Total","pure_calc",sep="_")] <-  Modelling_Dataset_Residual_T2[,paste("Total","pure_calc",sep="_")] + Modelling_Dataset_Residual_T2[,paste(i,"pure_calc",sep="_")]
}

Modelling_Dataset_Residual_T2$Sales_n_Residual <-  Modelling_Dataset_Residual_T2$Sales_n - (Modelling_Dataset_Residual_T2$Total_pure_calc/(Modelling_Dataset_Residual_T2$Total_pure_calc+Modelling_Dataset_Residual_T2$intercept_coef))*Modelling_Dataset_Residual_T2$Sales_n 

Modelling_Dataset_Residual_T2$Sales_n <- Modelling_Dataset_Residual_T2$Sales_n_Residual
# Modelling_Dataset_Residual_T1[is.na(Modelling_Dataset_Residual_T1)] <- 0

start_T2=202101
end_T2=202109
Modelling_Dataset_Residual_T2_M <- Modelling_Dataset_Residual_T2 %>% filter(yrmo >= start_T2 & yrmo <=end_T2)
promotions_residual <- c("Total_Sample_exp1")
formula_z <- paste(dependent_variable , paste(promotions_residual, collapse = "+") ,sep="~")
###########################################
## Fixed Models

# Add 0 to formula/equation for non-intercept model eg. like calls distribution between F2F and Virtual
# will return coefficients

cluster_estimates_Res_T2 <-Modelling_Dataset_Residual_T2_M %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(lm(as.formula(formula_z)
                         , data = .x)) %>%
                 select(term, estimate)) %>%
  ungroup %>%
  mutate(term=ifelse(term=='(Intercept)','intercept',term))%>%
  pivot_wider(names_from = term,values_from=c(estimate))

# To check P-values
cluster_pvalues_Res_T2 <-Modelling_Dataset_Residual_T2_M %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(lm(as.formula(formula_z)
                         , data = .x)) %>%
                 select(term, p.value)) %>%
  ungroup %>%
  mutate(term=ifelse(term=='(Intercept)','intercept',term))%>%
  pivot_wider(names_from = term,values_from=c(p.value))

cluster_r_square_Res_T2 <- Modelling_Dataset_Residual_T2_M %>%
  group_by(Cluster) %>%
  group_modify(~ glance(lm(as.formula(formula_z)
                           , data = .x)) %>%
                 select(r.squared,AIC,BIC)) %>%
  ungroup

mth_start_T2 = 17
mth_end_T2 = 25

colnames(cluster_estimates_Res_T2)[2] <- "intercept_2"
cluster_estimates_Res_T2a <- cbind(cluster_estimates_T2,cluster_estimates_Res_T2)
cluster_estimates_Res_T2a$intercept <-cluster_estimates_Res_T2a$intercept_2
cluster_estimates_Res_T2a <- cluster_estimates_Res_T2a[-c(9,10)]
all_promotions<- c(promotions, promotions_residual)

### impact calculation
fin_out_Res_T2 = cluster_impact_calc(mth_start_T2,mth_end_T2,Modelling_Dataset,all_promotions,var_not_carry,var_not_norm,cluster_estimates_Res_T2a)

#Actual Impact
s2_Res_T2 = fin_out_Res_T2$output_monthly
trend_actual_Res_T2 = s2_Res_T2[, c("yrmo", "total_predicted", "total_actual")]

paste("Actual_T2",prettyNum(sum(trend_actual_Res_T2$total_actual),big.mark = ','))
paste("Predicted_T2",prettyNum(sum(trend_actual_Res_T2$total_predicted),big.mark = ','))
paste("Error_T2",percent(sum(trend_actual_Res_T2$total_predicted) / sum(trend_actual_Res_T2$total_actual) - 1))
trend_actual_Res_T2$total_predicted = unlist(trend_actual_Res_T2$total_predicted)
# dev.off()
ggplot(trend_actual_Res_T2, aes(yrmo)) +
  geom_line(aes(y = total_predicted, colour = "Predicted")) +
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

actual_impact_Res_T2  = cbind(fin_out_Res_T2$impact_percent,percent(fin_out_Res_T2$impact_percent$percent))
colnames(actual_impact_Res_T2) = c(colnames(fin_out_Res_T2$impact_percent), "%")
view(actual_impact_Res_T2[,c(1,2,4)])
paste("Error_T2",percent(fin_out_Res_T2$non_normalized_error))

####### Residual Model - 2 ##############
Email_c_T1 <- c(rep(10,5))
# Email_c_T1 <- c(rep(1,5))
Email_c_T2 <- c(rep(0.00012,5))
# Email_c_T2 <- c(rep(0.0001,5))
Modelling_Dataset$email_n1 <- Modelling_Dataset$email_cnt + (0.3*dplyr::lag(Modelling_Dataset$email_cnt, 1)) + ((0.3*0.3)*dplyr::lag(Modelling_Dataset$email_cnt, 2))

for (i in unique(Modelling_Dataset$Cluster))
{
  Modelling_Dataset$Total_Email_exp1a <- 1-(exp(-Modelling_Dataset$email_n1*Email_c_T1[i]))
  Modelling_Dataset$Total_Email_exp1b <- 1-(exp(-Modelling_Dataset$email_n1*Email_c_T2[i]))
}

Modelling_Dataset$Total_Email_exp1= ifelse(Modelling_Dataset$yrmo >= start_T1 & Modelling_Dataset$yrmo <= end_T1 ,Modelling_Dataset$Total_Email_exp1a,
                                           ifelse(Modelling_Dataset$yrmo >= start_T2 & Modelling_Dataset$yrmo <= end_T2 ,Modelling_Dataset$Total_Email_exp1b,
                                                  Modelling_Dataset$Total_Email_exp))

promotions_residual_2 <- c("Total_Email_exp1")
formula_a <- paste(dependent_variable , paste(promotions_residual_2, collapse = "+") ,sep="~")

########## 9 Months time frame ###########

##### Residual Models 
# promotions_residual_2 <- c("Total_Email_exp")
# formula_a <- paste(dependent_variable , paste(promotions_residual_2, collapse = "+") ,sep="~")

variables <- c("Sales_n_1", "Sales_n_2", all_promotions)

Modelling_Dataset_Residual_2_T2 <- Modelling_Dataset %>% filter(yrmo >= start_T2 & yrmo <=end_T2)

estimates_T2 <- cluster_estimates_Res_T2a
colnames(estimates_T2) = paste(colnames(estimates_T2),"_coef",sep="")
Modelling_Dataset_Residual_2_T2 = merge(Modelling_Dataset_Residual_2_T2, estimates_T2 , by.x  = "Cluster" , by.y = "Cluster_coef", all.x = TRUE)

Modelling_Dataset_Residual_2_T2[,paste("Total","pure_calc",sep="_")] <- 0
for(i in variables)
{
  Modelling_Dataset_Residual_2_T2[,paste(i,"pure_calc",sep="_")] <- Modelling_Dataset_Residual_2_T2[,paste(i)] * Modelling_Dataset_Residual_2_T2[,paste(i,"coef",sep="_")]
  Modelling_Dataset_Residual_2_T2[,paste("Total","pure_calc",sep="_")] <-  Modelling_Dataset_Residual_2_T2[,paste("Total","pure_calc",sep="_")] + Modelling_Dataset_Residual_2_T2[,paste(i,"pure_calc",sep="_")]
}

Modelling_Dataset_Residual_2_T2$Sales_n_Residual <-  Modelling_Dataset_Residual_2_T2$Sales_n - (Modelling_Dataset_Residual_2_T2$Total_pure_calc/(Modelling_Dataset_Residual_2_T2$Total_pure_calc+Modelling_Dataset_Residual_2_T2$intercept_coef))*Modelling_Dataset_Residual_2_T2$Sales_n 

Modelling_Dataset_Residual_2_T2$Sales_n <- Modelling_Dataset_Residual_2_T2$Sales_n_Residual
# Modelling_Dataset_Residual_T2[is.na(Modelling_Dataset_Residual_T2)] <- 0

start_T2=202101
end_T2=202109

Modelling_Dataset_Residual_2_T2_M <- Modelling_Dataset_Residual_2_T2 %>% filter(yrmo >= start_T2 & yrmo <=end_T2)

for(i in length(C))
  Correlation_data_Modelling<- Modelling_Dataset_Residual_2_T2_M %>%
  filter(yrmo>=start_T2 & yrmo <= end_T2) %>%
  filter(Cluster == 1) %>%
  select(Sales_n,Total_PDEs_exp,ITRS_PDEs_exp,ITRP_PDEs_exp,email_n,Total_Sample_exp,Total_Email_exp,Total_Email_exp1,Opened_Email_exp,covid,seasonality,trend)

Correlation_matrix_Res_T2 <- data.frame(cor(Correlation_data_Modelling))
Modelling_Dataset_Residual_2_T2_M$Cluster <- 1

###########################################
## Fixed Models

# Add 0 to formula/equation for non-intercept model eg. like calls distribution between F2F and Virtual
# will return coefficients
cluster_estimates_Res_2_T2 <-Modelling_Dataset_Residual_2_T2_M %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(lm(as.formula(formula_a)
                         , data = .x)) %>%
                 select(term, estimate)) %>%
  ungroup %>%
  mutate(term=ifelse(term=='(Intercept)','intercept',term))%>%
  pivot_wider(names_from = term,values_from=c(estimate))

# To check P-values
cluster_pvalues_Res_2_T2 <-Modelling_Dataset_Residual_2_T2_M %>%
  group_by(Cluster) %>%
  group_modify(~ tidy(lm(as.formula(formula_a)
                         , data = .x)) %>%
                 select(term, p.value)) %>%
  ungroup %>%
  mutate(term=ifelse(term=='(Intercept)','intercept',term))%>%
  pivot_wider(names_from = term,values_from=c(p.value))

cluster_r_square_Res_2_T2 <- Modelling_Dataset_Residual_2_T2_M %>%
  group_by(Cluster) %>%
  group_modify(~ glance(lm(as.formula(formula_a)
                           , data = .x)) %>%
                 select(r.squared,AIC,BIC)) %>%
  ungroup

mth_start_T2 = 17
mth_end_T2 = 25
promotions_residual <- c("Total_Sample_exp1")
all_promotions<- c(promotions, promotions_residual)

colnames(cluster_estimates_Res_2_T2)[2] <- "intercept_2"
cluster_estimates_Res_2_T2a <- cbind(cluster_estimates_Res_T2a,cluster_estimates_Res_2_T2)
cluster_estimates_Res_2_T2a$intercept <-cluster_estimates_Res_2_T2a$intercept_2
cluster_estimates_Res_2_T2a <- cluster_estimates_Res_2_T2a[-c(10,11)]
all_promotions_2<- c(all_promotions, promotions_residual_2)

### impact calculation
fin_out_Res_2_T2 = cluster_impact_calc(mth_start_T2,mth_end_T2,Modelling_Dataset,all_promotions_2,var_not_carry,var_not_norm,cluster_estimates_Res_2_T2a)

#Actual Impact
s2_Res_2_T2 = fin_out_Res_2_T2$output_monthly
trend_actual_Res_2_T2 = s2_Res_2_T2[, c("yrmo", "total_predicted", "total_actual")]

paste("Actual_T2",prettyNum(sum(trend_actual_Res_2_T2$total_actual),big.mark = ','))
paste("Predicted_T2",prettyNum(sum(trend_actual_Res_2_T2$total_predicted),big.mark = ','))
paste("Error_T2",percent(sum(trend_actual_Res_2_T2$total_predicted) / sum(trend_actual_Res_2_T2$total_actual) - 1))
trend_actual_Res_2_T2$total_predicted = unlist(trend_actual_Res_2_T2$total_predicted)
# dev.off()
ggplot(trend_actual_Res_2_T2, aes(yrmo)) +
  geom_line(aes(y = total_predicted, colour = "Predicted")) +
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

actual_impact_Res_2_T2  = cbind(fin_out_Res_2_T2$impact_percent,percent(fin_out_Res_2_T2$impact_percent$percent))
colnames(actual_impact_Res_2_T2) = c(colnames(fin_out_Res_2_T2$impact_percent), "%")
view(actual_impact_Res_2_T2[,c(1,2,4)])
paste("Error_T2",percent(fin_out_Res_2_T2$non_normalized_error))



####Mixed Effect Model################ 

rm(combined_glmmtb_T2_1,combined_glmmtb_T2_2,combined_glmmtb_T2_3,combined_glmmtb_T2_4,combined_glmmtb_T2_5)
rm(colmeans_T2_1,colmeans_T2_2,colmeans_T2_3,colmeans_T2_4,colmeans_T2_5)

for(i in unique(Modelling_Dataset$Cluster))
{
  data = Modelling_Dataset[which(Modelling_Dataset$yrmo >= start_T2 &
                                   Modelling_Dataset$yrmo <= end_T2 & Modelling_Dataset$Cluster == i), ]
  f_e_formula = Sales_n ~ Sales_n_1 + Sales_n_2 + Total_PDEs_exp + Price_Change+ covid + trend
  r_e_terms = c("Total_PDEs_exp")
  r_e_factor = "Brick"
  r_e_formula = NULL
  get_relative_change = FALSE
  min_distinct_obs = 3
  
  if (any(data %>% select(all.vars(f_e_formula)) %>% summarize_all(sum) == 0)) {
    return(list(NULL))
    
  }
  
  if (is.null(r_e_formula)) {
    r_e_formula <-
      paste0(f_e_formula, paste0(
        " + (1 + ",
        paste0(r_e_terms, collapse = " + "),
        "| r_e_id_internal)"
      ))
  }
  
  
  numeric_ids <- data %>%
    select(r_e_factor) %>%
    distinct %>%
    rowid_to_column("r_e_id_internal")
  
  
  data <- suppressWarnings(
    data %>%
      inner_join(numeric_ids, by = r_e_factor) %>%
      group_by(!!sym(r_e_factor)) %>%
      mutate(
        y_value_count = distinct_value_count(!!sym(lhs.vars(f_e_formula))),
        y_value_nonzero_count = nonzero_value_count(!!sym(lhs.vars(f_e_formula)))
      ) %>%
      ungroup %>%
      mutate(
        r_e_id_internal = r_e_id_internal + 1,
        r_e_id_internal = ifelse(y_value_count < min_distinct_obs, 1, r_e_id_internal),
        r_e_id_internal = ifelse(
          r_e_id_internal == 1 &
            (y_value_nonzero_count < min_distinct_obs),
          0,
          r_e_id_internal
        )
      ) %>%
      select(-y_value_count,-y_value_nonzero_count)
  )
  
  
  original_id_mapping <-
    data %>% select(r_e_factor, r_e_id_internal) %>% distinct
  
  
  # Handle cases where no observations have enough distinct values
  if (length(unique(data$r_e_id_internal)) == 1) {
    return(NULL)
  }
  library(glmmTMB)
  
  rm(test)
  test <-
    glmmTMB(
      Sales_n ~  Sales_n_1 + Sales_n_2 + covid + trend + Price_Change + Total_PDEs_exp +
        (covid + trend +Price_Change+ Total_PDEs_exp+ 1 | r_e_id_internal),
      data = data,
      family = gaussian,
      control = glmmTMBControl(
        optArgs = list(
          optimizer = optim,
          method = "nlminb",
          lower = c(-Inf,cluster_estimates_T2[i,"Sales_n_1"],cluster_estimates_T2[i,"Sales_n_2"],-Inf,-Inf,-Inf,cluster_estimates_T2[i,"Total_PDEs_exp"],-Inf,-Inf,-Inf,-Inf,-Inf),
          upper = c(Inf,cluster_estimates_T2[i,"Sales_n_1"],cluster_estimates_T2[i,"Sales_n_2"],Inf, Inf,Inf,cluster_estimates_T2[i,"Total_PDEs_exp"],Inf,Inf,Inf,Inf,Inf)
        )
      )
    )
  
  coef = coef(test)[[1]]$r_e_id_internal
  # view(coef)
  colMeans(coef)
  cluster_estimates_T1
  combined_glmmtb <- coef(test)[[1]]$r_e_id_internal %>%
    rename(intercept = `(Intercept)`) %>%
    mutate(r_e_id_internal := as.numeric(row.names(.))) %>%
    inner_join(original_id_mapping, by = "r_e_id_internal") %>%
    select(r_e_factor, everything(),-r_e_id_internal) %>%
    mutate(Segment = 1,
           altered = FALSE)
  
  if (any(is.na(fixef(test)))) {
    return(list(NULL))
  }
  
  rand_eff <- ranef(test)
  
  name <- paste("combined_glmmtb_T2", i, sep = "_")
  assign(name,  (as.data.frame(combined_glmmtb)))
  
  name1 <- paste("colmeans_T2", i, sep = "_")
  assign(name1,  as.data.frame(colMeans(coef)))
  
  name2 <- paste("random_effect_T2", i, sep = "_")
  assign(name2,  as.data.frame((rand_eff)))
}

colmeans_T2 <- cbind(colmeans_T2_1,colmeans_T2_2,colmeans_T2_3,colmeans_T2_4,colmeans_T2_5)
(cluster_estimates_T2)
colmeans_T2
combined_glmmtb_T2 <- rbind(combined_glmmtb_T2_1,combined_glmmtb_T2_2,combined_glmmtb_T2_3,combined_glmmtb_T2_4,combined_glmmtb_T2_5)

new_Data_T2 <- merge(Modelling_Dataset[Modelling_Dataset$yrmo>=202009 & Modelling_Dataset$yrmo<=end_T2, ], combined_glmmtb_T2, by = "Brick", all.x = TRUE , all.y = TRUE)
write.xlsx(new_Data_T2,paste(output_path,"Glmm_T2_18.2.xlsx",sep="/"), sheetName = "MixedEffect_Coef")
write.csv(combined_glmmtb_T2, "combined_T2.csv")