
# Setting up working directory --------------------------------------------
setwd("C:/Users/kqll413/Box Sync/Global/03. Data collection/UK/Symbicort a2z")

# Loding libraries --------------------------------------------------------
library(geosphere)
library(stringdist)
library(stringr)
library(tm)
library(sp)
library(rgdal)
library(geosphere)
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(stringr)
library(splitstackshape)


# Variables for data preparation ------------------------------------------
start = 201806
end = 201912

distance_cutoff = 5000

d = 5000
# Sales data --------------------------------------------------------------

# Setting up directory
setwd("C:/Users/kqll413/Box Sync/Global/03. Data collection/UK/Symbicort a2z/Raw data")

# Summarizing Packs
ics_laba_packs_2019 = read.xlsx('ICS LABA 2019.xlsx',sheet = 'Packs')

ics_laba_packs_2019_2 =  melt(ics_laba_packs_2019, id = c("Practice.ID","LHE.Name","Practice.NHS.ID","Brand","Pack"),
                              variable.name = "yrmo",value.name = "packs")

ics_laba_packs_2019_2 = ics_laba_packs_2019_2 %>% filter(packs > 0)

ics_laba_packs_2019_2$abc = as.character(ics_laba_packs_2019_2$yrmo)
ics_laba_packs_2019_2$def = as.integer(ics_laba_packs_2019_2 $abc)
ics_laba_packs_2019_2$date = as.Date(ics_laba_packs_2019_2$def, origin = "1899-12-30")

ics_laba_packs_2019_2$yrmo = as.numeric(format(ics_laba_packs_2019_2$date, "%Y"))*100+as.numeric(format(ics_laba_packs_2019_2$date, "%m"))

ics_laba_packs_2019_2$abc=NULL
ics_laba_packs_2019_2$def=NULL
ics_laba_packs_2019_2$date=NULL

sym_practice_mtly_packs_2019 = ics_laba_packs_2019_2 %>% filter(Brand == "Symbicort") %>% group_by(Practice.ID,yrmo) %>% 
  summarise(packs = sum(packs))

market_practice_mtly_packs_2019 = ics_laba_packs_2019_2 %>% group_by(Practice.ID,yrmo) %>% 
  summarise(packs = sum(packs))

# 2018 sales data

ics_laba_packs_2018 = read.xlsx('NHS_Jul-Dec_2018 v3.xlsx',sheet = "packs")

ics_laba_packs_2018_2 = ics_laba_packs_2018 %>% filter(ICS.Laba==1)
ics_laba_packs_2018_2$MARKET_NAME=NULL
ics_laba_packs_2018_2$ICS.Laba=NULL

ics_laba_packs_2018_3 =  melt(ics_laba_packs_2018_2, id = c("Practice.ID","LHE.Name","Practice.NHS.ID","Brand"),
                              variable.name = "yrmo",value.name = "packs")

ics_laba_packs_2018_3 = ics_laba_packs_2018_3 %>% filter(packs > 0)

ics_laba_packs_2018_3$abc = as.character(ics_laba_packs_2018_3$yrmo)
ics_laba_packs_2018_3$def = as.integer(ics_laba_packs_2018_3 $abc)
ics_laba_packs_2018_3$date = as.Date(ics_laba_packs_2018_3$def, origin = "1899-12-30")

ics_laba_packs_2018_3$yrmo = as.numeric(format(ics_laba_packs_2018_3$date, "%Y"))*100+as.numeric(format(ics_laba_packs_2018_3$date, "%m"))

ics_laba_packs_2018_3$abc=NULL
ics_laba_packs_2018_3$def=NULL
ics_laba_packs_2018_3$date=NULL

sym_practice_mtly_packs_2018H2 = ics_laba_packs_2018_3 %>% filter(Brand == "SYMBICORT") %>% group_by(Practice.ID,yrmo) %>% 
  summarise(packs = sum(packs))

market_practice_mtly_packs_2018H2 = ics_laba_packs_2018_3 %>% group_by(Practice.ID,yrmo) %>% 
  summarise(packs = sum(packs))

sym_practice_mtly_packs_2018H2_2019 = bind_rows(sym_practice_mtly_packs_2018H2,sym_practice_mtly_packs_2019)
colnames(sym_practice_mtly_packs_2018H2_2019)[3] = "symbicort_packs"

market_practice_mtly_packs_2018H2_2019 = bind_rows(market_practice_mtly_packs_2018H2,market_practice_mtly_packs_2019)
colnames(market_practice_mtly_packs_2018H2_2019)[3] = "ics_laba_packs"

# Calls data --------------------------------------------------------------

product = 'Symbicort_GB(RIA_GB'

calls_non_pharma_2019 = read.csv('Calls Jan_Dec 2019.csv',stringsAsFactors = FALSE)
calls_non_pharma_2018h2 = read.csv('Calls Jul_Dec 2018.csv',stringsAsFactors = FALSE)
calls_pharma = read.csv('Calls pharma Jul-18 Jul-20.csv',stringsAsFactors = FALSE)

calls_overall = rbind(calls_non_pharma_2019,calls_non_pharma_2018h2,calls_pharma)

calls_overall$dp = str_remove_all(calls_overall$Detailed.Products,"No Product_GB\\(RIA_GB\\)")
calls_overall$dp = str_remove_all(calls_overall$dp,"No Product_GB\\(Onc_GB\\)")
calls_overall$dp = str_remove_all(calls_overall$dp,"No Product_GB\\(Diabetes_GB\\)")
calls_overall$dp = str_remove_all(calls_overall$dp,"No Product_GB\\(CV_GB\\)")

calls_overall_2 = calls_overall %>% filter(str_detect(dp,"Symbicort"))
calls_overall_2 = cSplit(calls_overall_2,"dp", ")",drop = FALSE)

calls_overall_2$pde = if_else(calls_overall_2$dp_1 == product,1,
                              if_else(calls_overall_2$dp_2 == product,0.5,
                                      if_else(calls_overall_2$dp_3 == product,0.1,0)))

calls_overall_2$newdate = as.Date(calls_overall_2$Datetime, "%d/%m/%Y %H:%M")

calls_overall_2$yrmo = as.numeric(format(calls_overall_2$newdate, "%Y"))*100+as.numeric(format(calls_overall_2$newdate, "%m"))

# Filtering for selected time period
calls_overall_2 = calls_overall_2 %>% filter(yrmo >= start & yrmo <= end)

# Sale team mapping to territory
sales_teams = calls_overall_2 %>% select(Territory) %>% distinct()

write.csv(sales_teams,"sales_teams.csv",row.names = FALSE)

sales_team_mapping = read.csv('sales_teams V2.csv',stringsAsFactors = FALSE)

calls_overall_3 = merge(calls_overall_2,sales_team_mapping,by = "Territory",all.x = TRUE)

# Filtering of data

calls_overall_3$filter = if_else(calls_overall_3$sales.team == 'GBAZEX' & calls_overall_3$X1.1.Call == 0,1,0)

calls_overall_3$filter = if_else(calls_overall_3$pde >0 ,calls_overall_3$filter,1)

calls_overall_4 = calls_overall_3 %>% filter(filter==0)

calls_overall_4$cost = if_else(is.na(calls_overall_4$Actual.Costs),0,calls_overall_4$Actual.Costs)

calls_overall_4$rep_led_meeting = if_else(calls_overall_4$cost > 0 , 1, 0)

# calls_overall_4 %>% filter(rep_led_meeting == 1 & yrmo>=201901 & yrmo <= 201912) %>% summarise(n=n())
# 
# calls_overall_4 %>% filter(rep_led_meeting == 1 & yrmo>=201901 & yrmo <= 201912) %>% summarise(n=n())
# 
# calls_overall_4 %>% filter(yrmo>=201901 & yrmo <= 201912) %>% group_by(sales.team) %>% summarise(n=n())
# 
# calls_overall_4 %>% filter(rep_led_meeting == 0 & yrmo>=201901 & yrmo <= 201912) %>% group_by(sales.team) %>% summarise(n=n())

# Affiliation mappings

affiliations = read.xlsx('Customers to Organisation 2020 07 15.xlsx')

calls_overall_5 = merge(calls_overall_4, affiliations[,c("Child.Account:.Amazon.MDM.ID","Data.Provider.ID")],by.x = "Account..Amazon.MDM.ID",
                        by.y = "Child.Account:.Amazon.MDM.ID",all.x = TRUE)

colnames(calls_overall_5)[ncol(calls_overall_5)] = "Data_provider_id_from_hcp"

affiliations_2 = affiliations %>% distinct(Amazon.MDM.ID,Data.Provider.ID)

calls_overall_6 = merge(calls_overall_5, affiliations_2,by.x = "Account..Amazon.MDM.ID",by.y = "Amazon.MDM.ID",all.x = TRUE)

calls_overall_6$practice_id = if_else(is.na(calls_overall_6$Data_provider_id_from_hcp),calls_overall_6$Data.Provider.ID,calls_overall_6$Data_provider_id_from_hcp)

calls_overall_6$practice_id = if_else(calls_overall_6$Call..Record.Type == "Pharmacy Call",calls_overall_6$Account..Data.Provider.ID,calls_overall_6$practice_id)

practice_mtly_calls_overall = calls_overall_6 %>% group_by(practice_id,yrmo) %>% summarise(calls = n(),pde = sum(pde))

practice_mtly_rep_led_meeting_attendees = calls_overall_6 %>% filter(rep_led_meeting==1 & sales.team == "GBRT/S/A") %>% 
  group_by(practice_id,yrmo) %>% summarise(calls = n(),pde = sum(pde))
colnames(practice_mtly_rep_led_meeting_attendees)[3:4] = c("rep_led_meeting_attendee_calls","rep_led_meeting_attendee_pde")

practice_mtly_gbrs_t_a_normal_call = calls_overall_6 %>% filter(rep_led_meeting==0 & sales.team == "GBRT/S/A") %>% 
  group_by(practice_id,yrmo) %>% summarise(calls = n(),pde = sum(pde))
colnames(practice_mtly_gbrs_t_a_normal_call)[3:4] = c("gbrs_t_a_normal_calls","gbrs_t_a_normal_pde")

practice_mtly_gbred_normal_call = calls_overall_6 %>% filter(rep_led_meeting==0 & sales.team == "GBRED") %>% 
  group_by(practice_id,yrmo) %>% summarise(calls = n(),pde = sum(pde))
colnames(practice_mtly_gbred_normal_call)[3:4] = c("gbred_normal_calls","gbred_normal_pde")

practice_mtly_gbay_z_normal_call = calls_overall_6 %>% filter(rep_led_meeting==0 & sales.team == "GBAY/Z") %>% 
  group_by(practice_id,yrmo) %>% summarise(calls = n(),pde = sum(pde))
colnames(practice_mtly_gbay_z_normal_call)[3:4] = c("gbay_z_normal_calls","gbay_z_normal_pde")

practice_mtly_gbcs_azex_normal_call = calls_overall_6 %>% filter(rep_led_meeting==0 & (sales.team == "GBCS" | sales.team == "GBAZEX")) %>% 
  group_by(practice_id,yrmo) %>% summarise(calls = n(),pde = sum(pde))
colnames(practice_mtly_gbcs_azex_normal_call)[3:4] = c("gbcs_azex_normal_calls","gbcs_azex_normal_pde")

# sum(practice_mtly_gbrs_t_a_normal_call[which(practice_mtly_gbrs_t_a_normal_call$yrmo>= 201901),c("calls")])


# Speaker Meetings --------------------------------------------------------

meeting = read.xlsx('Meeting.xlsx',startRow = 3,sheet = 1)
meeting_product = read.xlsx('Meeting Product Symbicort.xlsx',startRow = 3,sheet = 1)
meeting_attendee = read.xlsx('Meeting Attendee.xlsx',startRow = 3,sheet = 1)
meeting_cost = read.xlsx('Meeting cost.xlsx',startRow = 3,sheet = 1)
meeting_cost = meeting_cost %>% group_by(Meeting_ID) %>% summarise(Meeting_Actual_Cost = sum(Meeting_Actual_Cost),Planned_Cost = sum(Planned_Cost))

meeting_filter = meeting %>% filter(Meeting_Status == 'Completed (all costs entered)' | Meeting_Status == 'Completed')
meeting_attendee_filter = meeting_attendee %>% filter(Attendee_Type == 'Account' & Attendee_Status == 'Signed' | Attendee_Type == 'Account' & Attendee_Status == 'Attended')

meeting_master = merge(x = meeting_product[,c(1,3)],y=meeting_filter[,c(1,6,7,8,11,12,14)],by = "Meeting_ID")

meeting_master$date = as.Date(meeting_master$Meeting_End_Date,origin="1899-12-30")
meeting_master$yrmo = as.numeric(format(meeting_master$date, "%Y"))*100+as.numeric(format(meeting_master$date, "%m"))

meeting_master_vf = merge(x = meeting_master,y=meeting_attendee_filter[,c(5,7,8)],by = "Meeting_ID")
meeting_master_vf = merge(x = meeting_master_vf,y=meeting_cost,by = "Meeting_ID",all.x = TRUE)

### Physician/Account info
customer_detail = read.xlsx('Customer Detail.xlsx',startRow = 3,sheet = 1)

meetings_consolidated_2 = merge(x = meeting_master_vf, y = customer_detail[,c(4,5,6,10,11,12,15,16,24,26,29,31)], by="Customer_GUID",all.x = TRUE)

meetings_consolidated_3 = meetings_consolidated_2 %>% filter(yrmo>=start & yrmo <= end & Mini_Brck_Code != "0" & Meeting_Type == "Speaker Meeting")

meetings_consolidated_4 = merge(x=meetings_consolidated_3,
                                y=affiliations[,c("Child.Account:.Amazon.MDM.ID","Data.Provider.ID")],
                                by.x = "Customer_GUID",by.y = "Child.Account:.Amazon.MDM.ID",all.x = TRUE)

practice_mtly_speaker_program_attendees = meetings_consolidated_4 %>% group_by(Data.Provider.ID,yrmo) %>% summarise(attendees = n())


# VAE Emails --------------------------------------------------------------

Veeva_approved_emails = read.xlsx('Veeva approved mail Symbicort.xlsx',startRow = 3,sheet = 1)

Veeva_approved_emails_filter = Veeva_approved_emails %>% filter(Mail_Status=="Delivered")
Veeva_approved_emails_filter$Mail_Sent_Date = as.numeric(Veeva_approved_emails_filter$Mail_Sent_Date)
Veeva_approved_emails_filter$sent_date = as.Date(Veeva_approved_emails_filter$Mail_Sent_Date,origin="1899-12-30")
Veeva_approved_emails_filter$sent_yrmo = as.numeric(format(Veeva_approved_emails_filter$sent_date, "%Y"))*100+as.numeric(format(Veeva_approved_emails_filter$sent_date, "%m"))

### Physician/Account info
VAE_consolidated_2 = merge(x = Veeva_approved_emails_filter, y = customer_detail[,c(4,5,6,10,11,12,15,16,24,26,29,31)], by="Customer_GUID",all.x = TRUE)

VAE_consolidated_3 = VAE_consolidated_2 %>% filter(sent_yrmo>=start & sent_yrmo <= end & Mini_Brck_Code != "0")

VAE_consolidated_4 = merge(x=VAE_consolidated_3,
                           y=affiliations[,c("Child.Account:.Amazon.MDM.ID","Data.Provider.ID")],
                           by.x = "Customer_GUID",by.y = "Child.Account:.Amazon.MDM.ID",all.x = TRUE)

vae_sent = VAE_consolidated_4 %>% filter(sent_yrmo>=start & sent_yrmo <= end & Mini_Brck_Code != '0') %>% 
  group_by(Data.Provider.ID,sent_yrmo) %>% summarise(veeva_emails_sent = n_distinct(Sent_Mail_ID))
colnames(vae_sent) = c("practice_id","yrmo","vae_emails_sent")

vae_open = VAE_consolidated_4 %>% filter(sent_yrmo>=start & sent_yrmo <= end & Mini_Brck_Code!= '0' & Total_Opens > 0) %>% 
  group_by(Data.Provider.ID,sent_yrmo) %>% summarise(veeva_emails_sent = n_distinct(Sent_Mail_ID))
colnames(vae_open) = c("practice_id","yrmo","vae_emails_open")

vae_clicks = VAE_consolidated_4 %>% filter(sent_yrmo>=start & sent_yrmo <= end & Mini_Brck_Code != '0' & Total_Clicks > 0) %>% 
  group_by(Data.Provider.ID,sent_yrmo) %>% summarise(veeva_emails_sent = n_distinct(Sent_Mail_ID))
colnames(vae_clicks) = c("practice_id","yrmo","vae_emails_click")


# Master data -------------------------------------------------------------

colnames(sym_practice_mtly_packs_2018H2_2019)[1] = "practice_id"
colnames(market_practice_mtly_packs_2018H2_2019)[1] = "practice_id"
colnames(practice_mtly_speaker_program_attendees)[1] = "practice_id"

# List of practices

practice_list = bind_rows(sym_practice_mtly_packs_2018H2_2019[,1],
                          market_practice_mtly_packs_2018H2_2019[,1],
                          practice_mtly_calls_overall[,1],
                          practice_mtly_speaker_program_attendees[,1],
                          vae_sent[,1]) %>% distinct()

# 14833

# List of months

yrmo = unique(sym_practice_mtly_packs_2018H2_2019$yrmo)
yrmo = as.data.frame(yrmo)

# Cross join

cross_master = merge(practice_list,yrmo)

cross_master = merge(cross_master,sym_practice_mtly_packs_2018H2_2019,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_calls_overall,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_rep_led_meeting_attendees,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_gbrs_t_a_normal_call,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_gbred_normal_call,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_gbay_z_normal_call,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_gbcs_azex_normal_call,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,practice_mtly_speaker_program_attendees,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,vae_sent,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,vae_open,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)
cross_master = merge(cross_master,vae_clicks,by.x = c("practice_id","yrmo"),by.y = c("practice_id","yrmo"),all.x = TRUE)

cross_master[is.na(cross_master)] = 0

# Getting customer type

# Create a mapping of customer ID to data provider ID
F1 = affiliations %>% distinct(Amazon.MDM.ID,Data.Provider.ID)
F2 = calls_pharma %>% distinct(Account..Amazon.MDM.ID,Account..Data.Provider.ID)
colnames(F2) = colnames(F1)

F3 = bind_rows(F1,F2)
F3 = F3 %>% distinct()

cross_master2 = merge(cross_master,F3,by.x = "practice_id",by.y = "Data.Provider.ID",all.x = TRUE)

cross_master3 = merge(cross_master2,customer_detail[,c("Customer_GUID","Customer_Type")],by.x = "Amazon.MDM.ID",by.y = "Customer_GUID",all.x = TRUE)

# sum(cross_master3[which(cross_master3$Customer_Type=="Pharmacy"),"symbicort_packs"])

# e = cross_master3 %>% group_by(yrmo) %>% summarise(sum(symbicort_packs),
#                                              sum(calls),
#                                              sum(pde),
#                                              sum(rep_led_meeting_attendee_calls),
#                                              sum(rep_led_meeting_attendee_pde),
#                                              sum(gbrs_t_a_normal_calls),
#                                              sum(gbrs_t_a_normal_pde),
#                                              sum(gbred_normal_calls),
#                                              sum(gbred_normal_pde),
#                                              sum(gbay_z_normal_calls),
#                                              sum(gbay_z_normal_pde),
#                                              sum(gbcs_azex_normal_calls),
#                                              sum(gbcs_azex_normal_pde),
#                                              sum(attendees),
#                                              sum(vae_emails_sent),
#                                              sum(vae_emails_open),
#                                              sum(vae_emails_click))


# Network mapping ---------------------------------------------------------

# Saled practices

saled_practices = cross_master3 %>% filter(symbicort_packs > 0) %>% select(Amazon.MDM.ID,practice_id) %>% distinct()
nrow(saled_practices %>% filter(is.na(Amazon.MDM.ID)))
# 227

saled_practices2 = saled_practices %>% filter(!is.na(Amazon.MDM.ID))
# 8363

# Only promoted hospital and pharmacy practices
cross_master3$all = cross_master3$symbicort_packs+cross_master3$calls+cross_master3$attendees+cross_master3$vae_emails_sent
practices = cross_master3 %>% group_by(practice_id,Customer_Type) %>% summarise(all = sum(all),packs = sum(symbicort_packs))
only_promoted_practices = practices %>% filter(all>0 & packs == 0)
only_promoted_hosp_pharm_practices = only_promoted_practices %>% filter(Customer_Type == "Hospital" | Customer_Type == "Pharmacy")
# 4942

only_promoted_hosp_pharm_practices[,c(2,3,4)] = NULL
only_promoted_hosp_pharm_practices = merge(only_promoted_hosp_pharm_practices,F3,by.x = "practice_id",by.y = "Data.Provider.ID",all.x = TRUE)

only_promoted_hosp_pharm_practices2 = only_promoted_hosp_pharm_practices %>% filter(!is.na(Amazon.MDM.ID)) %>% select(Amazon.MDM.ID,practice_id)
# 4942

# Getting postal codes for lat & long

saled_practices2 = merge(saled_practices2,customer_detail[,c("Customer_GUID","Postal_Code_P")],
                         by.x = "Amazon.MDM.ID",by.y = "Customer_GUID",all.x = TRUE)

only_promoted_hosp_pharm_practices2 = merge(only_promoted_hosp_pharm_practices2,customer_detail[,c("Customer_GUID","Postal_Code_P")],
                         by.x = "Amazon.MDM.ID",by.y = "Customer_GUID",all.x = TRUE)

# Loading UK postal code, lat & long information
ukpostcodes = read.csv("ukpostcodes.csv")

# Getting lat and long

saled_practices2 = merge(saled_practices2,ukpostcodes[,c("postcode","latitude","longitude")],
                          by.x = "Postal_Code_P",by.y = "postcode",all.x = TRUE)

only_promoted_hosp_pharm_practices2 = merge(only_promoted_hosp_pharm_practices2,ukpostcodes[,c("postcode","latitude","longitude")],
                                  by.x = "Postal_Code_P",by.y = "postcode",all.x = TRUE)

saled_practices3 = saled_practices2 %>% filter(!is.na(latitude))
# 8349
only_promoted_hosp_pharm_practices3 = only_promoted_hosp_pharm_practices2 %>% filter(!is.na(latitude))
# 4922

# Cross join

master = merge(only_promoted_hosp_pharm_practices3$practice_id,saled_practices3$practice_id,all=TRUE)

colnames(master) = c("hosp_pharm_dp_id","practice_dp_id")

master2 = merge(master,only_promoted_hosp_pharm_practices3[,c("practice_id","latitude","longitude")],
                by.x = "hosp_pharm_dp_id",by.y = "practice_id",all.x = TRUE)

master3 = master2

colnames(master3)[3:4] = c("hosp_pharm_lat","hosp_pharm_lon")

master4 = merge(master3,saled_practices3[,c("practice_id","latitude","longitude")],
                by.x = "practice_dp_id",by.y = "practice_id",all.x = TRUE)

colnames(master4)[5:6] = c("practice_lat","practice_lon")

master5 = master4 %>% mutate(dist = distHaversine(cbind(hosp_pharm_lon, hosp_pharm_lat), cbind(practice_lon, practice_lat)))

master6 = master5 %>% filter(dist <= distance_cutoff)

master6 = master6 %>% arrange(hosp_pharm_dp_id,dist)

master7 = merge(master6,F3,by.x = "practice_dp_id",by.y = "Data.Provider.ID",all.x = TRUE)

master8 = merge(master7,customer_detail[,c("Customer_GUID","Postal_Code_P","Customer","Customer_Type")],by.x = "Amazon.MDM.ID",
                by.y = "Customer_GUID",all.x=TRUE)

colnames(master8)[1] = "practice_mdm_id"
colnames(master8)[9] = "practice_postal_code"
colnames(master8)[10] = "practice_customer"
colnames(master8)[11] = "practice_customer_type"

master9 = merge(master8,F3,by.x = "hosp_pharm_dp_id",by.y = "Data.Provider.ID",all.x = TRUE)

master10 = merge(master9,customer_detail[,c("Customer_GUID","Postal_Code_P","Customer","Customer_Type")],by.x = "Amazon.MDM.ID",
                 by.y = "Customer_GUID",all.x=TRUE)

colnames(master10)[1] = "hosp_pharm_mdm_id"
colnames(master10)[13] = "hosp_pharm_postal_code"
colnames(master10)[14] = "hosp_pharm_customer"
colnames(master10)[15] = "hosp_pharm_customer_type"

master10 = master10 %>% arrange(hosp_pharm_dp_id,dist)

master10 = master10 %>% select(hosp_pharm_dp_id,hosp_pharm_mdm_id,hosp_pharm_customer,hosp_pharm_customer_type,hosp_pharm_postal_code,hosp_pharm_lat,hosp_pharm_lon,
                               practice_dp_id,practice_mdm_id,practice_customer,practice_customer_type,practice_postal_code,practice_lat,practice_lon,dist)


practice_packs_2019 = cross_master3 %>% filter(yrmo >= 201901 & yrmo <= 201912) %>% group_by(practice_id) %>% summarise(practice_2019_packs = sum(symbicort_packs))

practice_packs_2018_h2 = cross_master3 %>% filter(yrmo >= 201807 & yrmo <= 201812) %>% group_by(practice_id) %>% summarise(practice_2018_h2_packs = sum(symbicort_packs))

master11 = merge(master10,practice_packs_2019,by.x = "practice_dp_id",by.y = "practice_id",all.x = TRUE)
master11 = merge(master11,practice_packs_2018_h2,by.x = "practice_dp_id",by.y = "practice_id",all.x = TRUE)

master11$potential = if_else(master11$practice_2019_packs==0,master11$practice_2018_h2_packs*2,master11$practice_2019_packs)

# f = master11 %>% group_by(practice_dp_id) %>% summarise(potential = sum(potential))

# Ranking the distance
master11 = master11 %>% group_by(hosp_pharm_dp_id) %>% 
  mutate(dist_rank = order(order(dist)))

master11 = read.csv("master11.csv",stringsAsFactors = FALSE)
# 123489

# Clustering for hospitals
master_hospitals = master11 %>% filter(hosp_pharm_customer_type == "Hospital")

# Loop

master_hospitals$hosp_pharm_dp_id = as.character(master_hospitals$hosp_pharm_dp_id)
master_hospitals$practice_dp_id = as.character(master_hospitals$practice_dp_id)

master_hospitals = master_hospitals %>% arrange(hosp_pharm_dp_id,dist)

distinct_hosp = master_hospitals %>% distinct(hosp_pharm_dp_id)
# 415

distinct_hosp = as.data.frame(distinct_hosp)

qc = master_hospitals %>% filter(hosp_pharm_dp_id==distinct_hosp[1,])

abc = qc[,c("practice_dp_id","practice_lat","practice_lon")]
colnames(abc) = c("id","lat","lon")

def = qc[,c("hosp_pharm_dp_id","hosp_pharm_lat","hosp_pharm_lon")]
colnames(def) = c("id","lat","lon")

ghi = bind_rows(abc,def) %>% distinct()
ghi$hosp_flg = if_else(ghi$id==distinct_hosp[1,],1,0)

x = ghi$lon
y = ghi$lat

xy <- SpatialPointsDataFrame(
  matrix(c(x,y), ncol=2), data.frame(ID=seq(1:length(x))),
  proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84"))

mdist <- distm(xy)

# cluster all points using a hierarchical clustering approach
hc <- hclust(as.dist(mdist), method="complete")

xy$clust <- cutree(hc, h=d)
ab = as.data.frame(xy)
ab$data_provider_id = ghi$id
ab$hosp_flg = ghi$hosp_flg

req_cluster = ab[which(ab$hosp_flg==1),"clust"]

ab_filtered = ab[which(ab$clust==req_cluster),]

ab_filtered$hosp_id = distinct_hosp[1,]

result = ab_filtered

for (i in 2:nrow(distinct_hosp)) {
  
  qc = master_hospitals %>% filter(hosp_pharm_dp_id==distinct_hosp[i,])
  
  abc = qc[,c("practice_dp_id","practice_lat","practice_lon")]
  colnames(abc) = c("id","lat","lon")
  
  def = qc[,c("hosp_pharm_dp_id","hosp_pharm_lat","hosp_pharm_lon")]
  colnames(def) = c("id","lat","lon")
  
  ghi = bind_rows(abc,def) %>% distinct()
  ghi$hosp_flg = if_else(ghi$id==distinct_hosp[i,],1,0)
  
  x = ghi$lon
  y = ghi$lat
  
  xy <- SpatialPointsDataFrame(
    matrix(c(x,y), ncol=2), data.frame(ID=seq(1:length(x))),
    proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84"))
  
  mdist <- distm(xy)
  
  # cluster all points using a hierarchical clustering approach
  hc <- hclust(as.dist(mdist), method="complete")
  
  xy$clust <- cutree(hc, h=d)
  ab = as.data.frame(xy)
  ab$data_provider_id = ghi$id
  ab$hosp_flg = ghi$hosp_flg
  
  req_cluster = ab[which(ab$hosp_flg==1),"clust"]
  
  ab_filtered = ab[which(ab$clust==req_cluster),]
  
  ab_filtered$hosp_id = distinct_hosp[i,]
  
  result = bind_rows(result,ab_filtered)
  
}

hosp_fin_mapping = result[which(result$hosp_flg==0),c("data_provider_id","hosp_id")]

practice_packs = unique(master11[,c("practice_dp_id","potential")])

hosp_fin_mapping_2 = merge(hosp_fin_mapping,practice_packs,by.x = "data_provider_id",by.y = "practice_dp_id",all.x=TRUE)

tot_potential = hosp_fin_mapping_2 %>% group_by(hosp_id) %>% summarise(tot_potential = sum(potential))

hosp_fin_mapping_2 = merge(hosp_fin_mapping_2, tot_potential,by="hosp_id",all.x=TRUE)

hosp_fin_mapping_2$promotion_distribution = hosp_fin_mapping_2$potential/hosp_fin_mapping_2$tot_potential

hosp_fin_mapping_3 = hosp_fin_mapping_2[,c("hosp_id","data_provider_id","promotion_distribution")]

colnames(hosp_fin_mapping_3) = c("hosp_pharm_id","practice_id_to_allocate","promotion_distribution")

# Pharmacies
master_pharmacies = master11 %>% filter(hosp_pharm_customer_type == "Pharmacy")

pharmacy_mapping_distance = master_pharmacies %>% filter(dist_rank == 1)

pharmacy_mapping_distance_2 = pharmacy_mapping_distance[,c("hosp_pharm_dp_id","practice_dp_id")]

pharmacy_mapping_distance_2$promotion_distribution = 1

pharmacy_mapping_distance_2$hosp_pharm_dp_id = as.character(pharmacy_mapping_distance_2$hosp_pharm_dp_id)
pharmacy_mapping_distance_2$practice_dp_id = as.character(pharmacy_mapping_distance_2$practice_dp_id)

colnames(pharmacy_mapping_distance_2) = c("hosp_pharm_id","practice_id_to_allocate","promotion_distribution")

reallocation_vf = bind_rows(hosp_fin_mapping_3,pharmacy_mapping_distance_2)

# Promotion reallocation --------------------------------------------------

cross_master4 = merge(cross_master3,reallocation_vf,by.x = "practice_id",by.y = "hosp_pharm_id",all.x = TRUE)

cross_master4$fin_practice_id = if_else(is.na(cross_master4$practice_id_to_allocate),cross_master4$practice_id,cross_master4$practice_id_to_allocate)

cross_master4$promotion_distribution = if_else(is.na(cross_master4$promotion_distribution),1,cross_master4$promotion_distribution)

cross_master5 = cross_master4

cross_master5$symbicort_packs_2 = cross_master5$symbicort_packs * cross_master5$promotion_distribution
cross_master5$calls_2 = cross_master5$calls * cross_master5$promotion_distribution
cross_master5$pde_2 = cross_master5$pde * cross_master5$promotion_distribution
cross_master5$rep_led_meeting_attendee_calls_2 = cross_master5$rep_led_meeting_attendee_calls * cross_master5$promotion_distribution
cross_master5$rep_led_meeting_attendee_pde_2 = cross_master5$rep_led_meeting_attendee_pde * cross_master5$promotion_distribution
cross_master5$gbrs_t_a_normal_calls_2 = cross_master5$gbrs_t_a_normal_calls * cross_master5$promotion_distribution
cross_master5$gbrs_t_a_normal_pde_2 = cross_master5$gbrs_t_a_normal_pde * cross_master5$promotion_distribution
cross_master5$gbred_normal_calls_2 = cross_master5$gbred_normal_calls * cross_master5$promotion_distribution
cross_master5$gbred_normal_pde_2 = cross_master5$gbred_normal_pde * cross_master5$promotion_distribution
cross_master5$gbay_z_normal_calls_2 = cross_master5$gbay_z_normal_calls * cross_master5$promotion_distribution
cross_master5$gbay_z_normal_pde_2 = cross_master5$gbay_z_normal_pde * cross_master5$promotion_distribution
cross_master5$gbcs_azex_normal_calls_2 = cross_master5$gbcs_azex_normal_calls * cross_master5$promotion_distribution
cross_master5$gbcs_azex_normal_pde_2 = cross_master5$gbcs_azex_normal_pde * cross_master5$promotion_distribution
cross_master5$attendees_2 = cross_master5$attendees * cross_master5$promotion_distribution
cross_master5$vae_emails_sent_2 = cross_master5$vae_emails_sent * cross_master5$promotion_distribution
cross_master5$vae_emails_open_2 = cross_master5$vae_emails_open * cross_master5$promotion_distribution
cross_master5$vae_emails_click_2 = cross_master5$vae_emails_click * cross_master5$promotion_distribution

# Filtering out hospitals, pharmacies

cross_master6 = cross_master5 %>% filter(!(Customer_Type == "Pharmacy" | Customer_Type == "Hospital"))

f = cross_master5 %>% group_by(yrmo) %>% summarise(sum(symbicort_packs),
                                             sum(calls),
                                             sum(pde),
                                             sum(rep_led_meeting_attendee_calls),
                                             sum(rep_led_meeting_attendee_pde),
                                             sum(gbrs_t_a_normal_calls),
                                             sum(gbrs_t_a_normal_pde),
                                             sum(gbred_normal_calls),
                                             sum(gbred_normal_pde),
                                             sum(gbay_z_normal_calls),
                                             sum(gbay_z_normal_pde),
                                             sum(gbcs_azex_normal_calls),
                                             sum(gbcs_azex_normal_pde),
                                             sum(attendees),
                                             sum(vae_emails_sent),
                                             sum(vae_emails_open),
                                             sum(vae_emails_click))

g = cross_master5 %>% group_by(yrmo) %>% summarise(sum(symbicort_packs_2),
                                                   sum(calls_2),
                                                   sum(pde_2),
                                                   sum(rep_led_meeting_attendee_calls_2),
                                                   sum(rep_led_meeting_attendee_pde_2),
                                                   sum(gbrs_t_a_normal_calls_2),
                                                   sum(gbrs_t_a_normal_pde_2),
                                                   sum(gbred_normal_calls_2),
                                                   sum(gbred_normal_pde_2),
                                                   sum(gbay_z_normal_calls_2),
                                                   sum(gbay_z_normal_pde_2),
                                                   sum(gbcs_azex_normal_calls_2),
                                                   sum(gbcs_azex_normal_pde_2),
                                                   sum(attendees_2),
                                                   sum(vae_emails_sent_2),
                                                   sum(vae_emails_open_2),
                                                   sum(vae_emails_click_2))

master_data_symbicort = cross_master5 %>% group_by(fin_practice_id,yrmo) %>%
  summarise(sum(symbicort_packs_2),
            sum(calls_2),
            sum(pde_2),
            sum(rep_led_meeting_attendee_calls_2),
            sum(rep_led_meeting_attendee_pde_2),
            sum(gbrs_t_a_normal_calls_2),
            sum(gbrs_t_a_normal_pde_2),
            sum(gbred_normal_calls_2),
            sum(gbred_normal_pde_2),
            sum(gbay_z_normal_calls_2),
            sum(gbay_z_normal_pde_2),
            sum(gbcs_azex_normal_calls_2),
            sum(gbcs_azex_normal_pde_2),
            sum(attendees_2),
            sum(vae_emails_sent_2),
            sum(vae_emails_open_2),
            sum(vae_emails_click_2))

colnames(master_data_symbicort)[3:19] = colnames(cross_master3)[4:20]

colnames(master_data_symbicort)[3:19] = colnames(cross_master3)[4:20]

h = master_data_symbicort %>% group_by(yrmo) %>% summarise(sum(symbicort_packs),
                                                   sum(calls),
                                                   sum(pde),
                                                   sum(rep_led_meeting_attendee_calls),
                                                   sum(rep_led_meeting_attendee_pde),
                                                   sum(gbrs_t_a_normal_calls),
                                                   sum(gbrs_t_a_normal_pde),
                                                   sum(gbred_normal_calls),
                                                   sum(gbred_normal_pde),
                                                   sum(gbay_z_normal_calls),
                                                   sum(gbay_z_normal_pde),
                                                   sum(gbcs_azex_normal_calls),
                                                   sum(gbcs_azex_normal_pde),
                                                   sum(attendees),
                                                   sum(vae_emails_sent),
                                                   sum(vae_emails_open),
                                                   sum(vae_emails_click))

master_data_symbicort_2 = master_data_symbicort %>% filter(!(fin_practice_id=="(Empty)" | fin_practice_id=="0"))

master_data_symbicort_3 = master_data_symbicort_2%>% filter(yrmo >= 201901)

master_data_symbicort_4 = master_data_symbicort%>% filter(yrmo >= 201901)

master_data_symbicort_5 = master_data_symbicort %>% filter(fin_practice_id=="(Empty)" | fin_practice_id=="0")

sum(master_data_symbicort_4$calls)
  
master_data_symbicort_2019_practice = master_data_symbicort_2 %>% filter(!(fin_practice_id=="(Empty)" | fin_practice_id=="0")) %>%
  filter(yrmo>=201901) %>% group_by(fin_practice_id) %>% summarise(sum(symbicort_packs),
                                                                   sum(calls),
                                                                   sum(pde),
                                                                   sum(rep_led_meeting_attendee_calls),
                                                                   sum(rep_led_meeting_attendee_pde),
                                                                   sum(gbrs_t_a_normal_calls),
                                                                   sum(gbrs_t_a_normal_pde),
                                                                   sum(gbred_normal_calls),
                                                                   sum(gbred_normal_pde),
                                                                   sum(gbay_z_normal_calls),
                                                                   sum(gbay_z_normal_pde),
                                                                   sum(gbcs_azex_normal_calls),
                                                                   sum(gbcs_azex_normal_pde),
                                                                   sum(attendees),
                                                                   sum(vae_emails_sent),
                                                                   sum(vae_emails_open),
                                                                   sum(vae_emails_click))




# Adjusting for CCG calls -------------------------------------------------


