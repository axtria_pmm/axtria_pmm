
# Loading raw data --------------------------------------------------------

setwd('C:/Users/kqll413/Box Sync/Global/08. Market mix modeling/UK/post gbred inclusion/01. Data')

# Master data
master_raw_act = read.xlsx("master_data_symbicort_6.xlsx",sheet = "master_data")
# 178002*23

master_raw_act = master_raw_act %>% filter(cluster>0)
# 151092*21

master_raw = master_raw_act[,c(2:ncol(master_raw_act))]

master_raw$Customer_Type = NULL

# Create any necessary normalized variables before proceeding

dependent_variable = "symbicort_packs_wd"
original_variable = "symbicort_packs"
normalization_variable = "norm"
monthly_normalization = "working_days"

mth_norm = master_raw[,c("yrmo",monthly_normalization)]

mth_norm = mth_norm %>% distinct()

# Filtering for segment

master = master_raw[which(master_raw$cluster>0),]

# Outlier removal - optional

practice = master %>% filter(yrmo>=201901) %>% group_by(fin_practice_id) %>%
  summarise(n=sum(symbicort_packs))

practice$out_flag = if_else(practice$n<=-1,1,0)

master = merge(master,practice,by="fin_practice_id",all.x=TRUE)

master = master %>% filter(out_flag==0)

colnames(master)[1]='dma'
colnames(master_raw)[1]='dma'

# Sales lag - adstocks will not matter
n_lags = 1
master_2 = lag_calc(master,dependent_variable,n_lags,c(0.5,0.5,0.5),0)

alpha_attendees_wd = c(0.5,0.5^2,0.5^3)
alpha_pde_wd = c(0.5,0.5^2,0.5^3)
alpha_gbred_pde_wd = c(0.5,0.5^2,0.5^3)

# Promotional adstocks
master_2 = lag_calc(master_2,'attendees_wd',3,alpha_attendees_wd,1)
master_2 = lag_calc(master_2,'pde_3_wd',3,alpha_pde_wd,1)
master_2 = lag_calc(master_2,'gbred_normal_pde_wd',3,alpha_pde_wd,1)

# Variable transformations
master_2 %>% summarise(var = mean(attendees_wd))
master_2 %>% filter(attendees_wd>0) %>% summarise(var = mean(attendees_wd))

master_2 %>% summarise(var = mean(pde_3_wd))
master_2 %>% filter(pde_3_wd>0) %>% summarise(var = mean(pde_3_wd))

master_2 %>% summarise(var = mean(gbred_normal_pde_wd))
master_2 %>% filter(gbred_normal_pde_wd>0) %>% summarise(var = mean(gbred_normal_pde_wd))

c_attendees_wd = 18
c_pde_wd = 7
c_gbred_wd = 8

master_2$attendees_exp = 1-(exp(-c_attendees_wd*master_2$attendees_wd_e))
master_2$pde_exp = 1-(exp(-c_pde_wd*master_2$pde_3_wd_e))
master_2$gbred_pde_exp = 1-(exp(-c_pde_wd*master_2$gbred_normal_pde_wd_e))

promotions = c("attendees_exp","pde_exp","gbred_pde_exp")

############ Model parameters
start=201901
end=201912
model_equation = paste(dependent_variable,"~",
                       dependent_variable,"_1+",
                       paste(promotions, collapse = '+')
                       ,sep = ""
)

############ Regression
output = PRM(master_2,model_equation,start,end)

output

# NLS

lag_sum_criteria = 0.9
Sales_lower = 0
sales_lag_condition = 1
single_lag = 0.95*output$Estimate[2]

cf = PRM1(master_2,model_equation,start,end)

cf$rn = output$rn
cf$variable = NULL
rownames(cf) = rownames(output)

output = cf

output

############ Impact calculations
mth_start = 7
mth_end = 18

fin_out = impact_calc(mth_start,mth_end,promotions)

a  = cbind(fin_out$impact_percent,percent(fin_out$impact_percent$percent))
colnames(a) = c(colnames(fin_out$impact_percent),"%")
a
percent(fin_out$non_normalized_error)

s1 = fin_out$output_monthly

trend = s1[,c("yrmo","total_predicted","total_actual")]

trend$total_predicted = unlist(trend$total_predicted)

ggplot(trend, aes(yrmo)) + 
  geom_line(aes(y = total_predicted, colour = "Predicted")) + 
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

output[,c("rn","Estimate")]
a

data_2 = fin_out$dma_data

data_3 = data_2 %>% select(dma,contains("attendees_exp_impact"))
colnames(data_3) = c("dma",201901:201912) 

data_4 =  reshape2::melt(data_3, id = c("dma"),variable.name = "yrmo",value.name = "attendee_impact_norm")

data_4 = merge(data_4,mth_norm,by = "yrmo",all.x = TRUE)
data_4$attendee_impact = data_4$attendee_impact_norm * data_4$working_days

data_3_red = data_2 %>% select(dma,contains("gbred_pde_exp_impact"))
colnames(data_3_red) = c("dma",201901:201912) 

data_4_red = reshape2::melt(data_3_red, id = c("dma"),variable.name = "yrmo",value.name = "red_impact_norm")

data_4_red = merge(data_4_red,mth_norm,by = "yrmo",all.x = TRUE)
data_4_red$red_impact = data_4_red$red_impact_norm * data_4_red$working_days

data_5 = data_2 %>% select(dma,contains("symbicort_packs_wd"))
data_5$symbicort_packs_wd_1_coef = NULL
data_5[,c(2:7)] = NULL
colnames(data_5) = c("dma",201901:201912) 

data_6 =  reshape2::melt(data_5, id = c("dma"),variable.name = "yrmo",value.name = "sym_packs_norm")

data_6 = merge(data_6,mth_norm,by = "yrmo",all.x = TRUE)
data_6$sympicort_packs = data_6$sym_packs_norm * data_6$working_days

data_7 = merge(data_4,data_6[,c("dma","yrmo","sympicort_packs")],by=c("dma","yrmo"),all.x = TRUE)

data_7 = merge(data_7,data_4_red[,c(1,2,3,5)],by=c("dma","yrmo"),all.x = TRUE)

data_7$impact_to_remove = data_7$attendee_impact + data_7$red_impact

data_7$symbicort_packs_2 = if_else(data_7$sympicort_packs-data_7$impact_to_remove < 0 , 0, data_7$sympicort_packs-data_7$impact_to_remove)

data_8 = data_7 %>% group_by(dma) %>% summarise(impact_to_remove = sum(impact_to_remove), sympicort_packs = sum(sympicort_packs))

data_8$impact_to_remove_percent = if_else(data_8$sympicort_packs==0,0,data_8$impact_to_remove/data_8$sympicort_packs)

master_residual = master

master_residual = merge(master_residual,data_8[,c("dma","impact_to_remove_percent")],by="dma",all.x = TRUE)

master_residual = merge(master_residual,data_7[,c("dma","yrmo","symbicort_packs_2")],by=c("dma","yrmo"),all.x = TRUE)

master_residual$symbicort_packs_2 = if_else(master_residual$yrmo < 201901, 
                                            master_residual$symbicort_packs*(1-master_residual$impact_to_remove_percent),
                                            master_residual$symbicort_packs_2)

master_residual$symbicort_packs_2 = if_else(master_residual$symbicort_packs_2 < 0, 0, master_residual$symbicort_packs_2)

master_res = master_residual

master_res$symbicort_packs = master_res$symbicort_packs_2

master_res$symbicort_packs_wd = master_res$symbicort_packs/master_res$working_days

(sum(master[master$yrmo>=201901,"symbicort_packs"])-sum(master_res[master_res$yrmo>=201901,"symbicort_packs"]))/
  sum(master[master$yrmo>=201901,"symbicort_packs"]) * 100

sum(master[master$yrmo>=201901,"gbred_normal_pde"])

qc = master_raw %>% filter(yrmo>=201901 & new_cluster > 0) %>% group_by(dma,new_cluster) %>% 
  summarise(ics_laba_packs = sum(ics_laba_packs),symbicort_packs = sum(symbicort_packs),
            rep_led_meeting_attendee_pde = sum(rep_led_meeting_attendee_pde),
            gbrs_t_a_normal_pde = sum(gbrs_t_a_normal_pde),
            gbred_normal_pde = sum(gbred_normal_pde),
            attendees = sum(attendees))

View(fin_out$output_monthly)