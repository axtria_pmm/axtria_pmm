# Setting directory
setwd('C:/Users/kqll413/Box Sync/Global/03. Data collection/UK/Network mapping')

# Loading libraries
library(geosphere)

library(stringdist)
library(stringr)
library(tm)

# Loading UK postal code, lat & long information
ukpostcodes = read.csv("ukpostcodes.csv")

# Loading the customer details, sales data, calls data

customer_detail = read.xlsx('Customer Detail.xlsx',startRow = 3,sheet = 1)
affiliations = read.xlsx('Customers to Organisation 2020 07 15.xlsx')
ics_laba_packs_2019 = read.xlsx('ICS LABA 2019.xlsx',sheet = 'Packs')
calls_non_pharma_2019 = read.csv('Calls Jan_Dec 2019.csv',stringsAsFactors = FALSE)
calls_pharma = read.csv('Calls pharma Jul-18 Jul-20.csv',stringsAsFactors = FALSE)

# Create a mapping of customer ID to data provider ID

F1 = affiliations %>% distinct(Amazon.MDM.ID,Data.Provider.ID)
F2 = calls_pharma %>% distinct(Account..Amazon.MDM.ID,Account..Data.Provider.ID)
colnames(F2) = colnames(F1)

F3 = bind_rows(F1,F2)

saled_practices_2 = merge(saled_practices,F3,by.x = "practice_id",by.y = "Data.Provider.ID",all.x = TRUE)
nrow(saled_practices_2 %>% filter(is.na(Amazon.MDM.ID)))
# 206 have no Customer ID

only_promoted_practices_2 = merge(only_promoted_practices, F3, by.x = "practice_id",by.y = "Data.Provider.ID",all.x = TRUE)
nrow(only_promoted_practices_2 %>% filter(is.na(Amazon.MDM.ID)))
# 0

only_promoted_practices_2 = only_promoted_practices_2 %>% distinct()


# Getting zip codes

saled_practices_3 = merge(saled_practices_2,customer_detail[,c("Customer_GUID","Postal_Code_P")],
                          by.x = "Amazon.MDM.ID",by.y = "Customer_GUID",all.x = TRUE)

only_promoted_practices_3 = merge(only_promoted_practices_2,customer_detail[,c("Customer_GUID","Postal_Code_P")],
                          by.x = "Amazon.MDM.ID",by.y = "Customer_GUID",all.x = TRUE)


# Getting lat and long

saled_practices_4 = merge(saled_practices_3,ukpostcodes[,c("postcode","latitude","longitude")],
                          by.x = "Postal_Code_P",by.y = "postcode",all.x = TRUE)

only_promoted_practices_4 = merge(only_promoted_practices_3,ukpostcodes[,c("postcode","latitude","longitude")],
                          by.x = "Postal_Code_P",by.y = "postcode",all.x = TRUE)


nrow(saled_practices_4 %>% filter(is.na(latitude)))
# 220

nrow(only_promoted_practices_4 %>% filter(is.na(latitude)))
# 89

saled_practices_5 = saled_practices_4 %>% filter(!is.na(latitude))
only_promoted_practices_5 = only_promoted_practices_4 %>% filter(!is.na(latitude))

# Cross join

master = merge(only_promoted_practices_5$practice_id,saled_practices_5$practice_id,all=TRUE)

master2 = merge(master,only_promoted_practices_5[,c("practice_id","latitude","longitude")],
                by.x = "x",by.y = "practice_id",all.x = TRUE)

master3 = head(master2,100000)

master3 = master2

colnames(master3) = c("x","y","x_lat","x_lon")

master4 = merge(master3,saled_practices_5[,c("practice_id","latitude","longitude")],
                by.x = "y",by.y = "practice_id",all.x = TRUE)

colnames(master4) = c("x","y","y_lat","y_lon","x_lat","x_lon")
# distm(c(lon1, lat1), c(lon2, lat2), fun = distHaversine)

master5 = master4

colnames(master5) = c("x","y","y_lat","y_lon","x_lat","x_lon")

master6 = master5 %>% mutate(dist = distHaversine(cbind(x_lon, x_lat), cbind(y_lon, y_lat)))


cutoff = 5000

master7 = master6 %>% filter(dist <= cutoff)

master8 = master7 %>% arrange(y,dist)


master9 = merge(master8,F3,by.x = "x",by.y = "Data.Provider.ID",all.x = TRUE)

master9 = merge(master9,customer_detail[,c("Customer_GUID","Postal_Code_P","Customer")],by.x = "Amazon.MDM.ID",
                by.y = "Customer_GUID",all.x=TRUE)

colnames(master9)[1] = "x_mdm_id"
colnames(master9)[9] = "x_postal_code"
colnames(master9)[10] = "x_customer"


master10 = merge(master9,F3,by.x = "y",by.y = "Data.Provider.ID",all.x = TRUE)

master10 = master10 %>% distinct()

master10 = merge(master10,customer_detail[,c("Customer_GUID","Postal_Code_P","Customer")],by.x = "Amazon.MDM.ID",
                by.y = "Customer_GUID",all.x=TRUE)

colnames(master10)[1] = "y_mdm_id"
colnames(master10)[12] = "y_postal_code"
colnames(master10)[13] = "y_customer"

master10 = master10 %>% arrange(y,dist)

master10 = master10 %>% select(x_mdm_id,x,x_postal_code,x_lat,x_lon,x_customer,dist,y_mdm_id,y,y_postal_code,y_lat,y_lon,y_customer)

master11 = merge(master10,saled_practices,by.x = "x",by.y = "practice_id",all.x = TRUE)

colnames(master11)[14] = "x_packs"

master11 = master11 %>% arrange(y,dist)

master11 = master11 %>% select(x_mdm_id,x,x_postal_code,x_lat,x_lon,x_customer,x_packs,dist,y_mdm_id,y,y_postal_code,y_lat,y_lon,y_customer)

write.csv(master11,"master11_v2.csv")

master_12 = read.csv("master11_v2.csv",stringsAsFactors = FALSE)


# master_12 = master_12 %>% filter(dist<=2500)

master_12$Sim1<-stringdist(master_12$x_customer,master_12$y_customer,method = "jw")


write.csv(master_12,"master_12.csv",row.names = FALSE)






write.csv(F3,"F3.csv")








qc = master8%>% group_by(y) %>% summarise(n=n())

qc2 = master8 %>% filter(y=="L34654")


distm(c(-0.025909291,52.97350738), c(-3.786629, 56.0032), fun = distHaversine)
distm(c(-0.025909291,52.97350738), c(-3.786629, 56.0032), fun = distGeo)
distm(c(-0.025909291,52.97350738), c(-3.786629, 56.0032), fun = distCosine)
distm(c(-0.025909291,52.97350738), c(-3.786629, 56.0032), fun = distVincentySphere)
distm(c(-0.025909291,52.97350738), c(-3.786629, 56.0032), fun = distVincentyEllipsoid)
