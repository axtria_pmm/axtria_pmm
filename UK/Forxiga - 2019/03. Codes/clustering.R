
# Setting up working directory --------------------------------------------
setwd("C:/Users/kqll413/Box Sync/Global/07. Micro clustering/UK/Clustering")

# Loding libraries --------------------------------------------------------
library(geosphere)
library(stringdist)
library(stringr)
library(tm)
library(sp)
library(rgdal)
library(geosphere)
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(stringr)
library(splitstackshape)
library(rstatix)


# Clustering data prep ----------------------------------------------------

practice_level_data = read.xlsx('Symbicort_practice_summaries_2019.xlsx',sheet = 'data')

mb_db = practice_level_data

# Filtering out 1% of bottow practices
mb_db = mb_db %>% filter(flag == 0)

# Clubing none and NA segments
mb_db$`2020.Segment` = if_else(is.na(mb_db$`2020.Segment`),"None",mb_db$`2020.Segment`)

# Required variables

mb_db$share = mb_db$`%.Brand.share`
mb_db$segment = mb_db$`2020.Segment`

mb_db$Physician_engagement = as.factor(mb_db$Physician_engagement)

mb_db$share_flag = if_else(mb_db$share>= median(mb_db$share),1,0)

mb_db$share_flag = as.factor(mb_db$share_flag)

mb_db2 = mb_db[,c("fin_practice_id","market_packs","symbicort_packs","share","calls","attendees","vae_emails_sent","segment","growth.flag","Kmediods_2","Physician_engagement","share_flag")]


mb_db2$sales_calls = if_else(mb_db2$calls == 0, 0, mb_db2$symbicort_packs/mb_db2$calls)

res.aov2 <- aov(sales_calls ~ segment+share_flag+Physician_engagement+
                  segment*share_flag+segment*Physician_engagement+share_flag*Physician_engagement+
                  segment*share_flag*Physician_engagement, data = mb_db2)

res.aov2 <- aov(sales_calls ~ Kmediods_2, data = mb_db2)

# res.aov2 <- aov(symbicort_packs ~ segment+Kmediods_2+Physician_engagement+
#                   segment*Kmediods_2 + segment*Physician_engagement + Kmediods_2*Physician_engagement+
#                   segment*Kmediods_2*Physician_engagement, data = mb_db2)

# res.aov2 <- aov(symbicort_packs ~ segment+growth.flag+segment*growth.flag, data = mb_db2)

# res.aov2 <- aov(symbicort_packs ~ segment*growth.flag, data = mb_db2)

summary(res.aov2)

tk = TukeyHSD(res.aov2)
View(tk$Kmediods_2)

plot(tk , las=1 , col="brown")

# Define variables to use
colnames(mb_db)
vars = c("market_packs","%.Brand.share")

# Selecting variables
dataset = mb_db[,vars]

# Scaling data
df = as.data.frame(scale(dataset))

# Silhouette method
# fviz_nbclust(df, kmeans, method = "silhouette")

# Fitting K-Means
kmeans_model = kmeans(x = df, centers = 3,nstart = 10,iter.max = 300)
kmeans_centers = as.data.frame(kmeans_model$centers) 
kmeans_y = factor(kmeans_model$cluster)

ggplot() +
  geom_point(data = df, 
             mapping = aes(x = df[,vars[1]], 
                           y = df[,vars[2]], 
                           colour = kmeans_y)) +
  geom_point(mapping = aes_string(x = kmeans_centers[, vars[1]], 
                                  y = kmeans_centers[, vars[2]]),
             color = "red", size = 4)

mb_db$fin_clusters_kmeans = kmeans_y

# Summary

summary = mb_db %>% group_by(fin_clusters_kmeans) %>% summarise(count = n(),
                                                                sym_sales = sum(symbicort_packs),
                                                                sym_sales_per_practice = sum(symbicort_packs)/n(),
                                                                market_sales = sum(market_packs),
                                                                market_sales_per_practice = sum(market_packs)/n(),
                                                                sym_share = sum(symbicort_packs)/sum(market_packs),
                                                                sym_calls = sum(calls),
                                                                sym_calls_per_practice = sum(calls)/n(),
                                                                sym_attendees = sum(attendees),
                                                                sym_attendees_per_practice = sum(attendees)/n(),
                                                                sym_vae_sent = sum(vae_emails_sent),
                                                                sym_vae_sent_per_practice = sum(vae_emails_sent)/n())

summary = mb_db %>% group_by(Region.from.postal.code) %>% summarise(count = n(),
                                                                sym_sales = sum(symbicort_packs),
                                                                sym_sales_per_practice = sum(symbicort_packs)/n(),
                                                                market_sales = sum(market_packs),
                                                                market_sales_per_practice = sum(market_packs)/n(),
                                                                sym_share = sum(symbicort_packs)/sum(market_packs),
                                                                sym_calls = sum(calls),
                                                                sym_calls_per_practice = sum(calls)/n(),
                                                                sym_attendees = sum(attendees),
                                                                sym_attendees_per_practice = sum(attendees)/n(),
                                                                sym_vae_sent = sum(vae_emails_sent),
                                                                sym_vae_sent_per_practice = sum(vae_emails_sent)/n())



# Scatter plot

mb_db$c_tier = paste(mb_db$`2020.Segment`, mb_db$growth.flag, sep = "-")
  
ggplot() +
  geom_point(data = mb_db, 
             mapping = aes(x = mb_db$market_packs, 
                           y = mb_db$symbicort_packs, 
                           colour = mb_db$growth.flag)) +
  labs(colour='Combined tier') 

# Market Packs, Symbicort Packs
# Market Packs, Symbicort share
# Symbicort Packs, Symbicort share
# Symbicort Packs, Symbicort calls

