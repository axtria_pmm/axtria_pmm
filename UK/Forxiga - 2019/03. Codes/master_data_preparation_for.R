# Importing libraries
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(stringr)
library(splitstackshape)

# Define time period for data preparation 
start = 201901
end = 201912

# Sales data

# Setting up directory
setwd('C:/Users/kqll413/Box Sync/Global/03. Data collection/UK/vf')

# Summarizing Packs

ioad_packs_2019 = read.xlsx('IOAD 2019.xlsx',sheet = 'Packs')

ioad_packs_2019_2 =  melt(ioad_packs_2019, id = c("Practice.ID","LHE.Name","Practice.NHS.ID","Brand","Pack"),
                              variable.name = "yrmo",value.name = "packs")

ioad_packs_2019_2 = ioad_packs_2019_2 %>% filter(packs > 0)

ioad_packs_2019_2$abc = as.character(ioad_packs_2019_2$yrmo)
ioad_packs_2019_2$def = as.integer(ioad_packs_2019_2 $abc)
ioad_packs_2019_2$date = as.Date(ioad_packs_2019_2$def, origin = "1899-12-30")

ioad_packs_2019_2$yrmo = as.numeric(format(ioad_packs_2019_2$date, "%Y"))*100+as.numeric(format(ioad_packs_2019_2$date, "%m"))

ioad_packs_2019_2$abc=NULL
ioad_packs_2019_2$def=NULL
ioad_packs_2019_2$date=NULL

# Summarizing Cash

ioad_cash_2019 = read.xlsx('IOAD 2019.xlsx',sheet = 'Cash')

ioad_cash_2019_2 =  melt(ioad_cash_2019, id = c("Practice.ID","LHE.Name","Practice.NHS.ID","Brand","Pack"),
                             variable.name = "yrmo",value.name = "cash")

ioad_cash_2019_2 = ioad_cash_2019_2 %>% filter(cash > 0)

ioad_cash_2019_2$abc = as.character(ioad_cash_2019_2$yrmo)
ioad_cash_2019_2$def = as.integer(ioad_cash_2019_2 $abc)
ioad_cash_2019_2$date = as.Date(ioad_cash_2019_2$def, origin = "1899-12-30")

ioad_cash_2019_2$yrmo = as.numeric(format(ioad_cash_2019_2$date, "%Y"))*100+as.numeric(format(ioad_cash_2019_2$date, "%m"))

ioad_cash_2019_2$abc=NULL
ioad_cash_2019_2$def=NULL
ioad_cash_2019_2$date=NULL


# Summary 1

# Monthly sales trend

F_summary_1 = ioad_packs_2019_2 %>% group_by(Brand,yrmo) %>% summarise(overall_2019 = sum(packs))
F_summary_2 = ioad_cash_2019_2 %>% group_by(Brand,yrmo) %>% summarise(overall_2019 = sum(cash))

# Calls data

product = 'Forxiga_GB(Diabetes_GB)'
product2 = 'Forxiga_GB'

calls_non_pharma_2019 = read.csv('Calls Jan_Dec 2019.csv',stringsAsFactors = FALSE)
calls_pharma = read.csv('Calls pharma Jul-18 Jul-20.csv',stringsAsFactors = FALSE)

calls_overall = rbind(calls_non_pharma_2019,calls_pharma)

calls_overall$dp = str_remove_all(calls_overall$Detailed.Products,"No Product_GB\\(RIA_GB\\)")
calls_overall$dp = str_remove_all(calls_overall$dp,"No Product_GB\\(Onc_GB\\)")
calls_overall$dp = str_remove_all(calls_overall$dp,"No Product_GB\\(Diabetes_GB\\)")
calls_overall$dp = str_remove_all(calls_overall$dp,"No Product_GB\\(CV_GB\\)")

calls_overall_2 = calls_overall %>% filter(str_detect(dp,"Forxiga"))
calls_overall_2 = cSplit(calls_overall_2,"dp", " ")

calls_overall_2$pde = if_else((calls_overall_2$dp_1 == product) | (calls_overall_2$dp_1 == product2),1,
                              if_else((calls_overall_2$dp_2 == product) | (calls_overall_2$dp_2 == product2),0.5,
                                      if_else((calls_overall_2$dp_3 == product) | (calls_overall_2$dp_3 == product2),0.1,0)))

calls_overall_2$newdate = as.Date(calls_overall_2$Datetime, "%d/%m/%Y %H:%M")

calls_overall_2$yrmo = as.numeric(format(calls_overall_2$newdate, "%Y"))*100+as.numeric(format(calls_overall_2$newdate, "%m"))

calls_overall_2 = calls_overall_2 %>% filter(yrmo >= 201901 & yrmo <= 201912)

sales_team_mapping = read.csv('salesteam_forxiga.csv',stringsAsFactors = FALSE)

calls_overall_3 = merge(calls_overall_2,sales_team_mapping,by = "Territory",all.x = TRUE)

# Filtering of data

calls_overall_3$filter = if_else(calls_overall_3$sales.team == 'GBAZEX' & calls_overall_3$X1.1.Call == 0,1,0)

calls_overall_3$filter = if_else(calls_overall_3$pde >0 ,calls_overall_3$filter,1)

calls_overall_4 = calls_overall_3 %>% filter(filter==0)

calls_overall_4$cost = if_else(is.na(calls_overall_4$Actual.Costs),0,calls_overall_4$Actual.Costs)

calls_overall_4$rep_led_meeting = if_else(calls_overall_4$cost > 0 , 1, 0)

# Monthly calls, pde

F_summary_3 = calls_overall_4 %>% group_by(yrmo,sales.team,rep_led_meeting) %>% summarise(calls=n_distinct(Call..Call.Name),pde = sum(pde),cost = sum(Actual.Costs))

# Position wise PDE

calls_overall_4$is_pharmacy = if_else(calls_overall_4$Call..Record.Type == "Pharmacy Call",1,0)

calls_overall_4$service_type = if_else(calls_overall_4$X1.1.Call==1,"F2F","Drop of material")

a_lkp_call_channels = read.xlsx('a_lkp_call_channels.xlsx')

calls_overall_4$Key = paste(calls_overall_4$Call..Record.Type,calls_overall_4$Delivery.Channel,sep = "")

calls_overall_4 = merge(calls_overall_4,a_lkp_call_channels,by = "Key",all.x = TRUE)

summary_all = calls_overall_4 %>% 
  group_by(yrmo,dp_1, dp_2, dp_3,sales.team, rep_led_meeting,is_pharmacy,service_type,Channel) %>% 
  summarise(calls=n_distinct(Call..Call.Name),pde = sum(pde))

# Affiliations data

affiliations = read.xlsx('Customers to Organisation 2020 07 15.xlsx')

calls_overall_5 = merge(calls_overall_4, affiliations[,c("Child.Account:.Amazon.MDM.ID","Data.Provider.ID")],by.x = "Account..Amazon.MDM.ID",
                        by.y = "Child.Account:.Amazon.MDM.ID",all.x = TRUE)

colnames(calls_overall_5)[ncol(calls_overall_5)] = "Data_provider_id_from_hcp"

affiliations_2 = affiliations %>% distinct(Amazon.MDM.ID,Data.Provider.ID)

calls_overall_6 = merge(calls_overall_5, affiliations_2,by.x = "Account..Amazon.MDM.ID",by.y = "Amazon.MDM.ID",all.x = TRUE)

calls_overall_6$practice_id = if_else(is.na(calls_overall_6$Data_provider_id_from_hcp),calls_overall_6$Data.Provider.ID,calls_overall_6$Data_provider_id_from_hcp)

calls_overall_6$practice_id = if_else(calls_overall_6$Call..Record.Type == "Pharmacy Call",calls_overall_6$Account..Data.Provider.ID,calls_overall_6$practice_id)

p_calls_2019 = calls_overall_6 %>% filter(is_pharmacy == 1) %>% summarise(calls = n_distinct(Call..Call.Name), pde = sum(pde))

a_lkp_call_channels = read.xlsx('a_lkp_call_channels.xlsx')

calls_overall_6$Key = paste(calls_overall_6$Call..Record.Type,calls_overall_6$Delivery.Channel,sep = "")

calls_overall_7 = merge(calls_overall_6,a_lkp_call_channels,by = "Key",all.x = TRUE)

# Merging calls and sales

sales_2019 = ioad_packs_2019_2 %>% filter(Brand == "Forxiga Family") %>% group_by(Practice.ID) %>% summarise(packs = sum(packs))
calls_2019 = calls_overall_6 %>% group_by(practice_id) %>% summarise(calls = n_distinct(Call..Call.Name), pde = sum(pde))

F_sales_calls_2019 = merge(sales_2019, calls_2019, by.x = "Practice.ID",by.y = "practice_id", all = TRUE)
write.csv(F_sales_calls_2019,"F_sales_calls_2019.csv")

for_pharmacy = calls_overall_6 %>% filter(is_pharmacy == 1) %>% group_by(practice_id) %>%  summarise(calls = n_distinct(Call..Call.Name), pde = sum(pde))
write.csv(for_pharmacy,"for_pharmacy.csv")
