
dependent_variable = "symbicort_packs_wd"
original_variable = "symbicort_packs"
normalization_variable = "norm"
monthly_normalization = "working_days"

mth_norm = master_raw[,c("yrmo",monthly_normalization)]

mth_norm = mth_norm %>% distinct()

master = master_res

# Filtering for segment

master = master[which(master$cluster==1 | master$cluster==3),]

# Sales lag - adstocks will not matter
n_lags = 1
master_2 = lag_calc(master,dependent_variable,n_lags,c(0.5,0.5,0.5),0)

alpha_rep_led_meeting_attendee_wd = c(0.5,0.5^2,0.5^3)
alpha_gbrs_t_a_normal_pde_wd = c(0.5,0.5^2,0.5^3)

# Promotional adstocks
master_2 = lag_calc(master_2,'rep_led_meeting_attendee_pde_wd',3,alpha_rep_led_meeting_attendee_wd,1)
master_2 = lag_calc(master_2,'gbrs_t_a_normal_pde_wd',3,alpha_gbrs_t_a_normal_pde_wd,1)

# Variable transformations

master_2 %>% summarise(var = mean(rep_led_meeting_attendee_pde_wd))
master_2 %>% filter(rep_led_meeting_attendee_pde_wd>0) %>% summarise(var = mean(rep_led_meeting_attendee_pde_wd))

master_2 %>% summarise(var = mean(gbrs_t_a_normal_pde_wd))
master_2 %>% filter(gbrs_t_a_normal_pde_wd>0) %>% summarise(var = mean(gbrs_t_a_normal_pde_wd))

c_rep_led_meeting_attendee_wd = 7.9
c_gbrs_t_a_normal_pde_wd = 7.5

c_rep_led_meeting_attendee_wd = 25
c_gbrs_t_a_normal_pde_wd = 4

master_2$rep_led_meeting_attendee_pde_exp = 1-(exp(-c_rep_led_meeting_attendee_wd*master_2$rep_led_meeting_attendee_pde_wd_e))
master_2$gbrs_t_a_normal_pde_exp = 1-(exp(-c_gbrs_t_a_normal_pde_wd*master_2$gbrs_t_a_normal_pde_wd_e))

plot(master_2$rep_led_meeting_attendee_pde_wd_e,master_2$rep_led_meeting_attendee_pde_exp)
plot(master_2$gbrs_t_a_normal_pde_wd_e,master_2$gbrs_t_a_normal_pde_exp)

promotions = c("rep_led_meeting_attendee_pde_exp","gbrs_t_a_normal_pde_exp")

############ Model parameters
start=201901
end=201912
model_equation = paste(dependent_variable,"~",
                       dependent_variable,"_1+",
                       # dependent_variable,"_2+",
                       paste(promotions, collapse = '+')
                       ,sep = ""
)

############ Regression
output = PRM(master_2,model_equation,start,end)

output

lag_sum_criteria = 0.9
Sales_lower = 0
sales_lag_condition = 1
single_lag = 0.9*output$Estimate[2]

cf = PRM1(master_2,model_equation,start,end)

cf$rn = output$rn
cf$variable = NULL
rownames(cf) = rownames(output)

output = cf

output

############ Impact calculations

mth_start = 7
mth_end = 18

fin_out = impact_calc(mth_start,mth_end,promotions)

a  = cbind(fin_out$impact_percent,percent(fin_out$impact_percent$percent))
colnames(a) = c(colnames(fin_out$impact_percent),"%")
a
percent(fin_out$non_normalized_error)

s1 = fin_out$output_monthly

trend = s1[,c("yrmo","total_predicted","total_actual")]

trend$total_predicted = unlist(trend$total_predicted)

ggplot(trend, aes(yrmo)) + 
  geom_line(aes(y = total_predicted, colour = "Predicted")) + 
  geom_line(aes(y = total_actual, colour = "Actual")) +
  expand_limits(y = 0)

output[,c("rn","Estimate")]
a
a$sum[4]/a$sum[3]
(a$percent[4]+a$percent[3])*100

mean(master_2[master_2$yrmo>=201901,"symbicort_packs_wd_1"])

# write.csv(fin_out$dma_data,"dma_data_seg_1_3.csv",row.names = F)

View(fin_out$output_monthly)
