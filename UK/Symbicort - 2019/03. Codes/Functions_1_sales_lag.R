# Loading libraries -------------------------------------------------------

library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(furrr)
library(nls.multstart)
library(broom)
library(dplyr)
library(tidyr)
library(tidyverse)
library(lubridate)
library(sparklyr)
library(fastcluster)
library(cluster)
library(nls.multstart)
library(broom)
library(compiler)
library(formula.tools)
library(lme4)
library(optimx)
library(zoo)
library(RcppEigen)
# library(MASS)
library(corrplot)
library(pls)
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)
library(data.table)
library(splitstackshape)
library(glmmTMB)

# Functions
####Percent print
percent <- function(x, digits = 2, format = "f", ...) {
  paste0(formatC(100 * x, format = format, digits = digits, ...), "%")
}

####Correlation matrix
corr_mat = function(dataset){
  data = subset(dataset, select=-c(dma,yrmo,segment))
  correlation = cor(data)
  correlation = as.data.frame(correlation)
  return(correlation)
}

#### Lag function
lag_calc = function(dataset,var,no_of_lags,lags,single_col)
{
  data=dataset[order(dataset$dma,dataset$yrmo),]
  for (l in 1:length(var))
  {
    if(single_col==0)
    {
      print("Lags will not be removed")
      for (i in 1:no_of_lags)
      {
        data[(i+1):nrow(data),paste(var,i,sep="_")]=data[1:(nrow(data)-i),var]
        data[data['yrmo']<=201806+i,paste(var,i,sep="_")]=0
        print(sum(data[paste(var,i,sep="_")]))
        print(sum(data[data['yrmo']<=(max(data$yrmo)-i),var]))
      }
    }
    if(single_col==1)
    {
      print("Lags will be removed")
      data[,paste(var,'e',sep='_')]=data[,var]
      for (i in 1:no_of_lags)
      {
        data[(i+1):nrow(data),paste(var,i,sep="_")]=data[1:(nrow(data)-i),var]
        data[data['yrmo']<=201806+i,paste(var,i,sep="_")]=0
        print(sum(data[paste(var,i,sep="_")]))
        print(sum(data[data['yrmo']<=(max(data$yrmo)-i),var]))
        data[,paste(var,'e',sep='_')]=data[,paste(var,'e',sep='_')]+(data[,paste(var,i,sep='_')]*lags[i])
        data[,paste(var,i,sep='_')]<-NULL
      }
    }
  }
  return(data)
}

#### PRM
PRM = function(dataset,model_equation,start,end){
  
  data = master_2[which(master_2$yrmo>= start & master_2$yrmo <= end),]
  
  model = lm(formula = model_equation,data = data)
  
  model_output = as.data.frame(summary(model)$coefficients)
  model_output$r_squared = summary(model)$r.squared
  model_output$adj_r_squared = summary(model)$adj.r.squared
  model_output$AIC = AIC(model)
  model_output$BIC = BIC(model)
  
  setDT(model_output, keep.rownames = TRUE)[]
  model_output$rn[1] = 'Intercept'
  
  return(model_output)
  
}

#### Variable impact

impact_calc = function(mth_start,mth_end,promotions)
{
  
  out <- list()
  
  # data preparation
  yrmo  = as.data.frame(sort(unique(master_2$yrmo),decreasing = FALSE))
  colnames(yrmo) = 'yrmo'
  yrmo$mth = 1:nrow(yrmo)
  master_3 = merge(master_2, yrmo)
  
  out$yrmo = yrmo
  
  # Creating DMA level data
  dma_data_p = reshape2::dcast(master_3, dma ~ mth, value.var=dependent_variable)
  colnames(dma_data_p)[2:ncol(dma_data_p)] = paste(dependent_variable,"_",colnames(dma_data_p)[2:ncol(dma_data_p)],sep="")
  dma_data = dma_data_p
  
  for (i in 1:length(promotions)){
    dma_data_p = reshape2::dcast(master_3, dma ~ mth, value.var=promotions[i])
    colnames(dma_data_p)[2:ncol(dma_data_p)] = paste(promotions[i],"_",colnames(dma_data_p)[2:ncol(dma_data_p)],sep="")
    dma_data = merge(dma_data, dma_data_p)
  }
  
  dma_data$segment = 'abc'
  
  # Merging model coefficients
  
  model_coefs = dcast(output, rowid(rn) ~ rn, value.var="Estimate")
  model_coefs$rn = NULL
  colnames(model_coefs) = paste(colnames(model_coefs),"_coef",sep="")
  model_coefs$segment = 'abc'
  dma_data = merge(dma_data, model_coefs)
  dma_data$segment = NULL
  
  # Merging normalization variable
  norm  = as.data.frame(unique(master_2[,c("dma",normalization_variable)]))
  dma_data = merge(dma_data, norm)
  
  # Impact calculations
  
  # Baseline
  
  for (i in mth_start : mth_end)
  {
    if(i == mth_start)
    {
      dma_data[,paste("baseline",i,sep="_")] = dma_data$Intercept_coef  
    }
    if(i == mth_start+1)
    {
      dma_data[,paste("baseline",i,sep="_")] = dma_data$Intercept_coef + 
        dma_data[,paste("baseline",i-1,sep="_")]*dma_data[,paste(dependent_variable,"_1_coef",sep = "")]
    }
    if(i == mth_start+2)
    {
      dma_data[,paste("baseline",i,sep="_")] = dma_data$Intercept_coef + 
        dma_data[,paste("baseline",i-1,sep="_")]*dma_data[,paste(dependent_variable,"_1_coef",sep = "")] 
      # + 
        # dma_data[,paste("baseline",i-2,sep="_")]*dma_data[,paste(dependent_variable,"_2_coef",sep = "")]
    }
    if(i > mth_start+2)
    {
      dma_data[,paste("baseline",i,sep="_")] = dma_data$Intercept_coef + 
        dma_data[,paste("baseline",i-1,sep="_")]*dma_data[,paste(dependent_variable,"_1_coef",sep = "")] 
      # + 
        # dma_data[,paste("baseline",i-2,sep="_")]*dma_data[,paste(dependent_variable,"_2_coef",sep = "")] 
      # +
      # dma_data[,paste("baseline",i-3,sep="_")]*dma_data[,paste(dependent_variable,"_3_coef",sep = "")]
    }
  }
  
  # Carry over
  
  for (i in mth_start : mth_end)
  {
    if(i == mth_start)
    {
      dma_data[,paste("carryover",i,sep="_")] = 
        dma_data[,paste(dependent_variable,"_1_coef",sep = "")]*dma_data[,paste(dependent_variable,i-1,sep="_")] 
      # +
        # dma_data[,paste(dependent_variable,"_2_coef",sep = "")]*dma_data[,paste(dependent_variable,i-2,sep="_")] 
      # +
      # dma_data[,paste(dependent_variable,"_3_coef",sep = "")]*dma_data[,paste(dependent_variable,i-3,sep="_")]
      
    }
    if(i == mth_start+1)
    {
      dma_data[,paste("carryover",i,sep="_")] = 
        dma_data[,paste(dependent_variable,"_1_coef",sep = "")]*dma_data[,paste("carryover",i-1,sep="_")] 
      # +
        # dma_data[,paste(dependent_variable,"_2_coef",sep = "")]*dma_data[,paste(dependent_variable,i-2,sep="_")] 
      # +
      # dma_data[,paste(dependent_variable,"_3_coef",sep = "")]*dma_data[,paste(dependent_variable,i-3,sep="_")]
    }
    if(i == mth_start+2)
    {
      dma_data[,paste("carryover",i,sep="_")] = 
        dma_data[,paste(dependent_variable,"_1_coef",sep = "")]*dma_data[,paste("carryover",i-1,sep="_")] 
      # +
        # dma_data[,paste(dependent_variable,"_2_coef",sep = "")]*dma_data[,paste("carryover",i-2,sep="_")] 
      # +
      # dma_data[,paste(dependent_variable,"_3_coef",sep = "")]*dma_data[,paste(dependent_variable,i-3,sep="_")]
    }
    if(i > mth_start+2)
    {
      dma_data[,paste("carryover",i,sep="_")] = 
        dma_data[,paste(dependent_variable,"_1_coef",sep = "")]*dma_data[,paste("carryover",i-1,sep="_")] 
      # +
        # dma_data[,paste(dependent_variable,"_2_coef",sep = "")]*dma_data[,paste("carryover",i-2,sep="_")] 
      # +
      # dma_data[,paste(dependent_variable,"_3_coef",sep = "")]*dma_data[,paste("carryover",i-3,sep="_")]
    }
  }
  
  # Promotional channels
  
  for (i in 1:length(promotions))
  {
    var = promotions[i]
    for (i in mth_start : mth_end)
    {
      if(i == mth_start)
      {
        dma_data[,paste(var,"impact",i,sep="_")] = dma_data[,paste(var,"coef",sep="_")]*dma_data[,paste(var,i,sep="_")]
      }
      if(i == mth_start+1)
      {
        dma_data[,paste(var,"impact",i,sep="_")] = dma_data[,paste(var,"coef",sep="_")]*dma_data[,paste(var,i,sep="_")] + 
          dma_data[,paste(var,"impact",i-1,sep="_")]*dma_data[,paste(dependent_variable,"_1_coef",sep = "")]
      }
      if(i == mth_start+2)
      {
        dma_data[,paste(var,"impact",i,sep="_")] = dma_data[,paste(var,"coef",sep="_")]*dma_data[,paste(var,i,sep="_")] + 
          dma_data[,paste(var,"impact",i-1,sep="_")]*dma_data[,paste(dependent_variable,"_1_coef",sep = "")] 
        # +
          # dma_data[,paste(var,"impact",i-2,sep="_")]*dma_data[,paste(dependent_variable,"_2_coef",sep = "")]
      }
      if(i > mth_start+2)
      {
        dma_data[,paste(var,"impact",i,sep="_")] = dma_data[,paste(var,"coef",sep="_")]*dma_data[,paste(var,i,sep="_")] + 
          dma_data[,paste(var,"impact",i-1,sep="_")]*dma_data[,paste(dependent_variable,"_1_coef",sep = "")] 
        # +
          # dma_data[,paste(var,"impact",i-2,sep="_")]*dma_data[,paste(dependent_variable,"_2_coef",sep = "")] 
        # +
        # dma_data[,paste(var,"impact",i-3,sep="_")]*dma_data[,paste(dependent_variable,"_3_coef",sep = "")]
      }
    }
  }
  
  out$dma_data = dma_data
  
  # Final monthly calculations
  
  for (i in mth_start : mth_end){
    if(i == mth_start){
      
      
      baseline_norm = sum(dma_data[,paste("baseline",i,sep="_")])
      carryover_norm = sum(dma_data[,paste("carryover",i,sep="_")])
      
      output_monthly_norm = cbind(yrmo[i,],baseline_norm,carryover_norm)
      
      for (j in 1:length(promotions))
        
      {
        impact = as.data.frame(sum(dma_data[,paste(promotions[j],"impact",i,sep="_")]))
        colnames(impact) = paste(promotions[j],"norm_impact",sep="_")
        output_monthly_norm = cbind(output_monthly_norm,impact)
      }
      
      baseline = crossprod(dma_data[,paste("baseline",i,sep="_")],dma_data[,normalization_variable])
      carryover = crossprod(dma_data[,paste("carryover",i,sep="_")],dma_data[,normalization_variable])
      
      output_monthly = cbind(yrmo[i,],baseline,carryover)
      
      for (j in 1:length(promotions))
        
      {
        impact = as.data.frame(crossprod(dma_data[,paste(promotions[j],"impact",i,sep="_")],dma_data[,normalization_variable]))
        colnames(impact) = paste(promotions[j],"impact",sep="_")
        output_monthly = cbind(output_monthly,impact)
      }
      
    }
    if(i > mth_start){
      baseline_norm = sum(dma_data[,paste("baseline",i,sep="_")])
      carryover_norm = sum(dma_data[,paste("carryover",i,sep="_")])
      
      output_monthly_norm_1 = cbind(yrmo[i,],baseline_norm,carryover_norm)
      
      for (j in 1:length(promotions))
        
      {
        impact = as.data.frame(sum(dma_data[,paste(promotions[j],"impact",i,sep="_")]))
        colnames(impact) = paste(promotions[j],"norm_impact",sep="_")
        output_monthly_norm_1 = cbind(output_monthly_norm_1,impact)
      }
      
      baseline = crossprod(dma_data[,paste("baseline",i,sep="_")],dma_data[,normalization_variable])
      carryover = crossprod(dma_data[,paste("carryover",i,sep="_")],dma_data[,normalization_variable])
      
      output_monthly_1 = cbind(yrmo[i,],baseline,carryover)
      
      for (j in 1:length(promotions))
        
      {
        impact = as.data.frame(crossprod(dma_data[,paste(promotions[j],"impact",i,sep="_")],dma_data[,normalization_variable]))
        colnames(impact) = paste(promotions[j],"impact",sep="_")
        output_monthly_1 = cbind(output_monthly_1,impact)
      }
      
      output_monthly = rbind(output_monthly,output_monthly_1)
      output_monthly_norm = rbind(output_monthly_norm,output_monthly_norm_1)
    }
  }
  
  output_monthly = output_monthly %>% mutate(promotional_impact = output_monthly %>% select(ends_with("_impact")) %>% rowSums())
  output_monthly$total_predicted = output_monthly$baseline+output_monthly$carryover+output_monthly$promotional_impact
  output_monthly$mth = NULL
  actual = aggregate(master_2[,original_variable]~master_2[,"yrmo"],data = master_2[which(master_2$yrmo>=201901),], FUN = sum)
  colnames(actual) = c("yrmo","total_actual")
  output_monthly = merge(output_monthly,actual)
  
  output_monthly_norm = output_monthly_norm %>% mutate(promotional_norm_impact = output_monthly_norm %>% select(ends_with("_norm_impact")) %>% rowSums())
  output_monthly_norm$total_norm_predicted = output_monthly_norm$baseline+output_monthly_norm$carryover+output_monthly_norm$promotional_norm_impact
  output_monthly_norm$mth = NULL
  actual = aggregate(master_2[,dependent_variable]~master_2[,"yrmo"],data = master_2[which(master_2$yrmo>=201901),], FUN = sum)
  colnames(actual) = c("yrmo","total_norm_actual")
  output_monthly_norm = merge(output_monthly_norm,actual)
  
  # Monthly normalization
  
  output_monthly = merge(output_monthly,mth_norm)
  
  output_monthly[2:(ncol(output_monthly)-2)] = lapply(output_monthly[2:(ncol(output_monthly)-2)], '*', output_monthly[monthly_normalization])
  
  out$output_monthly = output_monthly
  out$output_monthly_norm = output_monthly_norm
  
  Baseline = sum(output_monthly$baseline)/sum(output_monthly$total_predicted)
  Carryover = sum(output_monthly$carryover)/sum(output_monthly$total_predicted)
  
  baseline_percent = as.data.frame("Baseline")
  baseline_percent$sum = sum(output_monthly$baseline)
  baseline_percent$percent = Baseline
  colnames(baseline_percent) = c("var","sum","percent")
  
  carryover_percent = as.data.frame("Carryover")
  carryover_percent$sum = sum(output_monthly$carryover)
  carryover_percent$percent = Carryover
  colnames(carryover_percent) = c("var","sum","percent")
  
  impact_percent = rbind(baseline_percent,carryover_percent)
  
  for (i in 1:length(promotions)) {
    var = paste(promotions[i],"impact",sep = "_")
    var_impact = sum(output_monthly[,paste(promotions[i],"impact",sep="_")])/sum(output_monthly$total_predicted)
    
    var_impact_percent = as.data.frame(promotions[i])
    var_impact_percent$sum = sum(output_monthly[,paste(promotions[i],"impact",sep="_")])
    var_impact_percent$percent = var_impact
    colnames(var_impact_percent) = c("var","sum","percent")
    impact_percent = rbind(impact_percent,var_impact_percent)
  }
  
  # Normalized variables % impactables
  Baseline_norm = sum(output_monthly_norm$baseline_norm)/sum(output_monthly_norm$total_norm_predicted)
  Carryover_norm = sum(output_monthly_norm$carryover_norm)/sum(output_monthly_norm$total_norm_predicted)
  
  baseline_norm_percent = as.data.frame("Baseline_norm")
  baseline_norm_percent$sum = sum(output_monthly_norm$baseline_norm)
  baseline_norm_percent$percent = Baseline_norm
  colnames(baseline_norm_percent) = c("var","sum","percent")
  
  carryover_norm_percent = as.data.frame("Carryover_norm")
  carryover_norm_percent$sum = sum(output_monthly_norm$carryover_norm)
  carryover_norm_percent$percent = Carryover_norm
  colnames(carryover_norm_percent) = c("var","sum","percent")
  
  impact_norm_percent = rbind(baseline_norm_percent,carryover_norm_percent)
  
  for (i in 1:length(promotions)) {
    var = paste(promotions[i],"norm_impact",sep = "_")
    var_norm_impact = sum(output_monthly_norm[,paste(promotions[i],"norm_impact",sep="_")])/sum(output_monthly_norm$total_norm_predicted)
    
    var_norm_impact_percent = as.data.frame(promotions[i])
    var_norm_impact_percent$sum = sum(output_monthly_norm[,paste(promotions[i],"norm_impact",sep="_")])
    var_norm_impact_percent$percent = var_norm_impact
    colnames(var_norm_impact_percent) = c("var","sum","percent")
    impact_norm_percent = rbind(impact_norm_percent,var_norm_impact_percent)
    
  }
  
  out$impact_percent = impact_percent
  out$impact_norm_percent = impact_norm_percent
  
  # Error calculations
  non_normalized_error = sum(output_monthly$total_predicted)/sum(output_monthly$total_actual)-1
  normalized_error = sum(output_monthly_norm$total_norm_predicted)/sum(output_monthly_norm$total_norm_actual)-1
  
  out$non_normalized_error = non_normalized_error
  out$normalized_error = normalized_error
  
  return(out)
  
}

PRM1 = function(dataset,model_equation,start,end){
  
  formulafix <- model_equation
  data = dataset[which(dataset$yrmo>= start & dataset$yrmo <= end),]
  
  a1<-strsplit(formulafix,"~",fixed = T)[[1]][2]
  b1<-trimws(strsplit(a1,"+",fixed = T)[[1]])
  
  b<-c(dependent_variable,b1)
  
  coeff_name <-c("First_intercept",paste0("A_",b1))
  neg_var<-vector()
  k=1
  while (k == 1) {
    
    b1 <- b1[!(b1 %in% neg_var)]
    formulafix <- paste(dependent_variable,paste(b1,collapse = " + "),sep = " ~ ")
    
    coeff_name <- coeff_name[!(coeff_name %in% paste0("A_",neg_var))]
    
    fit <- lm(as.formula(formulafix), data = data)
    
    lm <- as.list(coef(fit))
    names(lm) <- coeff_name
    
    phi = ifelse(mean(data[[dependent_variable]])>0,Sales_lower*mean(data[[dependent_variable]]),0)
    
    lm$First_intercept <- ifelse(lm$First_intercept>phi, lm$First_intercept, phi)
    
    lm[lm<0] <- 0
    
    b2 <- c("First_intercept",paste(names(lm)[2:length(names(lm))],b1,sep = "*"))
    nls_formula <- paste(dependent_variable,paste(b2,collapse = " + "),sep = " ~ ")
    
    lower_bounds <- c(phi,rep(0,length(lm)-1))
    # lower_bounds = c(phi,0,0)
    
    set.seed(79)
    nl_fit <- tryCatch(nls(as.formula(nls_formula),
                           data=data,
                           start=lm,
                           algorithm="port",
                           lower=lower_bounds
    ),error = function(e)  lm(as.formula(formulafix), data = data))
    
    summary_coef_1 <- summary(nl_fit)$coefficients[,c(1,4)]
    
    
    coef_1 <- cbind("variable" = rownames(summary_coef_1),as.data.frame(summary_coef_1))
    colnames(coef_1) <- c("variable","Estimate","p_value")
    
    
    
    if(sum(coef_1[coef_1$variable %in%
                  c(paste("A_",dependent_variable,"_1",sep = ""),paste("A_",dependent_variable,"_2",sep = ""),paste("A_",dependent_variable,"_3",sep = "")),]$Estimate) > lag_sum_criteria)
    {
      luke <- coef_1[coef_1$variable %in%
                       c(paste("A_",dependent_variable,"_1",sep = ""),paste("A_",dependent_variable,"_2",sep = ""),paste("A_",dependent_variable,"_3",sep = "")),]
      
      if (sales_lag_condition==1){
        
        luke$Asym <- (luke$Estimate*single_lag)/sum(luke$Estimate)
        
      } else if (sales_lag_condition==2) {
        
        luke$Asym <- c(single_lag)
        
      } else if (sales_lag_condition==3) {
        
        luke$Asym <- c(0,0)
        
      }
      
      lm[2:2] <- luke$Asym
      
      upper_bounds <- c(1000000,luke$Asym,rep(1000000,length(lm)-2))
      # upper_bounds = c(1000000,luke$Asym,1000000,0.75,1000000,1000000,1000000)
      
      # upper_bounds <- c(0.4,luke$Asym,1000000)
      
      #upper_bound_df <- data.frame(varibale = names(lm), value = as.numeric(upper_bounds))
      
      nl_fit <- tryCatch(nls(as.formula(nls_formula),
                             data=data,
                             start=lm,
                             algorithm="port",
                             lower = lower_bounds,
                             upper = upper_bounds
      ),error = function(e)  lm(as.formula(formulafix), data = data))
      
      summary_coef <- summary(nl_fit)$coefficients[,c(1,4)]
      
      coef <- cbind("variable" = rownames(summary_coef),as.data.frame(summary_coef))
      colnames(coef) <- c("variable","Estimate","p_value")
    }  else {
      coef <- coef_1
      
    }
    
    neg_var <- as.vector(coef[coef$Estimate<0,"variable"])
    
    if (length(neg_var) == 0 | (length(neg_var) == 1 & neg_var[1] == "(Intercept)"))  k = 0
    
  }   
  
  
  cf <- data.frame(coef,
                   r_square = summary(fit)$r.squared,
                   adj_r_squared=summary(fit)$adj.r.squared,
                   AIC(nl_fit),
                   AIC(fit),
                   BIC(nl_fit),
                   BIC(fit),
                   RMSE  = sqrt(mean((data[[dependent_variable]]-fitted(nl_fit))^2)))
  return(cf)
}

# RE related Functions
coalesce_df <- cmpfun(function(x, y, join_by = NULL, replace_which = NULL){
  
  if(nrow(y) == 1 | is.null(join_by) | is.null(replace_which)){
    
    y <- y %>% slice(1)
    
  }else{
    
    joined <- x %>% left_join(y %>% select(join_by, replace_which), by = join_by, suffix = c("","_replace_with"))
    
    y <- joined %>% select(colnames(joined)[str_detect(colnames(joined), "_replace_with$")])
    
    colnames(y) <- str_replace_all(colnames(y), "_replace_with$", "")
    
  }
  common_cols <- intersect(colnames(x), colnames(y))
  
  x[, common_cols] <- mapply(fill_x_y, x[, common_cols], y[, common_cols])
  
  return(x)
  
})

nonzero_value_count <- cmpfun(function(x){
  if(any(x > 0)){return(length(unique(x[x>0])))}
  return(0)
})

distinct_value_count <- cmpfun(function(x){
  return(length(unique(x)))
})

get_furrr_options <- function(packages = setdiff((.packages()), c("stats","graphics","grDevices","utils","datasets","methods","base","furrr", "sparklyr")), scheduling = 1){
  return(future_options(packages = packages, scheduling = scheduling))
}

get_r_e_estimates <- cmpfun(function(data, f_e_formula, r_e_terms, r_e_factor, min_distinct_obs = 3, r_e_formula = NULL, get_relative_change = FALSE){
  
  if(any(data %>% select(all.vars(f_e_formula)) %>% summarize_all(sum) == 0)){
    
    return(list(NULL))
    
  }
  
  if(is.null(r_e_formula)){
    r_e_formula <- paste0(f_e_formula, paste0(" + (1 + ", paste0(r_e_terms, collapse = " + "), "| r_e_id_internal)"))
  }
  
  numeric_ids <- data %>% 
    select(r_e_factor) %>% 
    distinct %>% 
    rowid_to_column("r_e_id_internal") 
  
  data <- suppressWarnings(data %>%
                             inner_join(numeric_ids, by = r_e_factor) %>%
                             group_by(!!sym(r_e_factor)) %>%
                             mutate(y_value_count = distinct_value_count(!!sym(lhs.vars(f_e_formula))),
                                    y_value_nonzero_count = nonzero_value_count(!!sym(lhs.vars(f_e_formula)))) %>%
                             ungroup %>%
                             mutate(r_e_id_internal = r_e_id_internal + 1,
                                    r_e_id_internal = ifelse(y_value_count < min_distinct_obs, 1, r_e_id_internal),
                                    r_e_id_internal = ifelse(r_e_id_internal == 1 & (y_value_nonzero_count < min_distinct_obs), 0, r_e_id_internal)) %>%
                             select(-y_value_count, -y_value_nonzero_count))
  
  original_id_mapping <- data %>% select(r_e_factor, r_e_id_internal) %>% distinct
  
  # Handle cases where no observations have enough distinct values
  if(length(unique(data$r_e_id_internal)) == 1){
    return(NULL)
  }
  
  fit <- suppressMessages(suppressWarnings(lme4::lmer(r_e_formula,
                                                      data = data,
                                                      control = lmerControl(optimizer = "optimx", 
                                                                            optCtrl=list(method="nlminb", starttests = FALSE)))))
  
  if(any(is.na(fixef(fit)))){ return(list(NULL)) }
  
  combined <- coef(fit)[[1]] %>% 
    rename(intercept = `(Intercept)`) %>%
    mutate(r_e_id_internal := as.numeric(row.names(.))) %>%
    inner_join(original_id_mapping, by = "r_e_id_internal") %>%
    select(r_e_factor, everything(), -r_e_id_internal)
  
  fixed <- enframe(fixef(fit)) %>% 
    filter(value > 0 & (name %in% r_e_terms)) %>%
    pivot_wider() 
  
  if(nrow(fixed) == 0 | all(combined[, r_e_terms, drop = F] > 0)){ 
    
    return(combined) 
    
  }else{
    
    valid_r_e <- intersect(r_e_terms, colnames(fixed))
    combined[, valid_r_e][is_negative(combined[, valid_r_e])] <- NA
    return(as_tibble(coalesce_df(combined, fixed)))
    
  }
  
  return(combined) 
  
})
