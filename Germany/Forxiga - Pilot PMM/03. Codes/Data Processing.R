rm(list = ls())
library(openxlsx)
library(dplyr)
library(reshape2)

setwd("C:/Users/knss680/Box Sync/Forxiga - Call Planning 2022/Raw Data")

input = read.xlsx("Calls - New Data.xlsx")
df = input
names(df)[names(df)=="1ST_DET"] = "First_Det"
names(df)[names(df)=="2ND_DET"] = "Second_Det"
names(df)[names(df)=="3RD_DET"] = "Third_Det"

names(df)
brands = c("Forxiga_DIA_T2_DE","Forxiga CKD_DE","Forxiga HF_DE","Xigduo_DE")

df1 = df %>% 
  filter(First_Det %in% brands | Second_Det  %in% brands | Third_Det  %in% brands)

# write.csv(df1, "Forxiga_Family_Calls.csv")

df2 = df1 %>% 
  filter(Monat != "Nov")

names(df2)
unique(df2$FACT_ACTT_DESC_KK)

# temp = dcast(df2, "Monat ~ FACT_ACTT_DESC_KK", value.var = "ACT_ID")
# sum(colSums(temp[-1]))

san_channels = c("Santis Besuch", "Santis Email", "Santis Telefon", 
                 "Santis Video Call", "Santis Fax", "Santis Brief", 
                 "Santis Veeva Engage", "SAN Arbeitsessen", "Santis Service")
unique(df2$FACT_ACTT_DESC_KK)
San_df = df2 %>% 
  filter(FACT_ACTT_DESC_KK %in% san_channels)

# write.csv(San_df, "San_Calls_Ex_Nov.csv", row.names = F)
unique(San_df$First_Det)
length(unique(San_df$ACT_ID))

df_1_NPD = San_df %>% 
  filter(First_Det == "No Product_DE")

df_1_NPD1 = df_1_NPD
df_1_NPD1$First_Det = df_1_NPD1$Second_Det
df_1_NPD1$Second_Det = df_1_NPD1$Third_Det
df_1_NPD1$Third_Det = "NV"

San_df_v1 = San_df %>% 
  filter(!(ACT_ID %in% df_1_NPD$ACT_ID)) %>% 
  union(df_1_NPD1)

df_2_NPD = San_df_v1 %>% 
  filter(Second_Det == "No Product_DE")
unique(df_2_NPD$First_Det)
unique(df_2_NPD$Second_Det)

df_2_NPD1 = df_2_NPD
df_2_NPD1$Second_Det = df_2_NPD1$Third_Det
df_2_NPD1$Third_Det = "NV"

San_df_v2 = San_df_v1 %>% 
  filter(!(ACT_ID %in% df_2_NPD$ACT_ID)) %>% 
  union(df_2_NPD1)

df_3_NPD = San_df_v2 %>% 
  filter(Third_Det == "No Product_DE")
unique(df_3_NPD$First_Det)
unique(df_3_NPD$Second_Det)
unique(df_3_NPD$Third_Det)

df_3_NPD1 = df_3_NPD
df_3_NPD1$Third_Det = "NV"

San_df_v3 = San_df_v2 %>% 
  filter(!(ACT_ID %in% df_3_NPD$ACT_ID)) %>% 
  union(df_3_NPD1)

unique(San_df_v3$First_Det)
unique(San_df_v3$Second_Det)
unique(San_df_v3$Third_Det)

# write.csv(San_df_v3,"Processed_Santis_Calls_Ex_Nov.csv", row.names = F)

length(unique(San_df_v3$ACT_ID))

# PDE Calculation (Forxiga)
df3 = San_df_v3 %>% 
  select(ACT_ID, First_Det,Second_Det,Third_Det)

unique(df3$First_Det)          # Forxiga_DIA_T2_DE, Xigduo_DE

a = unlist(apply(df3[-1],1, function(x) which(x=="Forxiga_DIA_T2_DE")), use.names = F)
df3$Forxiga = apply(df3[-1],1, function(x) which(x=="Forxiga_DIA_T2_DE"))

idx <- !(sapply(df3$Forxiga, length))
df3$Forxiga[idx] <- 10
unique(df3$Forxiga)

df3$Forxiga = unlist(df3$Forxiga)

# PDE Calculation (Xigduo)
df3$Xigduo = apply(df3[-1],1, function(x) which(x=="Xigduo_DE"))

idx1 <- !(sapply(df3$Xigduo, length))
df3$Xigduo[idx1] <- 10
unique(df3$Xigduo)

df3$Xigduo = unlist(df3$Xigduo)

df4 = df3 %>% 
  rowwise() %>% 
  mutate(Forxiga_Family = min(Forxiga,Xigduo))

df5 = df4
df5[df5 == "Xigduo_DE"] <- "Forxiga_DIA_T2_DE"
df5$Forxiga_Family_Count = rowSums(df5[-1] == "Forxiga_DIA_T2_DE")
df5$NV_Count = rowSums(df5[-1] == "NV")


df5 = df5 %>% 
  rowwise() %>% 
  mutate(Product_Count = 3- Forxiga_Family_Count+1- NV_Count)


San_df_v4 = merge(San_df_v3,df5[-c(2,3,4)], by.x = "ACT_ID", by.y = "ACT_ID", all.x = T, all.y = T)

San_df_v5 = San_df_v4 %>% 
  rowwise() %>% 
  mutate(New_PDEs = ifelse(Product_Count == 3, ifelse(Forxiga_Family==1,0.6,
                                                  ifelse(Forxiga_Family==2,0.3,0.1)),
                       ifelse(Product_Count==2,ifelse(Forxiga_Family==1,0.7,0.3),1)))


write.csv(San_df_v5, "Processed_Santis_Calls_Ex_Nov_PDEs.csv", row.names = F)











