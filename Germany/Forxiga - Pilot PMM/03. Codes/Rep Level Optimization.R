rm(list = ls())
# Importing libraries
library(openxlsx)
library(tidyr)
library(dplyr)
library(zoo)
library(reshape2)
library(ggplot2)
library(factoextra)
library(NbClust)
library(cluster)
library(fpc)
library(dbscan)
library(ztable)
library(ggpubr)

setwd('C:/Users/knss680/Box Sync/Forxiga - Call Planning 2022/Raw Data')
input = read.xlsx('Rep WB - Input Data (40 Terr).xlsx')
# input$historical_interactions = NULL
# input$historical_interactions = input$historical_interactions_old

# input = read.xlsx('Final Processed Data.xlsx', sheet = "Rep WB - Input(Rep)")
colSums(is.na(input))
str(input)
max_bound = 500000
lower_bound = 0
input$max_bound = input$historical_interactions*max_bound
dist = input%>%distinct(Sector,Sector.constraint,cost_per_touch_point)

input_c = input[which(input$Sector==dist[1,1]),]
# input_c = input[which(input$Sector==dist[1,1]),]
step = 10
historical = mean(input_c$Sector.constraint)*mean(input_c$cost_per_touch_point)
historical = 100*round(historical/100)
trx_c = 1.34
cost_pde = 116.947623608384
co_factor = 1
n = historical/step

for (i in (1:n)) {
  
  if(i == 1)
  {
    # 1st step run
    
    # input_c$activity = step/input_c$cost_per_touch_point    
    input_c$activity = input_c$historical_interactions*lower_bound
    input_c$step_return = input_c$lt_a * (1-exp(-1*input_c$c*input_c$activity))
    
    # max = input_c[which(input_c$activity <= input_c$max_bound),]
    # max = max[which(max$step_return == max(max$step_return)),]
    # max = max[1,]
    
    # input_c$max = ifelse(input_c$minibrick == (max$minibrick) & input_c$channel == (max$channel),1,0)
    # input_c$allocated_return = ifelse(input_c$max==1,input_c$step_return,0)
    # input_c$allocated_activity = ifelse(input_c$max==1,input_c$activity,0)
    input_c$allocated_return = input_c$step_return
    input_c$allocated_activity = input_c$activity
    
  }
  if(i ==2)
  {
    # 2nd step
    
    input_c$activity_1 = step/input_c$cost_per_touch_point
    input_c$new_activity = input_c$activity+input_c$activity_1
    # input_c$incremental_return = input_c$lt_a * (1-exp(-1*input_c$c*input_c$new_activity)) - input_c$allocated_return
    input_c$incremental_return = (((input_c$lt_a*co_factor*input_c$c*trx_c* (exp(-1*input_c$c*input_c$new_activity)) - cost_pde)/cost_pde)+1)
    
    
    max = input_c[which(input_c$new_activity <= input_c$max_bound),]
    max = max[which(max$incremental_return == max(max$incremental_return)),]
    max = max[1,]
    
    input_c$max = ifelse(input_c$minibrick == (max$minibrick) & input_c$channel == (max$channel),1,0)
    # input_c$allocated_return = ifelse(input_c$max==1,input_c$allocated_return + input_c$incremental_return,input_c$allocated_return)
    input_c$allocated_activity = ifelse(input_c$max==1,input_c$new_activity,input_c$allocated_activity)
  }
  if(i > 2 )
  {
    # 3rd step
    input_c$activity_2 = step/input_c$cost_per_touch_point
    input_c$new_activity = ifelse(input_c$max==1,input_c$activity_2+input_c$allocated_activity,input_c$allocated_activity)
    # input_c$incremental_return = input_c$lt_a * (1-exp(-1*input_c$c*input_c$new_activity)) - input_c$allocated_return
    input_c$incremental_return = ((input_c$lt_a*co_factor*input_c$c*trx_c* (exp(-1*input_c$c*input_c$new_activity)) - cost_pde)/cost_pde)+1
    
    max = input_c[which(input_c$new_activity <= input_c$max_bound),]
    if( nrow(max)==0){break}
    max = max[which(max$incremental_return == max(max$incremental_return)),]
    max = max[1,]
    
    input_c$max = ifelse(input_c$minibrick == (max$minibrick) & input_c$channel == (max$channel),1,0)
    # input_c$allocated_return = ifelse(input_c$max==1,input_c$allocated_return + input_c$incremental_return,input_c$allocated_return)
    input_c$allocated_activity = ifelse(input_c$max==1,input_c$allocated_activity+input_c$activity_2,input_c$allocated_activity)
    # if(sum(input_c$allocated_activity)>=sum(input_c$historical_interactions)){
    if(sum(input_c$allocated_activity)>=sum(input_c$historical_interactions) | sum(input_c$allocated_activity)>=mean(input_c$Sector.constraint)){
      break
    }
    
  }
}

input_c$spend = input_c$cost_per_touch_point*input_c$allocated_activity
# input_c$return_on_investment = (input_c$allocated_return * trx_c) / input_c$spend

output = input_c

for (s in 2:nrow(dist)){
  
  input_c = input[which(input$Sector==dist[s,1]),]
  # historical  = mean(input_c$Sector.constraint)*mean(input_c$cost_per_touch_point)*(1-lower_bound)
  historical  = mean(input_c$Sector.constraint)*mean(input_c$cost_per_touch_point)
  historical = 10*round(historical/10)
  trx_c = 1.34
  cost_pde = 116.947623608384
  co_factor = 1
  n = historical/step
  # n = 5
  for (i in (1:n)) {
    
    if(i == 1)
    {
      # 1st step run
      
      # input_c$activity = step/input_c$cost_per_touch_point    
      input_c$activity = input_c$historical_interactions*lower_bound
      input_c$step_return = input_c$lt_a * (1-exp(-1*input_c$c*input_c$activity))
      
      # max = input_c[which(input_c$activity <= input_c$max_bound),]
      # max = max[which(max$step_return == max(max$step_return)),]
      # max = max[1,]
      
      # input_c$max = ifelse(input_c$minibrick == (max$minibrick) & input_c$channel == (max$channel),1,0)
      # input_c$allocated_return = ifelse(input_c$max==1,input_c$step_return,0)
      # input_c$allocated_activity = ifelse(input_c$max==1,input_c$activity,0)
      input_c$allocated_return = input_c$step_return
      input_c$allocated_activity = input_c$activity
      
    }
    if(i ==2)
    {
      # 2nd step
      
      input_c$activity_1 = step/input_c$cost_per_touch_point
      input_c$new_activity = input_c$activity+input_c$activity_1
      # input_c$incremental_return = input_c$lt_a * (1-exp(-1*input_c$c*input_c$new_activity)) - input_c$allocated_return
      input_c$incremental_return = ((input_c$lt_a*co_factor*input_c$c*trx_c* (exp(-1*input_c$c*input_c$new_activity)) - cost_pde)/cost_pde)+1
      
      max = input_c[which(input_c$new_activity <= input_c$max_bound),]
      max = max[which(max$incremental_return == max(max$incremental_return)),]
      max = max[1,]
      
      input_c$max = ifelse(input_c$minibrick == (max$minibrick) & input_c$channel == (max$channel),1,0)
      # input_c$allocated_return = ifelse(input_c$max==1,input_c$allocated_return + input_c$incremental_return,input_c$allocated_return)
      input_c$allocated_activity = ifelse(input_c$max==1,input_c$new_activity,input_c$allocated_activity)
    }
    if(i > 2 )
    {
      # 3rd step
      input_c$activity_2 = step/input_c$cost_per_touch_point
      input_c$new_activity = ifelse(input_c$max==1,input_c$activity_2+input_c$allocated_activity,input_c$allocated_activity)
      # input_c$incremental_return = input_c$lt_a * (1-exp(-1*input_c$c*input_c$new_activity)) - input_c$allocated_return
      input_c$incremental_return = ((input_c$lt_a*co_factor*input_c$c*trx_c* (exp(-1*input_c$c*input_c$new_activity)) - cost_pde)/cost_pde)+1
      
      max = input_c[which(input_c$new_activity <= input_c$max_bound),]
      if(nrow(max)==0){break}
      max = max[which(max$incremental_return == max(max$incremental_return)),]
      max = max[1,]
      
      input_c$max = ifelse(input_c$minibrick == (max$minibrick) & input_c$channel == (max$channel),1,0)
      # input_c$allocated_return = ifelse(input_c$max==1,input_c$allocated_return + input_c$incremental_return,input_c$allocated_return)
      input_c$allocated_activity = ifelse(input_c$max==1,input_c$allocated_activity+input_c$activity_2,input_c$allocated_activity)
      if(sum(input_c$allocated_activity)>=sum(input_c$historical_interactions) | sum(input_c$allocated_activity)>=mean(input_c$Sector.constraint)){
      # if(sum(input_c$allocated_activity)>=sum(input_c$historical_interactions)){
        break
      }
      
    }
  }
  
  input_c$spend = input_c$cost_per_touch_point*input_c$allocated_activity
  # input_c$return_on_investment = (input_c$allocated_return * trx_c) / input_c$spend
  
  output = rbind(input_c,output)
  
}

sum(output$allocated_activity)

sum(output$historical_interactions)


write.csv(output,"FLSM_Cap_output_Unconstrained_V2.csv",row.names = FALSE)




