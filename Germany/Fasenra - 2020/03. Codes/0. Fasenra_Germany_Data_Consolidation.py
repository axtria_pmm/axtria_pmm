'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'
'###############################################################################'
'############################## Sales Data #####################################'
'###############################################################################'
'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'
# Importing Libraries

import pandas as pd
import numpy as np
import os
from functools import reduce
import pandasql as ps
pd.set_option('display.float_format', lambda x: '%.2f' % x)

###############################################################################
# Processed File Directory
input_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Raw Data'
processed_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Master Data/Intermediate Datasets'
master_data_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Master Data'

###############################################################################
# Reading Input data
sales = pd.read_excel(str(input_path)+"/Sales Raw data_Fasenra_AK.xlsx")
sales.info()

###############################################################################
# Converting into actionable format
date_col = 'ZEUMS_DATUM'
sales['ZEUMS_DATUM'].head(20)
def date_trans(df, col, form):
    df[col] = pd.to_datetime(df[col], format = form)
    df['YEAR'] = df[col].dt.year
    df['MONTH'] = df[col].dt.month
    df['YRMO'] = df[col].dt.strftime('%Y%m').astype('int')
    return df

date_trans(df = sales, col = date_col, form = '%d.%m.%Y')

# Rename Columns
def colClean(df):
    df.columns = df.columns.str.upper()
    df.columns = df.columns.str.replace(' ', '')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('+', '_')
    return df

colClean(sales)
sales.info()
sales['ZEIG_NAME'].unique()

mkt = sales.loc[~sales['ZEIG_NAME'].isin(['NUCALA + IMP. N1', 'NUCALA + IMP. N3','SON']),].sort_values(['ZEUMS_ZZEL_ID','YRMO'])
mkt1 = mkt.pivot(index = ['ZEUMS_ZZEL_ID','YRMO'], columns = 'ZEIG_NAME')[['VOL','VAL','DDD']].reset_index()
mkt1.columns = [f'{col[1]}_{col[0]}' for col in mkt1.columns]
colClean(mkt1)
mkt1.rename(columns = {'_ZEUMS_ZZEL_ID':'BRICK','_YRMO':'YRMO'}, inplace = True)
mkt1.columns
mkt1['MARKET_DDD_SALES'] = mkt1['CINQAERO_DDD'] + mkt1['DUPIXENT_ASTHMA_DDD'] + mkt1['FASENRA_DDD'] + mkt1['NUCALA_IMP_DDD'] + mkt1['XOLAIR_IMP_DDD']
mkt1['COMP_DDD_SALES'] = mkt1['MARKET_DDD_SALES'] - mkt1['FASENRA_DDD']
mkt1['MARKET_DDD_SALES'].sum()
mkt1['COMP_DDD_SALES'].sum()

comp_growth = mkt1[['YRMO','COMP_DDD_SALES']].groupby(['YRMO']).agg(np.sum).reset_index()
comp_growth['COMP_GROWTH'] = (comp_growth['COMP_DDD_SALES'] - comp_growth['COMP_DDD_SALES'].shift(1))/comp_growth['COMP_DDD_SALES'].shift(1)
comp_growth['COMP_GROWTH'].fillna(0, inplace = True)

mkt1 = pd.merge(mkt1,comp_growth[['YRMO','COMP_GROWTH']], how = 'left',left_on = 'YRMO', right_on = 'YRMO')

mkt1.to_excel(str(processed_path)+'/Fasenra_Sales_Data.xlsx',index = False)


'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'
'###############################################################################'
'############################## Meetings Data ##################################'
'###############################################################################'
'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'

# Reading Input data
Attendees = pd.read_csv(str(input_path)+'/05_HCP_Promotional_Activity_EVENT_ATTENDEE.csv', delimiter = ';')
event = pd.read_csv(str(input_path)+'/05_HCP_Promotional_Activity_EVENT.csv',delimiter=';')
hcp_detail = pd.read_excel(str(input_path)+'/01_HCP_List_and_Details_HCP.xlsx', sheet_name='Sheet1')
mapping_df = pd.read_csv(str(input_path)+"/05_Country_Territory_Cut.csv")
brick_sub_terr_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'brick to sub terr')
sub_terr_area_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'sub terr to area')
area_province_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'final')

###############################################################################
# Getting Brick info using mapping file
mapping_df.info()
mapping_df1 = mapping_df.loc[mapping_df['TERRITORYLEVELGLOBAL']=='Minibrick',['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']].drop_duplicates()
mapping_df1['PARENTTERRITORYIDENTIFIER'].nunique()
mapping_df1['TERRITORYIDENTIFIER'] = mapping_df1['TERRITORYIDENTIFIER'].astype(int)
mapping_df1 = mapping_df1.rename(columns = {'PARENTTERRITORYIDENTIFIER':'BRICK'})
mapping_df1.info()

colClean(Attendees)
colClean(event)
colClean(hcp_detail)
colClean(mapping_df)
colClean(mapping_df1)
colClean(brick_sub_terr_mapp)
colClean(sub_terr_area_mapp)
colClean(area_province_mapp)

###############################################################################
# Merging all these files
df = pd.merge(Attendees, event[['EVT_ID','EVTT_NAME','EVTF_NAME','SOURCE_SYS',
                                'PRD_NAME']], how = 'left', left_on = 'XEVAT_EVT_ID', right_on = 'EVT_ID').drop_duplicates()

df1 = pd.merge(df,hcp_detail[['DCUS_ID','DCUS_CELL_ID','DCUS_CUST_ID','DORG_TYPE_NAME', 
                              'DCUS_PRIM_SPEC_NAME']], how = 'left', 
               left_on = 'XEVAT_CUS_ID', right_on = 'DCUS_ID')

df2 = pd.merge(df1,mapping_df1, how = 'left',left_on = 'DCUS_CELL_ID', right_on = 'TERRITORYIDENTIFIER')
df2.info()
df2 = df2.fillna('')

###############################################################################
# Transorming Date Columns
date_col = 'EVT_START_DATE'
df3 = date_trans(df2, col = date_col, form='%d.%m.%Y')
df3 = df3.sort_values(['BRICK','YRMO'])
df3.YRMO.unique()

###############################################################################
# Attendees Data
df3['PRD_NAME'].value_counts()
fasenra_att = df3.loc[(df3['XEVAT_CUS_ID']!=-1) & (df3['PRD_NAME'].isin(['Fasenra_DE'])) & (df3['BRICK']!='')    ,].drop_duplicates()
fasenra_att = fasenra_att[~fasenra_att['EVTT_NAME'].str.contains('Medical')]
fasenra_att['BRICK'] = fasenra_att['BRICK'].astype('int')
fasenra_att = pd.merge(fasenra_att, brick_sub_terr_mapp[['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']],
                   left_on = ['BRICK'], right_on = ['TERRITORYIDENTIFIER'], how = 'left').rename(columns = {'PARENTTERRITORYIDENTIFIER':'SUB_TERRITORY'})

fasenra_att = pd.merge(fasenra_att, sub_terr_area_mapp[['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']],
                   left_on = ['SUB_TERRITORY'], right_on = ['TERRITORYIDENTIFIER'], how = 'left').rename(columns = {'PARENTTERRITORYIDENTIFIER':'OLD_PROVINCE'})

fasenra_att = pd.merge(fasenra_att, area_province_mapp[['PARENTTERRITORYIDENTIFIER','NEWREGION']],
                   left_on = ['OLD_PROVINCE'], right_on = ['PARENTTERRITORYIDENTIFIER'], how = 'left').rename(columns = {'NEWREGION':'NEW_PROVINCE'})

fasenra_att.info()
fasenra_att.drop(['TERRITORYIDENTIFIER_x','TERRITORYIDENTIFIER_y',
              'PARENTTERRITORYIDENTIFIER'],axis = 1, inplace = True)

# Filtering required Events only
fasenra_att['EVTT_NAME'].value_counts()
excluded_events = ['CV KardioDialog & Trialog','Diab TAS sonstige regionale VA',
                   'CV DMP AZ','ONCO Lung IO']
fasenra_att = fasenra_att.loc[~(fasenra_att['EVTT_NAME'].isin(excluded_events) ),]

fasenra_att1 = fasenra_att.pivot_table(index = ['BRICK','YRMO'],
                         values = ['XEVAT_CUS_ID'], aggfunc = 'count').reset_index()

fasenra_att['EVTT_NAME'].value_counts()
fasenra_att1.loc[(fasenra_att1['YRMO'] >= 202001) & (fasenra_att1['YRMO'] <= 202012),].sum()

# Events Data
df3['PRD_NAME'].value_counts()
fasenra_evt = fasenra_att.groupby(['BRICK','YRMO'])['XEVAT_EVT_ID'].agg(lambda x:len(x.unique())).reset_index()
fasenra_evt1 = fasenra_att.groupby(['YRMO'])['XEVAT_EVT_ID'].agg(lambda x: len(x.unique())).reset_index()
fasenra_evt.sum()
fasenra_evt1.loc[(fasenra_evt1['YRMO'] >= 202001) & (fasenra_evt1['YRMO'] <= 202012),].sum()

###############################################################################
# Combining Attnedees and Events Data
meetings_final = pd.merge(fasenra_att1,fasenra_evt, how = 'outer',
                          left_on = ['BRICK','YRMO'], right_on = ['BRICK','YRMO'])
meetings_final.columns = ['BRICK','YRMO','Attendees','Events']
meetings_final.sum()
meetings_final.loc[(meetings_final['YRMO'] >= 202001) & (meetings_final['YRMO'] <= 202012),].sum()
meetings_final.to_excel(str(processed_path)+'/Meetings_Final.xlsx',index = False)




'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'
'###############################################################################'
'############################## Calls Data #####################################'
'###############################################################################'
'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'

###############################################################################
# Reading Input data 
call = pd.read_csv(str(input_path)+"/05_HCP_Promotional_Activity_CALL.csv",delimiter = ';')
mapping_df = pd.read_csv(str(input_path)+"/05_Country_Territory_Cut.csv")
hcp_detail = pd.read_excel(str(input_path)+'/01_HCP_List_and_Details_HCP.xlsx', sheet_name='Sheet1')
brick_sub_terr_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'brick to sub terr')
sub_terr_area_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'sub terr to area')
area_province_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'final')
colClean(call)
colClean(mapping_df)
colClean(hcp_detail)
colClean(brick_sub_terr_mapp)
colClean(sub_terr_area_mapp)
colClean(area_province_mapp)

###############################################################################
# Getting Brick info using mapping file
mapping_df.info()
mapping_df1 = mapping_df.loc[mapping_df['TERRITORYLEVELGLOBAL']=='Minibrick',['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']].drop_duplicates()
mapping_df1['PARENTTERRITORYIDENTIFIER'].nunique()
mapping_df1['TERRITORYIDENTIFIER'] = mapping_df1['TERRITORYIDENTIFIER'].astype(int)
mapping_df1.info()

###############################################################################
# Calls Data Exploration
call1 = call.copy()
call1.columns

# Checking Date Column and creating new DateTime variables
call1['FACT_DATE'].unique()
date_col = 'FACT_DATE'
call1['FACT_DATE'].head()

date_trans(df = call1, col = date_col, form = '%d.%m.%Y')
call1.YRMO.unique()
call1.info()
call1 = call1.sort_values(['YRMO'])

###############################################################################
# Merging Calls Data with Territory Mapping File to get Brick info
call1.info()
call2 = pd.merge(call1,mapping_df1[['TERRITORYIDENTIFIER', 'PARENTTERRITORYIDENTIFIER']], how = 'inner', left_on = 'FACT_DCELL_ID_1', right_on = 'TERRITORYIDENTIFIER')

# Renaming all the column names by removing any special character or spaces
call2 = colClean(call2)
call2.columns

# Renaming Brick Column
call2.rename(columns = {'PARENTTERRITORYIDENTIFIER':'BRICK'}, inplace=True)
call2['BRICK'].nunique()

###############################################################################
# Exploring HCP Details and merging with the Calls Data
hcp_detail.info()
call3 = pd.merge(call2, hcp_detail, left_on = ['FACT_DCUS_ID'], right_on = ['DCUS_ID'], how = 'left')
call3.info()
call3['BRICK'] = call3['BRICK'].astype('int')

###############################################################################
# Merging Province information
brick_sub_terr_mapp.columns
call4 = pd.merge(call3, brick_sub_terr_mapp[['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']],
                   left_on = ['BRICK'], right_on = ['TERRITORYIDENTIFIER'], how = 'left').rename(columns = {'PARENTTERRITORYIDENTIFIER':'SUB_TERRITORY'})

call4 = pd.merge(call4, sub_terr_area_mapp[['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']],
                   left_on = ['SUB_TERRITORY'], right_on = ['TERRITORYIDENTIFIER'], how = 'left').rename(columns = {'PARENTTERRITORYIDENTIFIER':'OLD_PROVINCE'})

call4 = pd.merge(call4, area_province_mapp[['PARENTTERRITORYIDENTIFIER','NEWREGION']],
                   left_on = ['OLD_PROVINCE'], right_on = ['PARENTTERRITORYIDENTIFIER'], how = 'left').rename(columns = {'NEWREGION':'NEW_PROVINCE'})

call4.info()
call4.drop(['TERRITORYIDENTIFIER_x','TERRITORYIDENTIFIER_y',
              'PARENTTERRITORYIDENTIFIER'],axis = 1, inplace = True)

###############################################################################
# Required Summaries and EDA
fasenra_calls = call4.loc[(call4['FIRST_DETAIL'] == 'Fasenra_DE') | (call4['SECOND_DETAIL'] == 'Fasenra_DE') | (call4['THIRD_DETAIL'] == 'Fasenra_DE') | (call4['FOURTH_DETAIL'] == 'Fasenra_DE') | (call4['FIFTH_DETAIL'] == 'Fasenra_DE'),]
fasenra_calls = fasenra_calls.loc[(fasenra_calls['ACTT_DESC'] != 'VAE sent'),]
fasenra_calls['ACTT_DESC'].value_counts()
fasenra_calls[['FIRST_DETAIL','SECOND_DETAIL','THIRD_DETAIL','FOURTH_DETAIL','FIFTH_DETAIL']] = fasenra_calls[['FIRST_DETAIL','SECOND_DETAIL','THIRD_DETAIL','FOURTH_DETAIL','FIFTH_DETAIL']].replace(' ','_', regex = True)
fasenra_calls['FIRST_DETAIL'].value_counts()

def call_trans(df,detail1, detail2, detail3, detail4, detail5):
    df1 = df.replace({'BREAST_CANCER_(Durvalumab)':'NV', 'No_Product_DE':'NV', 'Xigduo_DE':'Forxiga_DE'})
    df1.loc[df1['FIRST_DETAIL'] == 'Forxiga_DE','Flag1'] = 1
    df1['Flag1'].fillna(0,inplace = True)

    df1['sum_flags'] = df1[[col for col in df1.columns if col.startswith('Flag')]].sum(axis = 1)
    df1['Flag2'] = np.where(df1['SECOND_DETAIL']=='Forxiga_DE', np.where(df1['sum_flags']>=1,df1['sum_flags']+1 ,1) ,0)
    df1['sum_flags'] = df1[[col for col in df1.columns if col.startswith('Flag')]].sum(axis = 1)
    df1['Flag3'] = np.where(df1['THIRD_DETAIL']=='Forxiga_DE', np.where(df1['sum_flags']>=1,df1['sum_flags']+1 ,1) ,0)
    df1['sum_flags'] = df1[[col for col in df1.columns if col.startswith('Flag')]].sum(axis = 1)
    df1['Flag4'] = np.where(df1['FOURTH_DETAIL']=='Forxiga_DE', np.where(df1['sum_flags']>=1,df1['sum_flags']+1 ,1) ,0) 
    df1['sum_flags'] = df1[[col for col in df1.columns if col.startswith('Flag')]].sum(axis = 1)
    df1['Flag5'] = np.where(df1['FIFTH_DETAIL']=='Forxiga_DE', np.where(df1['sum_flags']>=1,df1['sum_flags']+1 ,1) ,0)
    
    df1['FIRST_DETAIL1'] = np.where(df1['FIRST_DETAIL'] == 'Forxiga_DE', np.where(df1['Flag1']==1,df1['FIRST_DETAIL'],'NV') ,df1['FIRST_DETAIL'])
    df1['SECOND_DETAIL1'] = np.where(df1['SECOND_DETAIL'] == 'Forxiga_DE', np.where(df1['Flag2']==1,df1['SECOND_DETAIL'],'NV') ,df1['SECOND_DETAIL'])
    df1['THIRD_DETAIL1'] = np.where(df1['THIRD_DETAIL'] == 'Forxiga_DE', np.where(df1['Flag3']==1,df1['THIRD_DETAIL'],'NV') ,df1['THIRD_DETAIL'])
    df1['FOURTH_DETAIL1'] = np.where(df1['FOURTH_DETAIL'] == 'Forxiga_DE', np.where(df1['Flag4']==1,df1['FOURTH_DETAIL'],'NV') ,df1['FOURTH_DETAIL'])
    df1['FIFTH_DETAIL1'] = np.where(df1['FIFTH_DETAIL'] == 'Forxiga_DE', np.where(df1['Flag5']==1,df1['FIFTH_DETAIL'],'NV') ,df1['FIFTH_DETAIL'])
    
    df1['COMB_Detail'] = df1['FIRST_DETAIL1'] +str('*') + df1['SECOND_DETAIL1'] + str('*') + df1['THIRD_DETAIL1'] +str('*') + df1['FOURTH_DETAIL1'] + str('*') + df1['FIFTH_DETAIL1']
    df1['COMB_Detail'] = [x.replace('NV','') for x in df1['COMB_Detail']]
    df1['COMB_Detail'] = [x.replace('*',' ') for x in df1['COMB_Detail']]
    df1['COMB_Detail'] = [x.replace('    ',' ') for x in df1['COMB_Detail']]
    df1['COMB_Detail'] = [x.replace('   ',' ') for x in df1['COMB_Detail']]
    df1['COMB_Detail'] = [x.replace('  ',' ') for x in df1['COMB_Detail']]
    df1['COMB_Detail1'] = [x.lstrip() for x in df1['COMB_Detail']]

    df1[['First','Second','Third','Fourth','Fifth']] = df1['COMB_Detail1'].str.split(' ',expand = True)

    return df1

fasenra_calls1 = call_trans(fasenra_calls,'FIRST_DETAIL','SECOND_DETAIL','THIRD_DETAIL','FOURTH_DETAIL','FIFTH_DETAIL')

###############################################################################
# Filtering on Activity Type, Sales Unit
fasenra_calls1['ACTT_DESC'].value_counts()
fasenra_calls1['SALES_UNIT'].value_counts()
fasenra_calls1.loc[fasenra_calls1['YEAR']==2020,'ACTT_DESC'].value_counts()
cat_to_drop = ['AZ FF Service Contact','AZ FF Hospitation']

calls_type = fasenra_calls1.loc[~fasenra_calls1['ACTT_DESC'].isin(cat_to_drop),'ACTT_DESC'].unique().tolist()
teams = ['AZ_RESPI_BX','AZ_RESPI_INH']
start = 201901
end = 202112
call5 = fasenra_calls1.loc[fasenra_calls1['ACTT_DESC'].isin(calls_type) &
                          fasenra_calls1['SALES_UNIT'].isin(teams) &
                          (fasenra_calls1['YRMO']>=start) & (fasenra_calls1['YRMO']<=end),]

call5[['First','Second','Third','Fourth','Fifth']] = call5[['First','Second','Third','Fourth','Fifth']].replace({'':'NV'}) 
call5.columns
colClean(call5)
call5.columns
call5['ACTT_DESC'].value_counts()
call5['SALES_UNIT'].value_counts()
call5['FIRST'].value_counts()
call5['SECOND'].value_counts()
call5['THIRD'].value_counts()
call5['FOURTH'].value_counts()

# Defining function to get Calls & PDEs for each team
# Creating a temp df @ Brick*YearMonth granularity (will use at the end)
temp = call4[['BRICK','YRMO']].drop_duplicates(subset = ['BRICK','YRMO'],keep = 'first').sort_values(['BRICK','YRMO'])
temp1 = temp.loc[(temp['YRMO'] >=201901) & (temp['YRMO']<=202105),]

def call_pde2(df, detail_1, detail_2, detail_3, act_col, b_name, g_by_col):
    df_final = pd.DataFrame(columns = ['BRICK','YRMO'])
    for team in teams:
        for call in calls_type:
            df_temp = df.loc[(df['ACTT_DESC']== call) & (df['SALES_UNIT'] == team),]
            df1 = df_temp.loc[(df_temp[detail_1]==b_name) & (df_temp[detail_2]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
            df2 = df_temp.loc[(df_temp[detail_1]==b_name) & (df_temp[detail_2]!='NV') & (df_temp[detail_3]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
            df3 = df_temp.loc[(df_temp[detail_1]==b_name) & (df_temp[detail_2]!='NV') & (df_temp[detail_3]!='NV'),].groupby(g_by_col)[act_col].count().reset_index()
            df4 = df_temp.loc[(df_temp[detail_1]!=b_name) & (df_temp[detail_2]==b_name) & (df_temp[detail_3]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
            df5 = df_temp.loc[(df_temp[detail_1]!=b_name) & (df_temp[detail_2]==b_name) & (df_temp[detail_3]!='NV'),].groupby(g_by_col)[act_col].count().reset_index()
            df6 = df_temp.loc[(df_temp[detail_1]!=b_name) & (df_temp[detail_2]!='NV') & (df_temp[detail_3]==b_name),].groupby(g_by_col)[act_col].count().reset_index()
            dfs = [temp1,df1,df2,df3,df4,df5,df6]
            temp_df = reduce(lambda  left,right: pd.merge(left,right,on=g_by_col,
                                                          how='outer'), dfs)
            temp_df = temp_df.fillna(0)
            temp_df.columns = ['BRICK','YRMO','A', 'A_B', 'A_B_C', 'B_A', 'B_A_C', 'B_C_A']
            temp_df.columns = temp_df.columns[:2].append(str(team)+ '_'+ str(call)+ '_' + temp_df.columns[2:])
            temp_df = temp_df.fillna(0)
            temp_df['TOTAL_CALLS' + '_' + str(team) + '_' + str(call)] = temp_df.drop(['BRICK','YRMO'],axis = 1).sum(axis = 1)
            temp_df = temp_df.fillna(0)
            temp_df['TOTAL_PDE'+ '_' +str(team)+ '_' + str(call)] = temp_df[str(team)+ '_' + str(call)+'_'+'A'] + 0.7*temp_df[str(team)+ '_' + str(call)+'_'+'A_B'] + 0.6*temp_df[str(team)+'_' + str(call)+'_'+'A_B_C'] + 0.3*temp_df[str(team)+'_' + str(call)+'_'+'B_A'] + 0.3*temp_df[str(team)+'_' + str(call)+'_'+'B_A_C'] + 0.1*temp_df[str(team)+'_' + str(call)+'_'+'B_C_A']
            df_final = pd.merge(df_final,temp_df, how = 'outer', on = g_by_col)
            cols_to_drop = [col for col in df_final.columns if col.endswith('_A') | col.endswith('_B') | col.endswith('_C')]
            df_final = df_final.drop(cols_to_drop, axis = 1)
            df_final = df_final.fillna(0)
    return df_final

call_df1 = call_pde2(df = call5, detail_1 = 'FIRST', detail_2='SECOND',
                 detail_3='THIRD', act_col='FACT_ACT_ID',b_name='Fasenra_DE',
                 g_by_col=['BRICK','YRMO']).fillna(0)

call_df2 = call_df1.loc[:,(call_df1.sum(axis = 0)!=0)]
call_df2.info()
call_df2.columns = call_df2.columns.str.replace(' ', '_')

call_df2.loc[(call_df2['YRMO']>=202001) & (call_df2['YRMO']<=202012),].sum()
call_df2.loc[(call_df2['YRMO']>=202001) & (call_df2['YRMO']<=202012),].sum()[2:].sum()

###############################################################################
# HCP Count
hcp_type = ['Allgemeinmedizin','Innere Medizin', 'Praktischer Arzt']
hcp_detail.columns
hcp_count = pd.merge(hcp_detail,mapping_df1[['TERRITORYIDENTIFIER', 'PARENTTERRITORYIDENTIFIER']], how = 'inner', left_on = 'DCUS_CELL_ID', right_on = 'TERRITORYIDENTIFIER').rename(columns={'PARENTTERRITORYIDENTIFIER':'BRICK'})
hcp_count = hcp_count.loc[(hcp_count['DCUS_PRIM_SPEC_NAME'].isin(hcp_type)),]
hcp_count1 = hcp_count.groupby(['BRICK'])['DCUS_ID'].agg(lambda x: len(x.unique())).reset_index()
hcp_count1.sum()
hcp_count1['BRICK'] = hcp_count1['BRICK'].astype('int')

###############################################################################
# Final Calls Data
call_df3 = pd.merge(call_df2,hcp_count1, how = 'left',left_on = ['BRICK'], right_on = ['BRICK']).rename(columns = {'DCUS_ID':'HCP_COUNT'})
call_df3.to_excel(str(processed_path)+'/Calls_Consolidated_HJ_15072021.xlsx', index = False)



'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'
'###############################################################################'
'############################## VAE/EMAILS #####################################'
'###############################################################################'
'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'

# Reading Input data
email_open = pd.read_csv(str(input_path)+'/Email_Raw_HJ.csv')
email_sent = pd.read_csv(str(input_path)+'/VAE Sent Data.csv')
hcp_detail = pd.read_excel(str(input_path)+'/01_HCP_List_and_Details_HCP.xlsx', sheet_name='Sheet1')
mapping_df = pd.read_csv(str(input_path)+"/05_Country_Territory_Cut.csv")

colClean(email_open)
colClean(email_sent)
colClean(hcp_detail)
colClean(mapping_df)

###############################################################################
# Getting Brick info using mapping file
mapping_df.info()
mapping_df1 = mapping_df.loc[mapping_df['TERRITORYLEVELGLOBAL']=='Minibrick',['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']].drop_duplicates()
mapping_df1['PARENTTERRITORYIDENTIFIER'].nunique()
mapping_df1['TERRITORYIDENTIFIER'] = mapping_df1['TERRITORYIDENTIFIER'].astype(int)
mapping_df1 = mapping_df1.rename(columns = {'PARENTTERRITORYIDENTIFIER':'BRICK'})
mapping_df1.info()

###############################################################################
# Merging all these files
email_open1 = pd.merge(email_open, hcp_detail[['DCUS_ID','DCUS_CELL_ID','DCUS_CUST_ID','DORG_TYPE_NAME', 
                              'DCUS_PRIM_SPEC_NAME']], how = 'left', 
               left_on = 'FACT_DCUS_ID', right_on = 'DCUS_ID')

email_open2 = pd.merge(email_open1,mapping_df1, how = 'left',left_on = 'DCUS_CELL_ID', right_on = 'TERRITORYIDENTIFIER')
email_open2.info()
email_open2 = email_open2.fillna('')

###############################################################################
# Transorming Date Columns
date_col = 'FACT_DATE'
email_open3 = date_trans(email_open2, col = date_col, form='%d.%m.%Y')

###############################################################################
# Filtering Email for Fasenra only
brand = 'Fasenra_DE'
email_open4 = email_open3.loc[email_open3['FIRST_DETAIL']==brand,]

email_open4 = email_open4.pivot_table(index = ['BRICK','YRMO'], values = ['FACT_DCUS_ID'],
                      columns = ['ACTT_DESC'], aggfunc = 'count').reset_index().fillna(0)
email_open4.sum()

# Filtering for time period
start = 201901
end = 202112
email_open5 = email_open4.loc[(email_open4['YRMO']>= start) & (email_open4['YRMO']<=end),]
email_open5.sum()

email_open5.columns = [f'{col[1]}_{col[0]}' for col in email_open5.columns]
colClean(email_open5)
email_open5.rename(columns = {'_BRICK':'BRICK','_YRMO':'YRMO',
                      'VAECLICKED_FACT_DCUS_ID':'VAE_CLICKED',
                      'VAEDOCUMENTLOAD_FACT_DCUS_ID':'VAE_DOCUMENT_LOAD',
                      'VAEDOCUMENTREAD_FACT_DCUS_ID':'VAE_DOCUMENT_READ',
                      'VAEOPENED_FACT_DCUS_ID':'VAE_OPENED'}, inplace = True)
email_open5.columns


'#############################################################################'
'################################# Email Sent ################################'
'#############################################################################'
# Merging all these files
sent1 = pd.merge(email_sent, hcp_detail[['DCUS_ID','DCUS_CELL_ID','DCUS_CUST_ID','DORG_TYPE_NAME', 
                              'DCUS_PRIM_SPEC_NAME']], how = 'left', 
               left_on = 'FACT_DCUS_ID', right_on = 'DCUS_ID')

sent2 = pd.merge(sent1,mapping_df1, how = 'left',left_on = 'DCUS_CELL_ID', right_on = 'TERRITORYIDENTIFIER')
sent2.info()
sent2 = sent2.fillna('')

###############################################################################
# Transorming Date Columns
date_col = 'FACT_DATE'

sent3 = date_trans(sent2, col = date_col, form='%d.%m.%Y')
sent3 = sent3.sort_values(['BRICK','YRMO'])
sent3.YRMO.unique()

###############################################################################
# Filtering Email for Fasenra only
brand = 'Fasenra_DE'
sent4 = sent3.loc[sent3['FIRST_DETAIL']==brand,]
sent5 = sent4.pivot_table(index = ['BRICK','YRMO'], values = ['FACT_DCUS_ID'],
                      columns = ['ACTT_DESC'], aggfunc = 'count').reset_index().fillna(0)
sent5.sum()

# Filtering for time period
start = 201901
end = 202112
sent6 = sent5.loc[(sent5['YRMO']>= start) & (sent5['YRMO']<=end),]

sent6.columns = [f'{col[1]}_{col[0]}' for col in sent6.columns]
colClean(sent6)
sent6.rename(columns = {'_BRICK':'BRICK','_YRMO':'YRMO','VAESENT_FACT_DCUS_ID':'VAE_SENT'}, inplace = True)
sent6.columns
sent6.sum()

###############################################################################
# Merging both the dataframes (Email Opened, Email Sent)
email_final = pd.merge(email_open5,sent6, how = 'outer', left_on = ['BRICK','YRMO'], right_on = ['BRICK','YRMO']).fillna(0)

email_final.to_excel(str(processed_path)+'/EMAIL_CONSOLIDATED.xlsx',index = False)


'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'
'###############################################################################'
'###################### Consolidating all the datesets #########################'
'###############################################################################'
'-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-'

# Reading Input data
sales = pd.read_excel(str(processed_path)+'/Fasenra_Sales_Data.xlsx', sheet_name = 'Sheet1')
sales = colClean(sales)
call = pd.read_excel(str(processed_path)+'/Calls_Consolidated_HJ_15072021.xlsx', sheet_name = 'Sheet1')
call = colClean(call)
meeting = pd.read_excel(str(processed_path)+'/Meetings_Final.xlsx')
meeting = colClean(meeting)
email = pd.read_excel(str(processed_path)+'/EMAIL_CONSOLIDATED.xlsx')
email = colClean(email)
brick_sub_terr_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'brick to sub terr')
colClean(brick_sub_terr_mapp)
sub_terr_area_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'sub terr to area')
colClean(sub_terr_area_mapp)
area_province_mapp = pd.read_excel(str(input_path)+"/Geography mappings.xlsx", sheet_name = 'final')
colClean(area_province_mapp)

dfs = [sales, call,meeting, email]
master = reduce(lambda  left,right: pd.merge(left,right,on=['BRICK','YRMO'],
                                            how='outer'), dfs).fillna(0)

master = master.loc[master['YRMO'] >=201901,]
master.info()
master.loc[(master['YRMO']>=202001) & (master['YRMO']<=202012),].sum().reset_index()


################################################################################################
'##############################################################################################'
################################################################################################
# Getting Province Level Information
brick_sub_terr_mapp.columns
master1 = pd.merge(master, brick_sub_terr_mapp[['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']],
                   left_on = ['BRICK'], right_on = ['TERRITORYIDENTIFIER'], how = 'left').rename(columns = {'PARENTTERRITORYIDENTIFIER':'SUB_TERRITORY'})

master2 = pd.merge(master1, sub_terr_area_mapp[['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']],
                   left_on = ['SUB_TERRITORY'], right_on = ['TERRITORYIDENTIFIER'], how = 'left').rename(columns = {'PARENTTERRITORYIDENTIFIER':'OLD_PROVINCE'})

master3 = pd.merge(master2, area_province_mapp[['PARENTTERRITORYIDENTIFIER','NEWREGION']],
                   left_on = ['OLD_PROVINCE'], right_on = ['PARENTTERRITORYIDENTIFIER'], how = 'left').rename(columns = {'NEWREGION':'NEW_PROVINCE'})

master3.info()
master3.drop(['TERRITORYIDENTIFIER_x','TERRITORYIDENTIFIER_y',
              'PARENTTERRITORYIDENTIFIER'],axis = 1, inplace = True)

###############################################################################
# Applying Omni Channel scores on Call Activty type
def pde_weight(df, call_decs1, call_desc2, call_desc3, w1, w2, w3):
    for i in range(len(call_desc1)):
        df[call_desc1[i]+str('_WEIGHTED')] = df[call_desc1[i]] * w1
    for j in range(len(call_desc2)):
        df[call_desc2[j]+str('_WEIGHTED')] = df[call_desc2[j]] * w2
    for k in range(len(call_desc3)):
        df[call_desc3[k]+str('_WEIGHTED')] = df[call_desc3[k]] * w3
    return df

call_desc1 = ['TOTAL_PDE_AZ_RESPI_BX_AZ_FF_TRADITIONAL_CALL','TOTAL_PDE_AZ_RESPI_BX_AZ_FF_CONGRESS_CONTACT',
              'TOTAL_PDE_AZ_RESPI_BX_AZ_FF_WORKING_DINNER', 'TOTAL_PDE_AZ_RESPI_BX_AZ_FF_VIDEO_CALL',
              'TOTAL_PDE_AZ_RESPI_BX_AZ_FF_VEEVA_ENGAGE','TOTAL_PDE_AZ_RESPI_INH_AZ_FF_TRADITIONAL_CALL',
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_CONGRESS_CONTACT', 'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_WORKING_DINNER',
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_VIDEO_CALL', 'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_VEEVA_ENGAGE']
call_desc2 = ['TOTAL_PDE_AZ_RESPI_BX_AZ_FF_TELEPHONE_CONTACT', 'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_TELEPHONE_CONTACT']
call_desc3 = ['TOTAL_PDE_AZ_RESPI_BX_AZ_FF_EMAIL_CONTACT','TOTAL_PDE_AZ_RESPI_BX_AZ_FF_LETTER',
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_EMAIL_CONTACT','TOTAL_PDE_AZ_RESPI_INH_AZ_FF_LETTER']
w1 = 1
w2 = 0.5
w3 = 0.25

master3.loc[(master3['YRMO'] >=202001) & (master3['YRMO'] <=202012),'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_EMAIL_CONTACT'].sum()

master3 = pde_weight(master3, call_desc1, call_desc2, call_desc3, w1, w2, w3)
master3 = master3.loc[:,(master3.sum(axis = 0)!=0)]
master4 = master3.copy()

master4['F2F_PDE'] = master4[['TOTAL_PDE_AZ_RESPI_BX_AZ_FF_TRADITIONAL_CALL_WEIGHTED','TOTAL_PDE_AZ_RESPI_BX_AZ_FF_CONGRESS_CONTACT_WEIGHTED',
              'TOTAL_PDE_AZ_RESPI_BX_AZ_FF_WORKING_DINNER_WEIGHTED', 'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_TRADITIONAL_CALL_WEIGHTED',
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_CONGRESS_CONTACT_WEIGHTED', 'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_WORKING_DINNER_WEIGHTED']].sum(axis = 1)

master4['REMOTE_PDE'] = master4[['TOTAL_PDE_AZ_RESPI_BX_AZ_FF_VIDEO_CALL_WEIGHTED',
              'TOTAL_PDE_AZ_RESPI_BX_AZ_FF_VEEVA_ENGAGE_WEIGHTED','TOTAL_PDE_AZ_RESPI_INH_AZ_FF_VIDEO_CALL_WEIGHTED', 
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_VEEVA_ENGAGE_WEIGHTED','TOTAL_PDE_AZ_RESPI_BX_AZ_FF_TELEPHONE_CONTACT_WEIGHTED',
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_TELEPHONE_CONTACT_WEIGHTED','TOTAL_PDE_AZ_RESPI_BX_AZ_FF_EMAIL_CONTACT_WEIGHTED',
              'TOTAL_PDE_AZ_RESPI_BX_AZ_FF_LETTER_WEIGHTED',
              'TOTAL_PDE_AZ_RESPI_INH_AZ_FF_EMAIL_CONTACT_WEIGHTED','TOTAL_PDE_AZ_RESPI_INH_AZ_FF_LETTER_WEIGHTED']].sum(axis = 1)

master4['F2F_PDE'].sum()
master4['REMOTE_PDE'].sum()
master4['BX_INH_PDEs'] = master4['F2F_PDE'] + master4['REMOTE_PDE']


################################################################################################
# Merging Covid Indicators
covid_data = pd.read_excel(str(input_path)+"/Final_province_month_covid_2021.07.16.xlsx", sheet_name = 'Final Data')
covid_data.dtypes
master4.dtypes
master5 = pd.merge(master4, covid_data, how = 'left', left_on = ['YRMO','NEW_PROVINCE'], right_on = ['Yrmo','Final_Region'])
master5.fillna(0, inplace = True)
master5.columns
required_cols = ['FASENRA_DDD','BX_INH_PDEs','ATTENDEES']

################################################################################################
# HCP Count Normalization
master5['FASENRA_DDD_hcp_cnt_norm'] = master5['FASENRA_DDD']/master5['HCP_COUNT']
master5['BX_INH_PDEs_hcp_cnt_norm'] = master5['BX_INH_PDEs']/master5['HCP_COUNT']
master5['ATTENDEES_hcp_cnt_norm'] = master5['ATTENDEES']/master5['HCP_COUNT']

master5['norm'] = 1
master5.rename(columns = {'YRMO':'yrmo', 'COMP_GROWTH':'comp_growth_var'}, inplace = True)
master5.drop(['Final_Region','Yrmo'], axis = 1, inplace = True)
master5.to_excel(str(master_data_path) + '/Master_Data_1507.xlsx', index = False)

################################################################################################
# Master Data - Relevant columns only
master5.columns
required_cols = ['NEW_PROVINCE','BRICK', 'yrmo','FASENRA_DDD','MARKET_DDD_SALES',
       'COMP_DDD_SALES', 'comp_growth_var','HCP_COUNT', 'ATTENDEES',
       'EVENTS', 'VAE_OPENED', 'VAE_SENT', 'BX_INH_PDEs', 
       'retail_and_recreation_percent_change_from_baseline',
       'grocery_and_pharmacy_percent_change_from_baseline',
       'workplaces_percent_change_from_baseline',
       'residential_percent_change_from_baseline', 'Unemployment_rate',
       'Driving', 'Transit',
       'Walking', 'Cases_per_cap']

master6 = master5[required_cols]
master6.to_excel(str(master_data_path) + '/Fasenra_Germany_master_data_actuals.xlsx', index = False)

################################################################################################
'##############################################################################################'
################################################################################################















