# Mix models

data = master_2[which(master_2$yrmo>= start & master_2$yrmo <= end),]
f_e_formula = FASENRA_DDD_hcp_cnt_norm~FASENRA_DDD_hcp_cnt_norm_1+FASENRA_DDD_hcp_cnt_norm_2+BX_INH_PDEs_exp+ATTENDEES_exp+Cases_per_cap+comp_growth_var
# f_e_formula = FASENRA_DDD_hcp_cnt_norm~FASENRA_DDD_hcp_cnt_norm_1+FASENRA_DDD_hcp_cnt_norm_2+BX_INH_PDEs_exp+ATTENDEES_exp+pde_vae_it_norm_exp+Cases_per_cap+comp_growth_var
r_e_terms = c("BX_INH_PDEs_exp")
# r_e_terms = c("BX_INH_PDEs_exp","ATTENDEES_exp","vae_opened_exp")
r_e_factor = "dma"
r_e_formula = NULL
get_relative_change = FALSE
min_distinct_obs = 3

if(any(data %>% select(all.vars(f_e_formula)) %>% summarize_all(sum) == 0)){
  
  return(list(NULL))
  
}

if(is.null(r_e_formula)){
  r_e_formula <- paste0(f_e_formula, paste0(" + (1 + ", paste0(r_e_terms, collapse = " + "), " | r_e_id_internal)"))
}

numeric_ids <- data %>% 
  select(r_e_factor) %>% 
  distinct %>% 
  rowid_to_column("r_e_id_internal") 

data <- suppressWarnings(data %>%
                           inner_join(numeric_ids, by = r_e_factor) %>%
                           group_by(!!sym(r_e_factor)) %>%
                           mutate(y_value_count = distinct_value_count(!!sym(lhs.vars(f_e_formula))),
                                  y_value_nonzero_count = nonzero_value_count(!!sym(lhs.vars(f_e_formula)))) %>%
                           ungroup %>%
                           mutate(r_e_id_internal = r_e_id_internal + 1,
                                  r_e_id_internal = ifelse(y_value_count < min_distinct_obs, 1, r_e_id_internal),
                                  r_e_id_internal = ifelse(r_e_id_internal == 1 & (y_value_nonzero_count < min_distinct_obs), 0, r_e_id_internal)) %>%
                           select(-y_value_count, -y_value_nonzero_count))

original_id_mapping <- data %>% select(r_e_factor, r_e_id_internal) %>% distinct

# Handle cases where no observations have enough distinct values
if(length(unique(data$r_e_id_internal)) == 1){
  return(NULL)
}

fit <- suppressMessages(suppressWarnings(lme4::lmer(r_e_formula,
                                                    data = data,
                                                    control = lmerControl(optimizer = "optimx", 
                                                                          optCtrl=list(method="nlminb", starttests = FALSE)))))

if(any(is.na(fixef(fit)))){ return(list(NULL)) }

combined <- coef(fit)[[1]] %>% 
  rename(intercept = `(Intercept)`) %>%
  mutate(r_e_id_internal := as.numeric(row.names(.))) %>%
  inner_join(original_id_mapping, by = "r_e_id_internal") %>%
  select(r_e_factor, everything(), -r_e_id_internal)

fixed <- enframe(fixef(fit)) %>% 
  filter(value > 0 & (name %in% r_e_terms)) %>%
  pivot_wider() 

# write.csv(combined,"combined_vf.csv",row.names = F)
# write.csv(combined,"combined_pde_meeting_mixed.csv",row.names = F)

fixef(fit)

output

output_fixef = output
output_fixef$Estimate = fixef(fit)
output_fixef

output = output_fixef

fin_out = impact_calc(mth_start,mth_end,promotions)

a  = cbind(fin_out$impact_percent,percent(fin_out$impact_percent$percent))
colnames(a) = c(colnames(fin_out$impact_percent),"%")
a
a[,c(1,2,4)]
percent(fin_out$non_normalized_error)

((a[[3]][3] + a[[3]][5]) - 0.3265)*100
((a[[3]][1] + a[[3]][2]) - 0.6362)*100

(a[[3]][1] + a[[3]][2])

s1 = fin_out$output_monthly

trend = s1[,c("yrmo","total_predicted","total_actual")]

trend$total_predicted = unlist(trend$total_predicted)

# ggplot(trend, aes(yrmo)) + 
#   geom_line(aes(y = total_predicted, colour = "Predicted")) + 
#   geom_line(aes(y = total_actual, colour = "Actual")) +
#   expand_limits(y = 0)

output[,c("rn","Estimate")]
a
