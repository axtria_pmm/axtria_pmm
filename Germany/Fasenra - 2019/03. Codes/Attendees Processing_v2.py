import pandas as pd
import numpy as np
import os
import pandasql as ps

raw_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Raw Data/05_HCP_Promotional_Activity'
processed_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Processed/Meetings'
terr_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Raw Data/05_Country_Territory_Cut'
hcp_detail_path = 'C:/Users/knss680/Box Sync/Fasenra - PMM 2020/Raw Data/01_HCP_List_and_Details'
os.chdir(processed_path)

# Rename Columns
def rm(df):
    df.columns = df.columns.str.upper()
    df.columns = df.columns.str.replace(' ', '')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('+', '_')
    return df

# Reading Input data
Attendees = pd.read_csv(str(raw_path)+'/05_HCP_Promotional_Activity_EVENT_ATTENDEE.csv', delimiter = ';')
event = pd.read_csv(str(raw_path)+'/05_HCP_Promotional_Activity_EVENT.csv',delimiter=';')
hcp_detail = pd.read_excel(str(hcp_detail_path)+'/01_HCP_List_and_Details_HCP.xlsx', sheet_name='Sheet1')
mapping_df = pd.read_csv(str(terr_path)+"/05_Country_Territory_Cut.csv")

###############################################################################
###############################################################################
# Getting Brick info using mapping file
mapping_df.info()
mapping_df1 = mapping_df.loc[mapping_df['TERRITORYLEVELGLOBAL']=='Minibrick',['TERRITORYIDENTIFIER','PARENTTERRITORYIDENTIFIER']].drop_duplicates()
mapping_df1['PARENTTERRITORYIDENTIFIER'].nunique()
mapping_df1['TERRITORYIDENTIFIER'] = mapping_df1['TERRITORYIDENTIFIER'].astype(int)
mapping_df1 = mapping_df1.rename(columns = {'PARENTTERRITORYIDENTIFIER':'Brick_ID'})
mapping_df1.info()

df = pd.merge(Attendees, event[['EVT_ID','EVTT_NAME','EVTF_NAME','SOURCE_SYS',
                                'PRD_NAME']], how = 'left', left_on = 'XEVAT_EVT_ID', right_on = 'EVT_ID').drop_duplicates()

df1 = pd.merge(df,hcp_detail[['DCUS_ID','DCUS_CELL_ID','DCUS_CUST_ID','DORG_TYPE_NAME', 
                              'DCUS_PRIM_SPEC_NAME']], how = 'left', 
               left_on = 'XEVAT_CUS_ID', right_on = 'DCUS_ID').drop_duplicates()

df2 = pd.merge(df1,mapping_df1, how = 'left',left_on = 'DCUS_CELL_ID', right_on = 'TERRITORYIDENTIFIER').drop_duplicates()
df2.info()

# Checking Date Column and creating new DateTime variables
df2['EVT_START_DATE'].head()
date_col = 'EVT_START_DATE'
df2 = df2.fillna("")

def date_trans(df, col, form):
    df[col] = pd.to_datetime(df[col], format = form)
    df['Year'] = df[col].dt.year
    df['Month'] = df[col].dt.month
    df['yrmo'] = df[col].dt.strftime('%Y%m').astype('int')
    return df

#Attendees['EVT_START_DATE']= pd.to_datetime(Attendees['EVT_START_DATE'],format='%d.%m.%Y')
df3 = date_trans(df2, col = date_col, form='%d.%m.%Y')
df3 = df3.sort_values(['Brick_ID','yrmo'])
df3.yrmo.unique()

# Data Preparation - Long to Wide Format
df3.info()
df3.isnull().sum()
df3[['yrmo','Brick_ID']].nunique()

df4= df3.loc[df3['XEVAT_CUS_ID']!=-1].pivot_table(index = ['Brick_ID','yrmo'],
                         values = ['XEVAT_CUS_ID'], aggfunc = 'count').reset_index().rename(columns = {'Brick_ID':'BRICK'})

# q1 = """SELECT Brick_ID,yrmo, count(XEVAT_CUS_ID) as Attendees FROM Attendees1 where XEVAT_CUS_ID != -1 and Brick_ID ne null group by  Brick_ID,yrmo """
# Attendees3b = (ps.sqldf(q1, locals()))

df_yrmo = pd.DataFrame()
df_Brick = pd.DataFrame()

df_yrmo['yrmo'] = pd.unique(df4['yrmo'].sort_values())
df_Brick['BRICK'] = pd.unique(mapping_df1['Brick_ID'].sort_values())

df_yrmo['key'] = 1
df_Brick['key'] = 1

Yrmo_Brick = pd.merge(df_Brick,df_yrmo, on ='key').drop("key", 1)

# Final Data
Attendees_final = (pd.merge(Yrmo_Brick, df3, how = 'left', left_on = ['BRICK','yrmo'], right_on = ['Brick_ID','yrmo'])).rename(columns = {'XEVAT_CUS_ID' :'Attendees'})
Attendees_final['Attendees'] = Attendees_final['Attendees'].fillna(0)
Attendees_final.dropna()


start_y1 = 201801   
start_y2 = 201901
end_y1 = 201812
end_y2 = 201912
os.chdir(processed_path)

Attendees_final[(Attendees_final.yrmo>= start_y1) & (Attendees_final.yrmo <= end_y2)].reset_index(drop = True).to_csv('processed_attendees_data_' + '2018-19' + '.csv')  
Attendees_final[(Attendees_final.yrmo>= start_y2) & (Attendees_final.yrmo <= end_y2)].reset_index(drop = True).to_csv('processed_attendees_data_' + '2019' + '.csv')   

























