import pandas as pd
import numpy as np
import os
from datetime import date

import pandasql as ps

raw_path = "C:\\Users\\kmdc693\\OneDrive - AZCollaboration\\AZ\\Global MMx Project\\Reference\\Faserna\\DATA\\Raw"
processed_path ="C:\\Users\\kmdc693\\OneDrive - AZCollaboration\\AZ\\Global MMx Project\\Reference\\Faserna\\DATA\\Processed"

Filename = {"Attendees" : "Fasenra Attendees_V1_922.xlsx", "Sales" : "Asthma market sales.xlsx", 'Mapping' : 'Minibrick_Territory_Mapping.xlsx', 'Emails' : 'Email Sent Opened.xlsx' , 'Calls':'Copy of 05_HCP_Promotional_Activity_CALL.xlsx'}
Sheetname = {"Attendees" : "Final Sheet- Event n Attendee", "Sales" : "Raw", 'Mapping' : 'Sheet1' , 'Emails': 'Email Sent', 'Calls' :'05_HCP_Promotional_Activity_CAL'}

start_y1 = 201801   
start_y2 = 201901
end_y1 = 201812
end_y2 = 201912

os.chdir(raw_path)

# Rename Columns
def rm(df):
    df.columns = df.columns.str.replace(' ', '')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('+', '_')
    return df

# Reading Input data
Emails0 = pd.read_excel(Filename['Emails'],sheet_name= Sheetname['Emails'],usecols =range(16))
mapping_file0 = pd.read_excel(Filename['Mapping'],sheet_name= Sheetname['Mapping'])
Calls0 = pd.read_excel(Filename['Calls'], sheet_name=Sheetname['Calls'])

Emails1= Emails0.rename(columns = {'FACT_DCELL_ID_1' : 'Territory_identifier'}).fillna(0)
mapping_file0= mapping_file0.rename(columns = {'Brick' : 'Brick_ID'})
Calls1= Calls0.rename(columns = {'FACT_DCELL_ID_1' : 'Territory_identifier'}).fillna(0)

Emails1['Territory_identifier'].astype(int)
Calls1['Territory_identifier'].astype(int)

Emails2 = pd.merge(Emails1,mapping_file0[['Territory_identifier','Brick_ID']], how = 'left', on = ['Territory_identifier'])
Calls2 = pd.merge(Calls1,mapping_file0[['Territory_identifier','Brick_ID']], how = 'left', on = ['Territory_identifier'])

# Checking Date Column and creating new DateTime variables
Emails2['FACT_DATE'].unique()
date_col = 'FACT_DATE'

def date_trans(df, col,form):
    df[col] = pd.to_datetime(df[col], format = form)
    df['Year'] = df[col].dt.year
    df['Month'] = df[col].dt.month
    df['yrmo'] = df[col].dt.strftime('%Y%m').astype('int')
    return df

# Emails2['EVT_START_DATE']= pd.to_datetime(Emails2['EVT_START_DATE'],format='%d.%m.%Y')
Emails3 = date_trans(df = Emails2, col = date_col, form = '%d.%m.%Y')
Emails3.yrmo.unique()

Calls3 = date_trans(df = Calls2, col = date_col, form = '%d.%m.%Y')
Calls3.yrmo.unique()

Emails3 = Emails3[(Emails3.yrmo>= start_y2) & (Emails3.yrmo <= end_y2)]
Calls3= Calls3[(Calls3.yrmo>= start_y2) & (Calls3.yrmo <= end_y2)]

calls_type = ['AZ FF Traditional Call','AZ FF Congress Contact','AZ FF Working Dinner','AZ FF Hospitation']
teams = ['AZ_RESPI_BX','AZ_RESPI_INH']
Calls3 = Calls3.loc[Calls3['ACTT_DESC'].isin(calls_type) & Calls3['SALES_UNIT'].isin(teams),]

Emails_Calls = pd.merge(Emails3,Calls3[['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE']], how = 'left', on = ['FACT_DEMP_ID','FACT_DCUS_ID'],suffixes=('_Emails', '_Calls'))

# Data Preparation - Long to Wide Format
Emails3.info()
Emails3.isnull().sum()
Emails3.describe()
Emails3[['yrmo','Brick_ID']].nunique()

Emails_Calls.info()
Emails_Calls.isnull().sum()
Emails_Calls.describe()
Emails_Calls[['yrmo','Brick_ID']].nunique()

def numOfDays(date1, date2):
    return (date2-date1).dt.days

# Calculating the days difference and labelling whether the customer had call before email or after
Emails_Calls = Emails_Calls[['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails','FACT_DATE_Calls']].drop_duplicates()
Emails_Calls['Days_diff'] = numOfDays(Emails_Calls['FACT_DATE_Emails'], Emails_Calls['FACT_DATE_Calls'])

Emails_Calls['Before_Diff'] = Emails_Calls.loc[(Emails_Calls['Days_diff'] <= 0 ), ['Days_diff']]
Emails_CallsB0=Emails_Calls.sort_values(by = ['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails','Before_Diff'], ascending = False).reset_index(drop = True)
Emails_CallsB = Emails_CallsB0.groupby(['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails']).first().reset_index()

Emails_Calls['After_Diff'] = Emails_Calls.loc[(Emails_Calls['Days_diff'] >= 0 ), ['Days_diff']]
Emails_CallsA0=Emails_Calls.sort_values(by = ['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails','After_Diff'], ascending = True).reset_index(drop = True)
Emails_CallsA= Emails_CallsA0.groupby(['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails']).first().reset_index().drop(columns = 'Before_Diff')

Emails_Calls1= pd.merge(Emails_Calls[['FACT_DEMP_ID','FACT_DCUS_ID','FACT_DATE_Emails']],Emails_CallsB[['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails','Before_Diff']], how = 'left', on = ['FACT_DEMP_ID','FACT_DCUS_ID','FACT_DATE_Emails']).drop_duplicates()
Emails_Calls2= pd.merge(Emails_Calls1,Emails_CallsA[['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails','After_Diff']], how = 'left', on = ['FACT_DEMP_ID','FACT_DCUS_ID','FACT_DATE_Emails']).drop_duplicates()
Emails_Calls2['Abs_Before_Diff'] = Emails_Calls2.Before_Diff.abs()

Emails_Calls2['Final_Days'] = Emails_Calls2[['Abs_Before_Diff','After_Diff']].min(axis=1).fillna(10000)
Emails_Calls2['Final_Label'] = np.where(Emails_Calls2['Final_Days']== Emails_Calls2['After_Diff'], 'After', 'Before')

Emails_Calls2.info()
Emails_Calls2['Final_Days'].astype(int)
Emails_Calls2['Group'] = np.where(Emails_Calls2['Final_Days'] == 10000, 'No Calls',
                                 (np.where((Emails_Calls2['Final_Days'] >21) & (Emails_Calls2['Final_Label']=='Before'), 'Before 21 days', 
                                           (np.where((Emails_Calls2['Final_Days'] < 21) & (Emails_Calls2['Final_Days'] >=8) & (Emails_Calls2['Final_Label']=='Before'), '8 to 21 days before', 
                                                          (np.where((Emails_Calls2['Final_Days'] < 8) & (Emails_Calls2['Final_Days'] >0) & (Emails_Calls2['Final_Label']=='Before'), '0 to 7 days before',
                                                                    (np.where((Emails_Calls2['Final_Days'] ==0) , 'Same day',
                                                                              (np.where((Emails_Calls2['Final_Days'] > 0) & (Emails_Calls2['Final_Days'] <=7 ) & (Emails_Calls2['Final_Label']=='After'), '0 to 7 days After', 
                                                                                        (np.where((Emails_Calls2['Final_Days'] >= 8) & (Emails_Calls2['Final_Days'] <=21) & (Emails_Calls2['Final_Label']=='After'), '8 to 21 days After',
                                                                                             (np.where((Emails_Calls2['Final_Days'] >21) & (Emails_Calls2['Final_Label']=='After'), '21 days After',   'Others'  
                                                                                                  )))))))))))))))



Emails_f1= pd.merge(Emails3.rename(columns = {'FACT_DATE':'FACT_DATE_Emails'}),Emails_Calls2[['FACT_DEMP_ID','FACT_DCUS_ID', 'FACT_DATE_Emails','Final_Days','Final_Label','Group']], how = 'left', on = ['FACT_DEMP_ID','FACT_DCUS_ID','FACT_DATE_Emails'])
Emails_f1.pivot_table(index = ['Group'],values = ['FACT_DATE_Emails'], aggfunc = 'count').reset_index()
Emails_f1.pivot_table(index = ['Group'],values = ['FACT_DATE_Emails'], aggfunc = 'count').reset_index()


os.chdir(processed_path)
Emails_f1.to_csv('Emails_3_' + '2019' + '.csv')   























