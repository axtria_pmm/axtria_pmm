import pandas as pd
import numpy as np
import os
from functools import reduce
import matplotlib.pyplot as plt
import statsmodels.api as sm
import math
pd.set_option('display.float_format', lambda x: '%.2f' % x)

os.chdir("C:\\Users\\knss680\\Box Sync\\Fasenra\\Final Deliverables\\DATA\\Raw")

# Rename Columns
def rm(df):
    df.columns = df.columns.str.upper()
    df.columns = df.columns.str.replace(' ', '')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('+', '_')
    return df

# Reading Input data
sales = pd.read_csv('Sales_Data_Prepared.csv')
sales = rm(sales)
call = pd.read_csv('Calls_Data.csv')
call = rm(call)
#email = pd.read_csv() 
meeting = pd.read_excel('Fasenra Attendees_V1_922.xlsx', sheet_name = 'Final')
meeting = rm(meeting)
dfs = [sales, call, meeting]
master = reduce(lambda  left,right: pd.merge(left,right,on=['BRICK','YRMO'],
                                            how='left'), dfs).fillna(0)

master.sum()
master.isnull().sum()
temp = master.copy()
temp = temp.fillna(0)
temp = temp.sort_values(['BRICK','YRMO'])
temp.loc[(temp['YRMO']>=201901) & (temp['YRMO']<=201912),].sum()

##########################################################################################################
##########################################################################################################
# EDA
#plotiing independent variables with dependent variable
labels = temp["DDD_FASENRA"]
features = temp.drop(["DDD_FASENRA",'YRMO'], axis=1)

for x in features:
    plt.plot(labels, features[x], 'ro') 
    plt.title("Sales_volume vs " + x)
    plt.xlabel("sales_volume")
    plt.ylabel(x)
    plt.show()

#Data Distribultion 
for x in features:
    plt.hist(features[x], bins='auto')  # arguments are passed to np.histogram
    plt.title(x)
    plt.show()

# Creating Sales Lags
def sales_lag(df, var, g_by_col, n_lags = 2):
    df = df.sort_values(by = ['BRICK','YRMO'])
    for i in range(1,n_lags+1):
        df[var+'_'+str(i)] = df.groupby(g_by_col)[var].shift(i,fill_value = 0)
    return df

temp = sales_lag(temp, 'DDD_FASENRA', ['BRICK'])
temp.info()
comp_vars = ['COMP_DDD','COMP_VAL','COMP_VOL']
market_vars = ['MARKET_DDD','MARKET_VAL','MARKET_VOL']
brand_vars = ['DDD_FASENRA','DDD_FASENRA_1','DDD_FASENRA_2','VOL_FASENRA','VAL_FASENRA']
promo_vars = ['TOTAL_PDE_AZ_RESPI_BX','TOTAL_PDE_AZ_RESPI_INH', 'ATTENDEES']
others = ['BRICK','YRMO']
model_vars = others + brand_vars + promo_vars + market_vars + comp_vars

temp1 = temp[model_vars]
temp1.info()

###############################################################################
# AdStocking Promotion Variables
def adstock(data, var, n_lags, alpha, g_by_col = ['BRICK']):
    df = data.copy()
    n = np.arange(1,min(n_lags+1,len(alpha))+1)
    df[var + '_adstocked'] = df[var]
    for i in n:
        df[var + '_' + str(i)] = df.groupby(g_by_col)[var].shift(i, fill_value = 0) * alpha[i-1]
        df[var + '_adstocked'] = df[var + '_adstocked'] + df[var + '_' + str(i)]
        del df[var + '_' + str(i)]
    return df

temp2 = adstock(data = temp1, var = 'TOTAL_PDE_AZ_RESPI_INH',
                n_lags = 4, alpha = [0.5,0.5**2,0.5**3,0.5**4])
temp2 = adstock(data = temp2, var = 'TOTAL_PDE_AZ_RESPI_BX',
                n_lags = 4, alpha = [0.5,0.5**2,0.5**3,0.5**4])
temp2 = adstock(data = temp2, var = 'ATTENDEES',
                n_lags = 4, alpha = [0.5,0.5**2,0.5**3,0.5**4])

################################################################################################
# Creating Buckets based on Brand Sales/ Market Sales
def seg_sales(data, var, date_var, new_col = 'BUCKET'):
    df = data.copy()
    start = 201901
    end = 201912
    df = df.loc[(df[date_var]>=start) & (df[date_var]<=end),['BRICK','MARKET_DDD']]
    df = df.groupby('BRICK')[var].sum().reset_index()
    df = df.sort_values(by = var, ascending = False)
    df['PCT'] = np.cumsum(df[var])/np.sum(df[var])
    df = df.reset_index(drop = True)
    val = []
    for pct in df['PCT']:
        if pct<0.33:
            val.append(1)
        elif (pct>=0.33) & (pct <0.67):
            val.append(2)
        else:
            val.append(3)
    df[new_col] = val
    data = pd.merge(data,df[['BRICK',new_col]], how = 'inner', on = 'BRICK')
    return data

temp3 = seg_sales(temp2, 'MARKET_DDD','YRMO')
temp3.groupby('BUCKET')['BRICK'].nunique()
temp3 = rm(temp3)
temp3.columns

################################################################################################
# Data Transformation (Negative Exponential)
bx_c = [0.008170, 0.008339, 0.013412]
inh_c = [0.013947, 0.025587, 0.018331]
me_c = [0.8, 0.8, 0.8]

def transformation(data, var, segment_col, curv, curve_col_name):
    df = data.copy()
    df[curve_col_name] = np.where(df[segment_col]==1,curv[0], np.where(df[segment_col]==2,curv[1], curv[2]))
    df[str(var) + '_TRANS'] = 1- np.exp(-1*df[curve_col_name]*df[var])
    del df[curve_col_name]
    return df

temp4 = transformation(temp3,'TOTAL_PDE_AZ_RESPI_BX_ADSTOCKED','BUCKET',bx_c,'BX_CURV')
temp4 = transformation(temp4,'TOTAL_PDE_AZ_RESPI_INH_ADSTOCKED','BUCKET',inh_c,'INH_CURV')
temp4 = transformation(temp4,'ATTENDEES_ADSTOCKED','BUCKET',me_c,'ATTENDEES_CURV')

# Model Data
temp4.info()
temp5 = temp4.loc[(temp4['YRMO']>=201901) & (temp4['YRMO']<=201912),
                  ['BRICK','YRMO','DDD_FASENRA', 'DDD_FASENRA_1','DDD_FASENRA_2',
                   'TOTAL_PDE_AZ_RESPI_BX_ADSTOCKED_TRANS','TOTAL_PDE_AZ_RESPI_INH_ADSTOCKED_TRANS',
                   'ATTENDEES_ADSTOCKED_TRANS']]
temp5.sum()

# Linear Model
model_vars = ['DDD_FASENRA_1','DDD_FASENRA_2','TOTAL_PDE_AZ_RESPI_BX_ADSTOCKED_TRANS',
              'TOTAL_PDE_AZ_RESPI_INH_ADSTOCKED_TRANS','ATTENDEES_ADSTOCKED_TRANS']
X = sm.add_constant(temp5[model_vars])
Y = temp5['DDD_FASENRA']
model = sm.OLS(Y,X).fit()
print(model.summary())
model.params

y_pred = model.predict(X)
y_test = Y

def mean_absolute_percentage_error(y_actual, y_pred):
    y_actual, y_pred = np.array(y_actual), np.array(y_pred)
    return np.mean(np.abs((y_actual - y_pred) / y_actual)) * 100
mean_absolute_percentage_error(y_test, y_pred)

plt.scatter(Y,y_pred)


################################################################################################
################################################################################################
# Impact Calculation
model.params
sales_avg_1m = temp4.loc[temp4['YRMO']==201812,'DDD_FASENRA'].mean()
sales_avg_2m = temp4.loc[temp4['YRMO']==201811,'DDD_FASENRA'].mean()

coef_df = np.transpose(pd.DataFrame(model.params))
promo = ['TOTAL_PDE_AZ_RESPI_BX_ADSTOCKED_TRANS','TOTAL_PDE_AZ_RESPI_INH_ADSTOCKED_TRANS','ATTENDEES_ADSTOCKED_TRANS']

def impact(data, start, end, promotions, target, lag1, lag2):
    df1 = {}
    df1.update({'BRICK_COUNT':data['BRICK'].nunique(),
                'DDD_FASENRA_AVG':data[target].mean(),
                str(promotions[0]+'_AVG'):data[promotions[0]].mean(),
                str(promotions[1]+'_AVG'):data[promotions[1]].mean(),
                str(promotions[2]+'_AVG'):data[promotions[2]].mean(),
                'DDD_FASENRA' : data[target].sum()
                })
    df = pd.DataFrame(df1, index = [0])
    df = pd.merge(df,coef_df, left_index = True, right_index = True)
    for i in range(len(promotions)):
        col = promotions[i]
        for j in range(start,end+1):
            if j == start:
                df[str(col)+'_IMPACT'+str(j)] = df[col]*df[str(col)+'_AVG']
            elif j == start + 1:
                df[str(col)+'_IMPACT'+str(j)] = df[col]*df[str(col)+'_AVG'] + df[str(col)+'_IMPACT'+str(j-1)] * df[lag1]
            else:
                df[str(col)+'_IMPACT'+str(j)] = df[col]*df[str(col)+'_AVG'] + df[str(col)+'_IMPACT'+str(j-1)] * df[lag1] + df[str(col)+'_IMPACT'+str(j-2)] * df[lag2]
        df[str(col)+'_BRICK_IMPACT'] = df[[x for x in df.columns if x.startswith(col) & ~(x == col)]].sum(axis=1)
        df[str(col)+'_TOTAL_IMPACT'] = df[str(col)+'_BRICK_IMPACT'] * df['BRICK_COUNT']
        df[str(col)+'_IMPACT_PERCENT'] = (df[str(col)+'_TOTAL_IMPACT'] / df[target])*100
    
    for l in range(start, end+1):
        if l == start:
            df['BASELINE_' + str(l)] = df['const']
        elif l == start+ 1:
            df['BASELINE_' + str(l)] = df['const'] + df['BASELINE_' + str(l-1)] * df[lag1]
        else:
            df['BASELINE_' + str(l)] = df['const'] + df['BASELINE_' + str(l-1)] * df[lag1] + df['BASELINE_' + str(l-2)] * df[lag2]
    df['BASELINE_BRICK_IMPACT'] = df[[x for x in df.columns if x.startswith('BASELINE')]].sum(axis=1)
    df['BASELINE_TOTAL_IMPACT'] = df['BASELINE_BRICK_IMPACT'] * df['BRICK_COUNT']
    df['BASELINE_PERCENT_IMPACT'] = df['BASELINE_TOTAL_IMPACT'] / df[target] * 100
    
    for k in range(start, end+1):
        if k == start:
            df['CO_'+ str(k)] = df[lag1] * sales_avg_1m + df[lag2] * sales_avg_2m 
        elif k == start + 1:
            df['CO_' + str(k)] = df['CO_'+ str(k-1)] * df[lag1] + df[lag2] * sales_avg_1m
        else:
            df['CO_' + str(k)] = df['CO_'+ str(k-1)] * df[lag1] + df['CO_'+ str(k-2)] * df[lag2] 
    df['CO_BRICK_IMPACT'] = df[[x for x in df.columns if x.startswith('CO_')]].sum(axis=1)
    df['CO_TOTAL_IMPACT'] = df['CO_BRICK_IMPACT'] * df['BRICK_COUNT']
    df['CO_PERCENT_IMPACT'] = df['CO_TOTAL_IMPACT'] / df[target] * 100
    return df

impactable = np.transpose(impact(temp5,201901, 201912, promo, 'DDD_FASENRA', 'DDD_FASENRA_1', 'DDD_FASENRA_2')).reset_index().rename(columns = {'index':'INDEX',0:'VALUE'})

##########################################################################################################
##########################################################################################################
# Mixed Models































