import pandas as pd
import numpy as np
import os

os.chdir("C:\\Users\\knss680\\Box Sync\\Fasenra\\Final Deliverables\\DATA\\Raw")

# Rename Columns
def rm(df):
    df.columns = df.columns.str.replace(' ', '')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('+', '_')
    return df

# Reading Input data
data = pd.read_excel("Asthma market sales.xlsx", sheet_name = "Raw", usecols = range(15))
data.info()
data1 = data.copy()

# Checking Date Column and creating new DateTime variables
data1['ZEUMS_DATUM'].unique()
date_col = 'ZEUMS_DATUM'

def date_trans(df, col, form):
    df[col] = pd.to_datetime(df[col], format = form)
    df['Year'] = df[col].dt.year
    df['Month'] = df[col].dt.day
    df['yrmo'] = df[col].dt.strftime('%Y%d').astype('int')
    return df

data1 = date_trans(df = data1, col = date_col, form = '%Y-%d-%m')
data1.yrmo.unique()

# Data Preparation - Long to Wide Format
data1.info()
data1.isnull().sum()
data1[['ZEIG_NAME','yrmo','ZEUMS_ZZEL_ID']].nunique()

data2 = data1.pivot_table(index = ['yrmo','ZEUMS_ZZEL_ID'], columns = ['ZEIG_NAME'],
                         values = ['DDD','VOL','VAL'], aggfunc = 'sum').reset_index()

data2.columns = ['_'.join(col).upper() for col in data2.columns.values]
data2 = data2.rename(columns = {'YRMO_':'YRMO', 'ZEUMS_ZZEL_ID_':'BRICK'})
data2.columns
data2 = rm(data2)

# Creating Market and Competition Variables
data2.columns
def mar(df, col, brick_col, date_col):
    temp = pd.DataFrame(columns = ['YRMO','BRICK'])
    for i in col:
        df1 = df[[col for col in df.columns.str.upper() if col.startswith(brick_col) | col.startswith(date_col) | col.startswith(i)]]
        df1 = pd.concat([df1[['YRMO','BRICK']], df1.drop(['YRMO','BRICK'], axis = 1).sum(axis = 1)], axis = 1).rename(columns = {0:'MARKET_'+str(i)})
        temp = pd.merge(df1,temp, how = 'left', on = ['BRICK','YRMO'])
    return temp

df_market = mar(df = data2, col = ['DDD','VOL','VAL'], brick_col='BRICK', date_col='YRMO')

df_market.MARKET_DDD.sum()
df_market.MARKET_VOL.sum()
df_market.MARKET_VAL.sum()

def comp(df, brand, col, brick_col, date_col):
    temp = pd.DataFrame(columns = ['YRMO','BRICK'])
    for i in col:
        df1 = df[[col for col in df.columns.str.upper() if (col.startswith(brick_col) | col.startswith(date_col) | col.startswith(i) & ~(col.endswith(brand)))]]
        df1 = pd.concat([df1[['YRMO','BRICK']], df1.drop(['YRMO','BRICK'], axis = 1).sum(axis = 1)], axis = 1).rename(columns = {0:'COMP_'+str(i)})
        temp = pd.merge(df1,temp, how = 'left', on = ['BRICK','YRMO'])
    return temp

df_comp = comp(df = data2, brand = 'FASENRA', col = ['DDD','VOL','VAL'], brick_col='BRICK', date_col='YRMO')

df_comp.COMP_DDD.sum()
df_comp.COMP_VOL.sum()
df_comp.COMP_VAL.sum()

# Final Data
df_final = pd.merge(data2, df_market, how = 'left', on = ['BRICK','YRMO'])
df_final = pd.merge(df_final, df_comp, how = 'left', on = ['BRICK','YRMO'])

start = 201901
end = 201912
df_final1 = df_final.loc[(df_final['YRMO'] >=start) & (df_final['YRMO'] <=end),].reset_index(drop = True)

df_final.to_csv('Sales_Data_Prepared.csv', index = False)






















