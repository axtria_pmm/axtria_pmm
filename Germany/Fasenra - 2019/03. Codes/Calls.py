import pandas as pd
import numpy as np
import os
from functools import reduce
pd.set_option('display.float_format', lambda x: '%.2f' % x)

os.chdir("C:/Users/knss680/Box Sync/Germany 2021/Germany Project/Forxiga/Raw Data/Calls")

# Rename Columns
def rm(df):
    df.columns = df.columns.str.upper()
    df.columns = df.columns.str.replace(' ', '')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('+', '_')
    return df

# Reading Input data
data = pd.read_excel("Copy of 05_HCP_Promotional_Activity_CALL.xlsx", sheet_name = "05_HCP_Promotional_Activity_CAL")
mapping_df = pd.read_excel("Minibrick_Territory_Mapping.xlsx")
data.drop(['Year','Month','YrMo'], axis = 1, inplace = True)
data.info()

# Creating a copy of the data
data1 = data.copy()
data1.columns

# Checking Date Column and creating new DateTime variables
data1['FACT_DATE'].unique()
date_col = 'FACT_DATE'
data1.dtypes

def date_trans(df, col, form):
    df[col] = pd.to_datetime(df[col], format = form)
    df['Year'] = df[col].dt.year
    df['Month'] = df[col].dt.month
    df['yrmo'] = df[col].dt.strftime('%Y%m').astype('int')
    return df

data1 = date_trans(df = data1, col = date_col, form = '%d.%m.%Y')
data1.yrmo.unique()

# Getting Brick info using mapping file
data2 = pd.merge(data1,mapping_df[['Territory_identifier', 'Brick']], how = 'inner', left_on = 'FACT_DCELL_ID_1', right_on = 'Territory_identifier')

# Renaming all the column names
data2 = rm(data2)
#data2.isnull().sum()
#data2.columns
#data2['BRICK'].nunique()

# Creating a temp df @ Brick*YearMonth granularity (will use at the end)
temp = data2[['BRICK','YRMO']].drop_duplicates(subset = ['BRICK','YRMO'],keep = 'first')

# Filtering on Activity Type, Sales Unit
data2['ACTT_DESC'].value_counts()
data2['SALES_UNIT'].value_counts()
calls_type = ['AZ FF Traditional Call','AZ FF Congress Contact','AZ FF Working Dinner']
teams = ['AZ_RESPI_BX','AZ_RESPI_INH']
start = 201901
end = 201912
data3 = data2.loc[data2['ACTT_DESC'].isin(calls_type) & data2['SALES_UNIT'].isin(teams),]

data3['FIRST_DETAIL'].value_counts()
data3['SECOND_DETAIL'].value_counts()
data3['THIRD_DETAIL'].value_counts()

# Defining function to get Calls & PDEs for each team
def call_pde1(df, detail_1, detail_2, detail_3, act_col, b_name, g_by_col):
    df_final = pd.DataFrame(columns = ['BRICK','YRMO'])
    for team in teams:
        df_temp = df.loc[df['ACTT_DESC'].isin(calls_type) & (df['SALES_UNIT'] == team),]
        df1 = df_temp.loc[(df_temp[detail_1]==b_name) & (df_temp[detail_2]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
        df2 = df_temp.loc[(df_temp[detail_1]==b_name) & (df_temp[detail_2]!='NV') & (df_temp[detail_3]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
        df3 = df_temp.loc[(df_temp[detail_1]==b_name) & (df_temp[detail_2]!='NV') & (df_temp[detail_3]!='NV'),].groupby(g_by_col)[act_col].count().reset_index()
        df4 = df_temp.loc[(df_temp[detail_1]!=b_name) & (df_temp[detail_2]==b_name) & (df_temp[detail_3]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
        df5 = df_temp.loc[(df_temp[detail_1]!=b_name) & (df_temp[detail_2]==b_name) & (df_temp[detail_3]!='NV'),].groupby(g_by_col)[act_col].count().reset_index()
        df6 = df_temp.loc[(df_temp[detail_1]!=b_name) & (df_temp[detail_2]!='NV') & (df_temp[detail_3]==b_name),].groupby(g_by_col)[act_col].count().reset_index()
        dfs = [df1,df2,df3,df4,df5,df6]
        temp_df = reduce(lambda  left,right: pd.merge(left,right,on=g_by_col,
                                                how='outer'), dfs)
        temp_df = temp_df.fillna(0)
        temp_df.columns = ['BRICK','YRMO','A', 'A_B', 'A_B_C', 'B_A', 'B_A_C', 'B_C_A']
        temp_df.columns = temp_df.columns[:2].append(str(team)+'_'+temp_df.columns[2:])
        temp_df = temp_df.fillna(0)
        temp_df['TOTAL_CALLS' + '_' + str(team)] = temp_df.drop(['BRICK','YRMO'],axis = 1).sum(axis = 1)
        temp_df = temp_df.fillna(0)
        temp_df['TOTAL_PDE'+ '_' +str(team)] = temp_df[str(team)+'_'+'A'] + 0.7*temp_df[str(team)+'_'+'A_B'] + 0.6*temp_df[str(team)+'_'+'A_B_C'] + 0.3*temp_df[str(team)+'_'+'B_A'] + 0.3*temp_df[str(team)+'_'+'B_A_C'] + 0.1*temp_df[str(team)+'_'+'B_C_A']
        df_final = pd.merge(df_final,temp_df, how = 'outer', on = g_by_col)
        df_final = df_final.fillna(0)
    return df_final

call_df = call_pde1(df = data3, detail_1 = 'FIRST_DETAIL', detail_2='SECOND_DETAIL',
                 detail_3='THIRD_DETAIL', act_col='FACT_ACT_ID',b_name='Fasenra_DE',
                 g_by_col=['BRICK','YRMO']).fillna(0)

call_df.sum()
call_df.loc[(call_df['YRMO']>=start) & (call_df['YRMO']<=end),].sum()


# Exporting in a .csv file
call_df.to_csv('Calls_Data.csv', index = False) 



############################################################################################################################################################################
############################################################################################################################################################################
###########################################  COMPLETED  ######################################################################################################################
############################################################################################################################################################################
############################################################################################################################################################################











# Defining function to get Call and PDE
def call_pde(df, detail_1, detail_2, detail_3, act_col, b_name, g_by_col, team):
    df = df.loc[df['ACTT_DESC'].isin(calls_type) & (df['SALES_UNIT']==team),]
    df1 = df.loc[(df[detail_1]==b_name) & (df[detail_2]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
    df2 = df.loc[(df[detail_1]==b_name) & (df[detail_2]!='NV') & (df[detail_3]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
    df3 = df.loc[(df[detail_1]==b_name) & (df[detail_2]!='NV') & (df[detail_3]!='NV'),].groupby(g_by_col)[act_col].count().reset_index()
    df4 = df.loc[(df[detail_1]!=b_name) & (df[detail_2]==b_name) & (df[detail_3]=='NV'),].groupby(g_by_col)[act_col].count().reset_index()
    df5 = df.loc[(df[detail_1]!=b_name) & (df[detail_2]==b_name) & (df[detail_3]!='NV'),].groupby(g_by_col)[act_col].count().reset_index()
    df6 = df.loc[(df[detail_1]!=b_name) & (df[detail_2]!='NV') & (df[detail_3]==b_name),].groupby(g_by_col)[act_col].count().reset_index()
    dfs = [df1,df2,df3,df4,df5,df6]
    df_final = reduce(lambda  left,right: pd.merge(left,right,on=g_by_col,
                                            how='outer'), dfs)
    df_final.columns = ['BRICK','YRMO','A', 'A_B', 'A_B_C', 'B_A', 'B_A_C', 'B_C_A']
    df_final = df_final.fillna(0)
    df_final['TOTAL_CALLS'] = df_final.drop(['BRICK','YRMO'],axis = 1).sum(axis = 1)
    df_final.columns = df_final.columns[:2].append(str(team)+'_'+df_final.columns[2:])
    df_final['TOTAL_PDE'+ '_' +str(team)] = df_final[str(team)+'_'+'A'] + 0.7*df_final[str(team)+'_'+'A_B'] + 0.6*df_final[str(team)+'_'+'A_B_C'] + 0.3*df_final[str(team)+'_'+'B_A'] + 0.3*df_final[str(team)+'_'+'B_A_C'] + 0.1*df_final[str(team)+'_'+'B_C_A']
    return df_final

# Creating calls df for all the Sales Units (BX & INH)
bx_call = call_pde(df = data3, detail_1 = 'FIRST_DETAIL', detail_2='SECOND_DETAIL',
                 detail_3='THIRD_DETAIL', act_col='FACT_ACT_ID',b_name='Fasenra_DE',
                 g_by_col=['BRICK','YRMO'], team = 'AZ_RESPI_BX').fillna(0)

bx_call.loc[(bx_call['YRMO']>=start) & (bx_call['YRMO']<=end),].sum()

inh_call = call_pde(df = data3, detail_1 = 'FIRST_DETAIL', detail_2='SECOND_DETAIL',
                 detail_3='THIRD_DETAIL', act_col='FACT_ACT_ID',b_name='Fasenra_DE',
                 g_by_col=['BRICK','YRMO'], team = 'AZ_RESPI_INH').fillna(0)

inh_call.loc[(inh_call['YRMO']>=start) & (inh_call['YRMO']<=end),].sum()

# Merging all the dfs with temp_df (created earlier)
final_dfs = [temp, bx_call, inh_call]
df_final = reduce(lambda  left,right: pd.merge(left,right,on=['BRICK','YRMO'],
                                            how='outer'), final_dfs).fillna(0)


